// *****************************************************************************
// STLBM SOFTWARE LIBRARY

// Copyright © 2020 University of Geneva
// Authors: Jonas Latt, Christophe Coreixas, Joël Beny
// Contact: Jonas.Latt@unige.ch

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// *****************************************************************************

#pragma once

#include "lbm_q27.h"
#include <vector>
#include <array>
#include <tuple>

namespace aa_soa_chm {

struct LBM {
    using CellData = double;
    static size_t sizeOfLattice(size_t nelem) { return 27 * nelem; }

    CellData* lattice;
    CellType* flag;
    std::array<int, 3>* c;
    int* opp;
    double* t;
    double omega1;
    Dim dim;
    LBModel model;
    bool periodic = false;
    double omegaBulk = 0.;
    double omega2 = omega1;
    double omega3 = omega1;
    double omega4 = omega1;
    double omega5 = omega1;
    double omega6 = omega1;
    double omega7 = omega1;
    double omega8 = omega1;

    auto i_to_xyz (int i) const {
        int iX = i / (dim.ny * dim.nz);
        int remainder = i % (dim.ny * dim.nz);
        int iY = remainder / dim.nz;
        int iZ = remainder % dim.nz;
        return std::make_tuple(iX, iY, iZ);
    };

    size_t xyz_to_i (int x, int y, int z) const {
        return z + dim.nz * (y + dim.ny * x);
    };

    double& f (int i, int k) {
        return lattice[k * dim.nelem + i];
    }

    auto iniLattice (double& f0) {
        auto i = &f0 - lattice;
        for (int k = 0; k < 27; ++k) {
            f(i, k) = t[k];
        }
    };

    auto iniLattice (double& f0, double rho, std::array<double, 3> const& u) {
        auto i = &f0 - lattice;
        double usqr = 1.5 * (u[0] * u[0] + u[1] * u[1] + u[2] * u[2]);
        for (int k = 0; k < 27; ++k) {
            double ck_u = c[k][0] * u[0] + c[k][1] * u[1] + c[k][2] * u[2];
            f(i, k) = rho * t[k] * (1. + 3. * ck_u + 4.5 * ck_u * ck_u - usqr);
        }
    };

    auto macro (double const& f0) {
        auto i = &f0 - lattice;
        double X_M1 = f(i, 0) + f(i, 3) + f(i, 4) + f(i, 5) + f(i, 6) + f(i, 9) + f(i,10) + f(i,11) + f(i,12);
        double X_P1 = f(i,14) + f(i,17) + f(i,18) + f(i,19) + f(i,20) + f(i,23) + f(i,24) + f(i,25) + f(i,26);
        double X_0  = f(i, 1) + f(i, 2) + f(i, 7) + f(i, 8) + f(i,13) + f(i,15) + f(i,16) + f(i,21) + f(i,22);

        double Y_M1 = f(i, 1) + f(i, 3) + f(i, 7) + f(i, 8) + f(i, 9) + f(i,10) + f(i,18) + f(i,25) + f(i,26);
        double Y_P1 = f(i,15) + f(i,17) + f(i,21) + f(i,22) + f(i,23) + f(i,24) + f(i, 4) + f(i,11) + f(i,12);

        double Z_M1 = f(i, 2) + f(i, 5) + f(i, 7) + f(i, 9) + f(i,11) + f(i,20) + f(i,22) + f(i,24) + f(i,26);
        double Z_P1 = f(i,16) + f(i,19) + f(i,21) + f(i,23) + f(i,25) + f(i, 6) + f(i, 8) + f(i,10) + f(i,12);

        double rho = X_M1 + X_P1 + X_0;
        std::array<double, 3> u{ (X_P1 - X_M1) / rho, (Y_P1 - Y_M1) / rho, (Z_P1 - Z_M1) / rho };
        return make_pair(rho, u);
    }

    auto macropop (std::array<double, 27> const& pop) {
        double X_M1 = pop[ 0] + pop[ 3] + pop[ 4] + pop[ 5] + pop[ 6] + pop[ 9] + pop[10] + pop[11] + pop[12];
        double X_P1 = pop[14] + pop[17] + pop[18] + pop[19] + pop[20] + pop[23] + pop[24] + pop[25] + pop[26];
        double X_0  = pop[ 1] + pop[ 2] + pop[ 7] + pop[ 8] + pop[13] + pop[15] + pop[16] + pop[21] + pop[22];

        double Y_M1 = pop[ 1] + pop[ 3] + pop[ 7] + pop[ 8] + pop[ 9] + pop[10] + pop[18] + pop[25] + pop[26];
        double Y_P1 = pop[15] + pop[17] + pop[21] + pop[22] + pop[23] + pop[24] + pop[ 4] + pop[11] + pop[12];

        double Z_M1 = pop[ 2] + pop[ 5] + pop[ 7] + pop[ 9] + pop[11] + pop[20] + pop[22] + pop[24] + pop[26];
        double Z_P1 = pop[16] + pop[19] + pop[21] + pop[23] + pop[25] + pop[ 6] + pop[ 8] + pop[10] + pop[12];

        double rho = X_M1 + X_P1 + X_0;
        std::array<double, 3> u{ (X_P1 - X_M1) / rho, (Y_P1 - Y_M1) / rho, (Z_P1 - Z_M1) / rho };
        return make_pair(rho, u);
    };

    // Naive way to compute CHMs and macros
    auto computeCHM(std::array<double, 27> const& pop) {
        std::array<double, 27> CHM;
        std::fill(CHM.begin(), CHM.end(), 0.);
        double cMux, cMuy, cMuz;
        double Hxx, Hyy, Hzz;
        double cs2 = 1./3.;

        // Preliminary computation of density and velocity vector
        double X_M1 = pop[ 0] + pop[ 3] + pop[ 4] + pop[ 5] + pop[ 6] + pop[ 9] + pop[10] + pop[11] + pop[12];
        double X_P1 = pop[14] + pop[17] + pop[18] + pop[19] + pop[20] + pop[23] + pop[24] + pop[25] + pop[26];
        double X_0  = pop[ 1] + pop[ 2] + pop[ 7] + pop[ 8] + pop[13] + pop[15] + pop[16] + pop[21] + pop[22];

        double Y_M1 = pop[ 1] + pop[ 3] + pop[ 7] + pop[ 8] + pop[ 9] + pop[10] + pop[18] + pop[25] + pop[26];
        double Y_P1 = pop[15] + pop[17] + pop[21] + pop[22] + pop[23] + pop[24] + pop[ 4] + pop[11] + pop[12];

        double Z_M1 = pop[ 2] + pop[ 5] + pop[ 7] + pop[ 9] + pop[11] + pop[20] + pop[22] + pop[24] + pop[26];
        double Z_P1 = pop[16] + pop[19] + pop[21] + pop[23] + pop[25] + pop[ 6] + pop[ 8] + pop[10] + pop[12];

        double rho = X_M1 + X_P1 + X_0;
        std::array<double, 3> u{ (X_P1 - X_M1) / rho, (Y_P1 - Y_M1) / rho, (Z_P1 - Z_M1) / rho };
        CHM[M000] = rho;
        CHM[M100] = u[0];  // CHM is used to store the velocity component 
        CHM[M010] = u[1];  // CHM is used to store the velocity component
        CHM[M001] = u[2];  // CHM is used to store the velocity component

        // Naive computation of CHMs
        for (int k = 0; k<27; ++k) {
            cMux = c[k][0]- u[0];
            cMuy = c[k][1]- u[1];
            cMuz = c[k][2]- u[2];
            Hxx = cMux * cMux - cs2;
            Hyy = cMuy * cMuy - cs2;
            Hzz = cMuz * cMuz - cs2;

            // Order 2
            CHM[M200] += Hxx * pop[k];
            CHM[M020] += Hyy * pop[k];
            CHM[M002] += Hzz * pop[k];
            CHM[M110] += cMux * cMuy * pop[k];
            CHM[M101] += cMux * cMuz * pop[k];
            CHM[M011] += cMuy * cMuz * pop[k];
            // Order 3
            CHM[M210] += Hxx * cMuy * pop[k];
            CHM[M201] += Hxx * cMuz * pop[k];
            CHM[M021] += Hyy * cMuz * pop[k];
            CHM[M120] += cMux * Hyy * pop[k];
            CHM[M102] += cMux * Hzz * pop[k];
            CHM[M012] += cMuy * Hzz * pop[k];
            CHM[M111] += cMux * cMuy * cMuz * pop[k];
            // Order 4
            CHM[M220] += Hxx * Hyy * pop[k];
            CHM[M202] += Hxx * Hzz * pop[k];
            CHM[M022] += Hyy * Hzz * pop[k];
            CHM[M211] += Hxx * cMuy * cMuz * pop[k];
            CHM[M121] += cMux * Hyy * cMuz * pop[k];
            CHM[M112] += cMux * cMuy * Hzz * pop[k];
            // Order 5
            CHM[M221] += Hxx * Hyy * cMuz * pop[k];
            CHM[M212] += Hxx * cMuy * Hzz * pop[k];
            CHM[M122] += cMux * Hyy * Hzz * pop[k];
            // Order 6
            CHM[M222] += Hxx * Hyy * Hzz * pop[k];
        }

        double invRho = 1./rho;
        for (int k = 4; k<27; ++k) {
            CHM[k] *= invRho;
        }

        return CHM;
    }

    // First optimization (loop unrolling)
    auto computeCHMopt(std::array<double, 27> const& pop) {
        std::array<double, 27> CHM;
        std::fill(CHM.begin(), CHM.end(), 0.);
        // Order 0
        CHM[M000] =  pop[ 0] + pop[ 1] + pop[ 2] + pop[ 3] + pop[ 4] + pop[ 5] + pop[ 6] + pop[ 7] + pop[ 8] + pop[ 9] + pop[10] + pop[11] + pop[12] + pop[13] + pop[14] + pop[15] + pop[16] + pop[17] + pop[18] + pop[19] + pop[20] + pop[21] + pop[22] + pop[23] + pop[24] + pop[25] + pop[26];
        double invRho = 1./CHM[M000];
        // Order 1
        CHM[M100] = invRho * (- pop[ 0] - pop[ 3] - pop[ 4] - pop[ 5] - pop[ 6] - pop[ 9] - pop[10] - pop[11] - pop[12] + pop[14] + pop[17] + pop[18] + pop[19] + pop[20] + pop[23] + pop[24] + pop[25] + pop[26]);
        CHM[M010] = invRho * (- pop[ 1] - pop[ 3] + pop[ 4] - pop[ 7] - pop[ 8] - pop[ 9] - pop[10] + pop[11] + pop[12] + pop[15] + pop[17] - pop[18] + pop[21] + pop[22] + pop[23] + pop[24] - pop[25] - pop[26]);
        CHM[M001] = invRho * (- pop[ 2] - pop[ 5] + pop[ 6] - pop[ 7] + pop[ 8] - pop[ 9] + pop[10] - pop[11] + pop[12] + pop[16] + pop[19] - pop[20] + pop[21] - pop[22] + pop[23] - pop[24] + pop[25] - pop[26]);
        // Order 2
        CHM[M200] = invRho * (  pop[ 0] + pop[ 3] + pop[ 4] + pop[ 5] + pop[ 6] + pop[ 9] + pop[10] + pop[11] + pop[12] + pop[14] + pop[17] + pop[18] + pop[19] + pop[20] + pop[23] + pop[24] + pop[25] + pop[26]);
        CHM[M020] = invRho * (  pop[ 1] + pop[ 3] + pop[ 4] + pop[ 7] + pop[ 8] + pop[ 9] + pop[10] + pop[11] + pop[12] + pop[15] + pop[17] + pop[18] + pop[21] + pop[22] + pop[23] + pop[24] + pop[25] + pop[26]);
        CHM[M002] = invRho * (  pop[ 2] + pop[ 5] + pop[ 6] + pop[ 7] + pop[ 8] + pop[ 9] + pop[10] + pop[11] + pop[12] + pop[16] + pop[19] + pop[20] + pop[21] + pop[22] + pop[23] + pop[24] + pop[25] + pop[26]);
        CHM[M110] = invRho * (  pop[ 3] - pop[ 4] + pop[ 9] + pop[10] - pop[11] - pop[12] + pop[17] - pop[18] + pop[23] + pop[24] - pop[25] - pop[26]);
        CHM[M101] = invRho * (  pop[ 5] - pop[ 6] + pop[ 9] - pop[10] + pop[11] - pop[12] + pop[19] - pop[20] + pop[23] - pop[24] + pop[25] - pop[26]);
        CHM[M011] = invRho * (  pop[ 7] - pop[ 8] + pop[ 9] - pop[10] - pop[11] + pop[12] + pop[21] - pop[22] + pop[23] - pop[24] - pop[25] + pop[26]);
        // Order 3
        CHM[M210] = invRho * (- pop[ 3] + pop[ 4] - pop[ 9] - pop[10] + pop[11] + pop[12] + pop[17] - pop[18] + pop[23] + pop[24] - pop[25] - pop[26]);
        CHM[M201] = invRho * (- pop[ 5] + pop[ 6] - pop[ 9] + pop[10] - pop[11] + pop[12] + pop[19] - pop[20] + pop[23] - pop[24] + pop[25] - pop[26]);
        CHM[M021] = invRho * (- pop[ 7] + pop[ 8] - pop[ 9] + pop[10] - pop[11] + pop[12] + pop[21] - pop[22] + pop[23] - pop[24] + pop[25] - pop[26]);
        CHM[M120] = invRho * (- pop[ 3] - pop[ 4] - pop[ 9] - pop[10] - pop[11] - pop[12] + pop[17] + pop[18] + pop[23] + pop[24] + pop[25] + pop[26]);
        CHM[M102] = invRho * (- pop[ 5] - pop[ 6] - pop[ 9] - pop[10] - pop[11] - pop[12] + pop[19] + pop[20] + pop[23] + pop[24] + pop[25] + pop[26]);
        CHM[M012] = invRho * (- pop[ 7] - pop[ 8] - pop[ 9] - pop[10] + pop[11] + pop[12] + pop[21] + pop[22] + pop[23] + pop[24] - pop[25] - pop[26]);
        CHM[M111] = invRho * (- pop[ 9] + pop[10] + pop[11] - pop[12] + pop[23] - pop[24] - pop[25] + pop[26]);
        // Order 4
        CHM[M220] = invRho * (  pop[ 3] + pop[ 4] + pop[ 9] + pop[10] + pop[11] + pop[12] + pop[17] + pop[18] + pop[23] + pop[24] + pop[25] + pop[26]);
        CHM[M202] = invRho * (  pop[ 5] + pop[ 6] + pop[ 9] + pop[10] + pop[11] + pop[12] + pop[19] + pop[20] + pop[23] + pop[24] + pop[25] + pop[26]);
        CHM[M022] = invRho * (  pop[ 7] + pop[ 8] + pop[ 9] + pop[10] + pop[11] + pop[12] + pop[21] + pop[22] + pop[23] + pop[24] + pop[25] + pop[26]);
        CHM[M211] = invRho * (  pop[ 9] - pop[10] - pop[11] + pop[12] + pop[23] - pop[24] - pop[25] + pop[26]);
        CHM[M121] = invRho * (  pop[ 9] - pop[10] + pop[11] - pop[12] + pop[23] - pop[24] + pop[25] - pop[26]);
        CHM[M112] = invRho * (  pop[ 9] + pop[10] - pop[11] - pop[12] + pop[23] + pop[24] - pop[25] - pop[26]);
        // Order 5
        CHM[M221] = invRho * (- pop[ 9] + pop[10] - pop[11] + pop[12] + pop[23] - pop[24] + pop[25] - pop[26]);
        CHM[M212] = invRho * (- pop[ 9] - pop[10] + pop[11] + pop[12] + pop[23] + pop[24] - pop[25] - pop[26]);
        CHM[M122] = invRho * (- pop[ 9] - pop[10] - pop[11] - pop[12] + pop[23] + pop[24] + pop[25] + pop[26]);
        // Order 6
        CHM[M222] = invRho * (  pop[ 9] + pop[10] + pop[11] + pop[12] + pop[23] + pop[24] + pop[25] + pop[26]);

        // Compute CMs from RMs using binomial formulas
        double ux   = CHM[M100];
        double uy   = CHM[M010];
        double uz   = CHM[M001];
        double ux2  = CHM[M100]*CHM[M100];
        double uy2  = CHM[M010]*CHM[M010];
        double uz2  = CHM[M001]*CHM[M001];
        double uxyz = CHM[M100]*CHM[M010]*CHM[M001];

        CHM[M200] -= (ux2);
        CHM[M020] -= (uy2);
        CHM[M002] -= (uz2);
        
        CHM[M110] -= (ux*uy);
        CHM[M101] -= (ux*uz);
        CHM[M011] -= (uy*uz);

        CHM[M210] -= (uy*CHM[M200] + 2.*ux*CHM[M110] + ux2*uy);
        CHM[M201] -= (uz*CHM[M200] + 2.*ux*CHM[M101] + ux2*uz);
        CHM[M021] -= (uz*CHM[M020] + 2.*uy*CHM[M011] + uy2*uz);
        CHM[M120] -= (ux*CHM[M020] + 2.*uy*CHM[M110] + ux*uy2);
        CHM[M102] -= (ux*CHM[M002] + 2.*uz*CHM[M101] + ux*uz2);
        CHM[M012] -= (uy*CHM[M002] + 2.*uz*CHM[M011] + uy*uz2);
        
        CHM[M111] -= (uz*CHM[M110] + uy*CHM[M101] + ux*CHM[M011] + uxyz );

        CHM[M220] -= (2.*uy*CHM[M210] + 2.*ux*CHM[M120] + uy2*CHM[M200] + ux2*CHM[M020] + 4.*ux*uy*CHM[M110] + ux2*uy2);
        CHM[M202] -= (2.*uz*CHM[M201] + 2.*ux*CHM[M102] + uz2*CHM[M200] + ux2*CHM[M002] + 4.*ux*uz*CHM[M101] + ux2*uz2);
        CHM[M022] -= (2.*uz*CHM[M021] + 2.*uy*CHM[M012] + uz2*CHM[M020] + uy2*CHM[M002] + 4.*uy*uz*CHM[M011] + uy2*uz2);

        CHM[M211] -= (uz*CHM[M210] + uy*CHM[M201] + 2.*ux*CHM[M111] + uy*uz*CHM[M200] + 2.*ux*uz*CHM[M110] + 2.*ux*uy*CHM[M101] + ux2*CHM[M011] + ux2*uy*uz);
        CHM[M121] -= (uz*CHM[M120] + ux*CHM[M021] + 2.*uy*CHM[M111] + ux*uz*CHM[M020] + 2.*uy*uz*CHM[M110] + 2.*ux*uy*CHM[M011] + uy2*CHM[M101] + ux*uy2*uz);
        CHM[M112] -= (uy*CHM[M102] + ux*CHM[M012] + 2.*uz*CHM[M111] + ux*uy*CHM[M002] + 2.*uy*uz*CHM[M101] + 2.*ux*uz*CHM[M011] + uz2*CHM[M110] + ux*uy*uz2);

        CHM[M221] -= (uz*CHM[M220] + 2.*uy*CHM[M211] + 2.*ux*CHM[M121] + 2.*uy*uz*CHM[M210] + uy2*CHM[M201] + ux2*CHM[M021] + 2.*ux*uz*CHM[M120] + 4.*ux*uy*CHM[M111] + uy2*uz*CHM[M200] + ux2*uz*CHM[M020] + 4.*uxyz*CHM[M110] + 2.*ux*uy2*CHM[M101] + 2.*ux2*uy*CHM[M011] + ux2*uy2*uz);
        CHM[M212] -= (uy*CHM[M202] + 2.*uz*CHM[M211] + 2.*ux*CHM[M112] + 2.*uy*uz*CHM[M201] + uz2*CHM[M210] + ux2*CHM[M012] + 2.*ux*uy*CHM[M102] + 4.*ux*uz*CHM[M111] + uy*uz2*CHM[M200] + ux2*uy*CHM[M002] + 4.*uxyz*CHM[M101] + 2.*ux*uz2*CHM[M110] + 2.*ux2*uz*CHM[M011] + ux2*uy*uz2);
        CHM[M122] -= (ux*CHM[M022] + 2.*uz*CHM[M121] + 2.*uy*CHM[M112] + 2.*ux*uz*CHM[M021] + uz2*CHM[M120] + uy2*CHM[M102] + 2.*ux*uy*CHM[M012] + 4.*uy*uz*CHM[M111] + ux*uz2*CHM[M020] + ux*uy2*CHM[M002] + 4.*uxyz*CHM[M011] + 2.*uy*uz2*CHM[M110] + 2.*uy2*uz*CHM[M101] + ux*uy2*uz2);

        CHM[M222] -= (2.*uz*CHM[M221] + 2.*uy*CHM[M212] + 2.*ux*CHM[M122] + uz2*CHM[M220] + uy2*CHM[M202] + ux2*CHM[M022] + 4.*uy*uz*CHM[M211] + 4.*ux*uz*CHM[M121] + 4.*ux*uy*CHM[M112] + 2.*uy*uz2*CHM[M210] + 2.*uy2*uz*CHM[M201] + 2.*ux2*uz*CHM[M021] + 2.*ux*uz2*CHM[M120] + 2.*ux*uy2*CHM[M102] + 2.*ux2*uy*CHM[M012] + 8.*uxyz*CHM[M111] + uy2*uz2*CHM[M200] + ux2*uz2*CHM[M020] + ux2*uy2*CHM[M002] + 4.*ux*uy*uz2*CHM[M110] + 4.*ux*uy2*uz*CHM[M101] + 4.*ux2*uy*uz*CHM[M011] + ux2*uy2*uz2);

        // Compute central Hermite moments from central moments
        // I further assumed that 1st-order CHM = 0 since they
        // are used to store velocity components
        double cs2 = 1./3.;
        double cs4 = cs2*cs2;

        CHM[M200] -= (cs2);
        CHM[M020] -= (cs2);
        CHM[M002] -= (cs2);
        
        CHM[M220] -= (cs2*(CHM[M200] + CHM[M020]) + cs4);
        CHM[M202] -= (cs2*(CHM[M200] + CHM[M002]) + cs4);
        CHM[M022] -= (cs2*(CHM[M020] + CHM[M002]) + cs4);

        CHM[M211] -= (cs2*CHM[M011]);
        CHM[M121] -= (cs2*CHM[M101]);
        CHM[M112] -= (cs2*CHM[M110]);

        CHM[M221] -= (cs2*(CHM[M201] + CHM[M021]));
        CHM[M212] -= (cs2*(CHM[M210] + CHM[M012]));
        CHM[M122] -= (cs2*(CHM[M120] + CHM[M102]));

        CHM[M222] -= (cs2*(CHM[M220] + CHM[M202] + CHM[M022]) + cs4*(CHM[M200] + CHM[M020] + CHM[M002]) + cs2*cs4);

        return CHM;
    }

    // Most optimized version (loop unrolling + redundant terms)
    // Further pre-computing terms is counter-productive (less than 1% gain)
    auto computeCHMopt2(std::array<double, 27> const& pop) {
        std::array<double, 27> CHM;
        std::fill(CHM.begin(), CHM.end(), 0.);

        double A1 = pop[ 9] + pop[10] + pop[11] + pop[12];
        double A2 = pop[23] + pop[24] + pop[25] + pop[26];
        double A3 = pop[ 9] + pop[10] - pop[11] - pop[12];
        double A4 = pop[23] + pop[24] - pop[25] - pop[26];
        double A5 = pop[ 9] - pop[10] + pop[11] - pop[12];
        double A6 = pop[23] - pop[24] + pop[25] - pop[26];
        double A7 = pop[ 9] - pop[10] - pop[11] + pop[12];
        double A8 = pop[23] - pop[24] - pop[25] + pop[26];

        double X_M1 = pop[ 0] + pop[ 3] + pop[ 4] + pop[ 5] + pop[ 6] + A1;
        double X_P1 = pop[14] + pop[17] + pop[18] + pop[19] + pop[20] + A2;
        double X_0  = pop[ 1] + pop[ 2] + pop[ 7] + pop[ 8] + pop[13] + pop[15] + pop[16] + pop[21] + pop[22];

        double Y_M1 = pop[ 1] + pop[ 3] + pop[ 7] + pop[ 8] + pop[ 9] + pop[10] + pop[18] + pop[25] + pop[26];
        double Y_P1 = pop[15] + pop[17] + pop[21] + pop[22] + pop[23] + pop[24] + pop[ 4] + pop[11] + pop[12];

        double Z_M1 = pop[ 2] + pop[ 5] + pop[ 7] + pop[ 9] + pop[11] + pop[20] + pop[22] + pop[24] + pop[26];
        double Z_P1 = pop[16] + pop[19] + pop[21] + pop[23] + pop[25] + pop[ 6] + pop[ 8] + pop[10] + pop[12];

        // Order 0
        CHM[M000] = X_M1 + X_P1 + X_0;
        double invRho = 1. / CHM[M000];

        // Order 6
        CHM[M222] = invRho * (  A1 + A2);
        // Order 5
        CHM[M221] = invRho * (- A5 + A6);
        CHM[M212] = invRho * (- A3 + A4);
        CHM[M122] = invRho * (- A1 + A2);
        // Order 4
        CHM[M220] = invRho * (  pop[ 3] + pop[ 4] + pop[17] + pop[18]) + CHM[M222];
        CHM[M202] = invRho * (  pop[ 5] + pop[ 6] + pop[19] + pop[20]) + CHM[M222];
        CHM[M022] = invRho * (  pop[ 7] + pop[ 8] + pop[21] + pop[22]) + CHM[M222];
        CHM[M211] = invRho * (  A7 + A8);
        CHM[M121] = invRho * (  A5 + A6);
        CHM[M112] = invRho * (  A3 + A4);
        // Order 3
        CHM[M210] = invRho * (- pop[ 3] + pop[ 4] + pop[17] - pop[18]) + CHM[M212];
        CHM[M201] = invRho * (- pop[ 5] + pop[ 6] + pop[19] - pop[20]) + CHM[M221];
        CHM[M021] = invRho * (- pop[ 7] + pop[ 8] + pop[21] - pop[22]) + CHM[M221];
        CHM[M120] = invRho * (- pop[ 3] - pop[ 4] + pop[17] + pop[18]) + CHM[M122];
        CHM[M102] = invRho * (- pop[ 5] - pop[ 6] + pop[19] + pop[20]) + CHM[M122];
        CHM[M012] = invRho * (- pop[ 7] - pop[ 8] + pop[21] + pop[22]) + CHM[M212];
        CHM[M111] = invRho * (- A7 + A8);
        // Order 2
        CHM[M200] = invRho * (X_P1 + X_M1);
        CHM[M020] = invRho * (Y_P1 + Y_M1); 
        CHM[M002] = invRho * (Z_P1 + Z_M1);
        CHM[M110] = invRho * (  pop[ 3] - pop[ 4] + pop[17] - pop[18]) + CHM[M112];
        CHM[M101] = invRho * (  pop[ 5] - pop[ 6] + pop[19] - pop[20]) + CHM[M121];
        CHM[M011] = invRho * (  pop[ 7] - pop[ 8] + pop[21] - pop[22]) + CHM[M211];
        // Order 1
        CHM[M100] = invRho * (X_P1 - X_M1);
        CHM[M010] = invRho * (Y_P1 - Y_M1); 
        CHM[M001] = invRho * (Z_P1 - Z_M1);

        // Compute CMs from RMs using binomial formulas
        double ux   = CHM[M100];
        double uy   = CHM[M010];
        double uz   = CHM[M001];
        double ux2  = CHM[M100]*CHM[M100];
        double uy2  = CHM[M010]*CHM[M010];
        double uz2  = CHM[M001]*CHM[M001];
        double uxyz = CHM[M100]*CHM[M010]*CHM[M001];

        CHM[M200] -= (ux2);
        CHM[M020] -= (uy2);
        CHM[M002] -= (uz2);
        
        CHM[M110] -= (ux*uy);
        CHM[M101] -= (ux*uz);
        CHM[M011] -= (uy*uz);

        CHM[M210] -= (uy*CHM[M200] + 2.*ux*CHM[M110] + ux2*uy);
        CHM[M201] -= (uz*CHM[M200] + 2.*ux*CHM[M101] + ux2*uz);
        CHM[M021] -= (uz*CHM[M020] + 2.*uy*CHM[M011] + uy2*uz);
        CHM[M120] -= (ux*CHM[M020] + 2.*uy*CHM[M110] + ux*uy2);
        CHM[M102] -= (ux*CHM[M002] + 2.*uz*CHM[M101] + ux*uz2);
        CHM[M012] -= (uy*CHM[M002] + 2.*uz*CHM[M011] + uy*uz2);
        
        CHM[M111] -= (uz*CHM[M110] + uy*CHM[M101] + ux*CHM[M011] + uxyz );

        CHM[M220] -= (2.*uy*CHM[M210] + 2.*ux*CHM[M120] + uy2*CHM[M200] + ux2*CHM[M020] + 4.*ux*uy*CHM[M110] + ux2*uy2);
        CHM[M202] -= (2.*uz*CHM[M201] + 2.*ux*CHM[M102] + uz2*CHM[M200] + ux2*CHM[M002] + 4.*ux*uz*CHM[M101] + ux2*uz2);
        CHM[M022] -= (2.*uz*CHM[M021] + 2.*uy*CHM[M012] + uz2*CHM[M020] + uy2*CHM[M002] + 4.*uy*uz*CHM[M011] + uy2*uz2);

        CHM[M211] -= (uz*CHM[M210] + uy*CHM[M201] + 2.*ux*CHM[M111] + uy*uz*CHM[M200] + 2.*ux*uz*CHM[M110] + 2.*ux*uy*CHM[M101] + ux2*CHM[M011] + ux2*uy*uz);
        CHM[M121] -= (uz*CHM[M120] + ux*CHM[M021] + 2.*uy*CHM[M111] + ux*uz*CHM[M020] + 2.*uy*uz*CHM[M110] + 2.*ux*uy*CHM[M011] + uy2*CHM[M101] + ux*uy2*uz);
        CHM[M112] -= (uy*CHM[M102] + ux*CHM[M012] + 2.*uz*CHM[M111] + ux*uy*CHM[M002] + 2.*uy*uz*CHM[M101] + 2.*ux*uz*CHM[M011] + uz2*CHM[M110] + ux*uy*uz2);

        CHM[M221] -= (uz*CHM[M220] + 2.*uy*CHM[M211] + 2.*ux*CHM[M121] + 2.*uy*uz*CHM[M210] + uy2*CHM[M201] + ux2*CHM[M021] + 2.*ux*uz*CHM[M120] + 4.*ux*uy*CHM[M111] + uy2*uz*CHM[M200] + ux2*uz*CHM[M020] + 4.*uxyz*CHM[M110] + 2.*ux*uy2*CHM[M101] + 2.*ux2*uy*CHM[M011] + ux2*uy2*uz);
        CHM[M212] -= (uy*CHM[M202] + 2.*uz*CHM[M211] + 2.*ux*CHM[M112] + 2.*uy*uz*CHM[M201] + uz2*CHM[M210] + ux2*CHM[M012] + 2.*ux*uy*CHM[M102] + 4.*ux*uz*CHM[M111] + uy*uz2*CHM[M200] + ux2*uy*CHM[M002] + 4.*uxyz*CHM[M101] + 2.*ux*uz2*CHM[M110] + 2.*ux2*uz*CHM[M011] + ux2*uy*uz2);
        CHM[M122] -= (ux*CHM[M022] + 2.*uz*CHM[M121] + 2.*uy*CHM[M112] + 2.*ux*uz*CHM[M021] + uz2*CHM[M120] + uy2*CHM[M102] + 2.*ux*uy*CHM[M012] + 4.*uy*uz*CHM[M111] + ux*uz2*CHM[M020] + ux*uy2*CHM[M002] + 4.*uxyz*CHM[M011] + 2.*uy*uz2*CHM[M110] + 2.*uy2*uz*CHM[M101] + ux*uy2*uz2);

        CHM[M222] -= (2.*uz*CHM[M221] + 2.*uy*CHM[M212] + 2.*ux*CHM[M122] + uz2*CHM[M220] + uy2*CHM[M202] + ux2*CHM[M022] + 4.*uy*uz*CHM[M211] + 4.*ux*uz*CHM[M121] + 4.*ux*uy*CHM[M112] + 2.*uy*uz2*CHM[M210] + 2.*uy2*uz*CHM[M201] + 2.*ux2*uz*CHM[M021] + 2.*ux*uz2*CHM[M120] + 2.*ux*uy2*CHM[M102] + 2.*ux2*uy*CHM[M012] + 8.*uxyz*CHM[M111] + uy2*uz2*CHM[M200] + ux2*uz2*CHM[M020] + ux2*uy2*CHM[M002] + 4.*ux*uy*uz2*CHM[M110] + 4.*ux*uy2*uz*CHM[M101] + 4.*ux2*uy*uz*CHM[M011] + ux2*uy2*uz2);

        // Compute central Hermite moments from central moments
        // I further assumed that 1st-order CHM = 0 since they
        // are used to store velocity components
        double cs2 = 1./3.;
        double cs4 = cs2*cs2;

        CHM[M200] -= (cs2);
        CHM[M020] -= (cs2);
        CHM[M002] -= (cs2);
        
        CHM[M220] -= (cs2*(CHM[M200] + CHM[M020]) + cs4);
        CHM[M202] -= (cs2*(CHM[M200] + CHM[M002]) + cs4);
        CHM[M022] -= (cs2*(CHM[M020] + CHM[M002]) + cs4);

        CHM[M211] -= (cs2*CHM[M011]);
        CHM[M121] -= (cs2*CHM[M101]);
        CHM[M112] -= (cs2*CHM[M110]);

        CHM[M221] -= (cs2*(CHM[M201] + CHM[M021]));
        CHM[M212] -= (cs2*(CHM[M210] + CHM[M012]));
        CHM[M122] -= (cs2*(CHM[M120] + CHM[M102]));

        CHM[M222] -= (cs2*(CHM[M220] + CHM[M202] + CHM[M022]) + cs4*(CHM[M200] + CHM[M020] + CHM[M002]) + cs2*cs4);

        return CHM;
    }

    // The function below has only an educational purpose since it is discarded
    // during the compilation because CHMeq are not used in collideCHM()
    auto computeCHMeq() {

        std::array<double, 27> CHMeq;
        // Order 2
        CHMeq[M200] = 0.;
        CHMeq[M020] = 0.;
        CHMeq[M002] = 0.;
        CHMeq[M110] = 0.;
        CHMeq[M101] = 0.;
        CHMeq[M011] = 0.;
        // Order 3
        CHMeq[M210] = 0.;
        CHMeq[M201] = 0.;
        CHMeq[M021] = 0.;
        CHMeq[M120] = 0.;
        CHMeq[M102] = 0.;
        CHMeq[M012] = 0.;
        CHMeq[M111] = 0.;
        // Order 4
        CHMeq[M220] = 0.;
        CHMeq[M202] = 0.;
        CHMeq[M022] = 0.;
        CHMeq[M211] = 0.;
        CHMeq[M121] = 0.;
        CHMeq[M112] = 0.;
        // Order 5
        CHMeq[M221] = 0.;
        CHMeq[M212] = 0.;
        CHMeq[M122] = 0.;
        // Order 6
        CHMeq[M222] = 0.;

        return CHMeq;
    }
};

struct Even : public LBM {

    auto collideCHM(int i, double rho, std::array<double, 3> const& u, std::array<double, 27> const& CHM, std::array<double, 27> const& CHMeq)
    {
        double cs2 = 1./3.;
        double cs4 = cs2*cs2;
        double ux2  = u[0]*u[0];
        double uy2  = u[1]*u[1];
        double uz2  = u[2]*u[2];
        double uxyz = u[0]*u[1]*u[2];

        // Post-collision moments.
        std::array<double, 27> RMcoll;
        std::array<double, 27> HMcoll;
        std::array<double, 27> CHMcoll;

        // Collision in the central Hermite moment space (CHMeq are replaced by their values)
        // Order 2
        if (omegaBulk == 0.) { // Don't adjust bulk viscosity
            CHMcoll[M200] = (1. - omega1) * CHM[M200];
            CHMcoll[M020] = (1. - omega1) * CHM[M020];
            CHMcoll[M002] = (1. - omega1) * CHM[M002];
        }
        else { // Adjust bulk viscosity
            const double omegaBulk = omega1;
            const double omegaMinus = (omegaBulk - omega1)/3.; // Notation used by Fei
            const double omegaPlus  = omegaMinus + omega1;     // Notation used by Fei
            CHMcoll[M200] = CHM[M200] - omegaPlus  * (CHM[M200]) - omegaMinus * (CHM[M020]) - omegaMinus * (CHM[M002]) ;
            CHMcoll[M020] = CHM[M020] - omegaMinus * (CHM[M200]) - omegaPlus  * (CHM[M020]) - omegaMinus * (CHM[M002]) ;
            CHMcoll[M002] = CHM[M002] - omegaMinus * (CHM[M200]) - omegaMinus * (CHM[M020]) - omegaPlus  * (CHM[M002]) ;
        }

        CHMcoll[M110] = (1. - omega2) * CHM[M110];
        CHMcoll[M101] = (1. - omega2) * CHM[M101];
        CHMcoll[M011] = (1. - omega2) * CHM[M011];
        // Order 3
        CHMcoll[M210] = (1. - omega3) * CHM[M210];
        CHMcoll[M201] = (1. - omega3) * CHM[M201];
        CHMcoll[M021] = (1. - omega3) * CHM[M021];
        CHMcoll[M120] = (1. - omega3) * CHM[M120];
        CHMcoll[M102] = (1. - omega3) * CHM[M102];
        CHMcoll[M012] = (1. - omega3) * CHM[M012];
        CHMcoll[M111] = (1. - omega4) * CHM[M111];
        // Order 4
        CHMcoll[M220] = (1. - omega5) * CHM[M220];
        CHMcoll[M202] = (1. - omega5) * CHM[M202];
        CHMcoll[M022] = (1. - omega5) * CHM[M022];
        CHMcoll[M211] = (1. - omega6) * CHM[M211];
        CHMcoll[M121] = (1. - omega6) * CHM[M121];
        CHMcoll[M112] = (1. - omega6) * CHM[M112];
        // Order 5
        CHMcoll[M221] = (1. - omega7) * CHM[M221];
        CHMcoll[M212] = (1. - omega7) * CHM[M212];
        CHMcoll[M122] = (1. - omega7) * CHM[M122];
        // Order 6
        CHMcoll[M222] = (1. - omega8) * CHM[M222];

        // Come back to HMcoll using binomial formulas
        HMcoll[M200] = CHMcoll[M200] + ux2;
        HMcoll[M020] = CHMcoll[M020] + uy2;
        HMcoll[M002] = CHMcoll[M002] + uz2;
        
        HMcoll[M110] = CHMcoll[M110] + u[0]*u[1];
        HMcoll[M101] = CHMcoll[M101] + u[0]*u[2];
        HMcoll[M011] = CHMcoll[M011] + u[1]*u[2];

        HMcoll[M210] = CHMcoll[M210] + u[1]*CHMcoll[M200] + 2.*u[0]*CHMcoll[M110] + ux2*u[1];
        HMcoll[M201] = CHMcoll[M201] + u[2]*CHMcoll[M200] + 2.*u[0]*CHMcoll[M101] + ux2*u[2];
        HMcoll[M021] = CHMcoll[M021] + u[2]*CHMcoll[M020] + 2.*u[1]*CHMcoll[M011] + uy2*u[2];
        HMcoll[M120] = CHMcoll[M120] + u[0]*CHMcoll[M020] + 2.*u[1]*CHMcoll[M110] + u[0]*uy2;
        HMcoll[M102] = CHMcoll[M102] + u[0]*CHMcoll[M002] + 2.*u[2]*CHMcoll[M101] + u[0]*uz2;
        HMcoll[M012] = CHMcoll[M012] + u[1]*CHMcoll[M002] + 2.*u[2]*CHMcoll[M011] + u[1]*uz2;
        
        HMcoll[M111] = CHMcoll[M111] + u[2]*CHMcoll[M110] + u[1]*CHMcoll[M101] + u[0]*CHMcoll[M011] + uxyz ;

        HMcoll[M220] = CHMcoll[M220] + 2.*u[1]*CHMcoll[M210] + 2.*u[0]*CHMcoll[M120] + uy2*CHMcoll[M200] + ux2*CHMcoll[M020] + 4.*u[0]*u[1]*CHMcoll[M110] + ux2*uy2;
        HMcoll[M202] = CHMcoll[M202] + 2.*u[2]*CHMcoll[M201] + 2.*u[0]*CHMcoll[M102] + uz2*CHMcoll[M200] + ux2*CHMcoll[M002] + 4.*u[0]*u[2]*CHMcoll[M101] + ux2*uz2;
        HMcoll[M022] = CHMcoll[M022] + 2.*u[2]*CHMcoll[M021] + 2.*u[1]*CHMcoll[M012] + uz2*CHMcoll[M020] + uy2*CHMcoll[M002] + 4.*u[1]*u[2]*CHMcoll[M011] + uy2*uz2;

        HMcoll[M211] = CHMcoll[M211] + u[2]*CHMcoll[M210] + u[1]*CHMcoll[M201] + 2.*u[0]*CHMcoll[M111] + u[1]*u[2]*CHMcoll[M200] + 2.*u[0]*u[2]*CHMcoll[M110] + 2.*u[0]*u[1]*CHMcoll[M101] + ux2*CHMcoll[M011] + ux2*u[1]*u[2];
        HMcoll[M121] = CHMcoll[M121] + u[2]*CHMcoll[M120] + u[0]*CHMcoll[M021] + 2.*u[1]*CHMcoll[M111] + u[0]*u[2]*CHMcoll[M020] + 2.*u[1]*u[2]*CHMcoll[M110] + 2.*u[0]*u[1]*CHMcoll[M011] + uy2*CHMcoll[M101] + u[0]*uy2*u[2];
        HMcoll[M112] = CHMcoll[M112] + u[1]*CHMcoll[M102] + u[0]*CHMcoll[M012] + 2.*u[2]*CHMcoll[M111] + u[0]*u[1]*CHMcoll[M002] + 2.*u[1]*u[2]*CHMcoll[M101] + 2.*u[0]*u[2]*CHMcoll[M011] + uz2*CHMcoll[M110] + u[0]*u[1]*uz2;

        HMcoll[M221] = CHMcoll[M221] + u[2]*CHMcoll[M220] + 2.*u[1]*CHMcoll[M211] + 2.*u[0]*CHMcoll[M121] + 2.*u[1]*u[2]*CHMcoll[M210] + uy2*CHMcoll[M201] + ux2*CHMcoll[M021] + 2.*u[0]*u[2]*CHMcoll[M120] + 4.*u[0]*u[1]*CHMcoll[M111] + uy2*u[2]*CHMcoll[M200] + ux2*u[2]*CHMcoll[M020] + 4.*uxyz*CHMcoll[M110] + 2.*u[0]*uy2*CHMcoll[M101] + 2.*ux2*u[1]*CHMcoll[M011] + ux2*uy2*u[2];
        HMcoll[M212] = CHMcoll[M212] + u[1]*CHMcoll[M202] + 2.*u[2]*CHMcoll[M211] + 2.*u[0]*CHMcoll[M112] + 2.*u[1]*u[2]*CHMcoll[M201] + uz2*CHMcoll[M210] + ux2*CHMcoll[M012] + 2.*u[0]*u[1]*CHMcoll[M102] + 4.*u[0]*u[2]*CHMcoll[M111] + u[1]*uz2*CHMcoll[M200] + ux2*u[1]*CHMcoll[M002] + 4.*uxyz*CHMcoll[M101] + 2.*u[0]*uz2*CHMcoll[M110] + 2.*ux2*u[2]*CHMcoll[M011] + ux2*u[1]*uz2;
        HMcoll[M122] = CHMcoll[M122] + u[0]*CHMcoll[M022] + 2.*u[2]*CHMcoll[M121] + 2.*u[1]*CHMcoll[M112] + 2.*u[0]*u[2]*CHMcoll[M021] + uz2*CHMcoll[M120] + uy2*CHMcoll[M102] + 2.*u[0]*u[1]*CHMcoll[M012] + 4.*u[1]*u[2]*CHMcoll[M111] + u[0]*uz2*CHMcoll[M020] + u[0]*uy2*CHMcoll[M002] + 4.*uxyz*CHMcoll[M011] + 2.*u[1]*uz2*CHMcoll[M110] + 2.*uy2*u[2]*CHMcoll[M101] + u[0]*uy2*uz2;

        HMcoll[M222] = CHMcoll[M222] + 2.*u[2]*CHMcoll[M221] + 2.*u[1]*CHMcoll[M212] + 2.*u[0]*CHMcoll[M122] + uz2*CHMcoll[M220] + uy2*CHMcoll[M202] + ux2*CHMcoll[M022] + 4.*u[1]*u[2]*CHMcoll[M211] + 4.*u[0]*u[2]*CHMcoll[M121] + 4.*u[0]*u[1]*CHMcoll[M112] + 2.*u[1]*uz2*CHMcoll[M210] + 2.*uy2*u[2]*CHMcoll[M201] + 2.*ux2*u[2]*CHMcoll[M021] + 2.*u[0]*uz2*CHMcoll[M120] + 2.*u[0]*uy2*CHMcoll[M102] + 2.*ux2*u[1]*CHMcoll[M012] + 8.*uxyz*CHMcoll[M111] + uy2*uz2*CHMcoll[M200] + ux2*uz2*CHMcoll[M020] + ux2*uy2*CHMcoll[M002] + 4.*u[0]*u[1]*uz2*CHMcoll[M110] + 4.*u[0]*uy2*u[2]*CHMcoll[M101] + 4.*ux2*u[1]*u[2]*CHMcoll[M011] + ux2*uy2*uz2;

        // Come back to RMcoll using relationships between HMs and RMs
        RMcoll[M200] = HMcoll[M200] + cs2;
        RMcoll[M020] = HMcoll[M020] + cs2;
        RMcoll[M002] = HMcoll[M002] + cs2;
        
        RMcoll[M110] = HMcoll[M110];
        RMcoll[M101] = HMcoll[M101];
        RMcoll[M011] = HMcoll[M011];

        RMcoll[M210] = HMcoll[M210] + cs2*u[1];
        RMcoll[M201] = HMcoll[M201] + cs2*u[2];
        RMcoll[M021] = HMcoll[M021] + cs2*u[2];
        RMcoll[M120] = HMcoll[M120] + cs2*u[0];
        RMcoll[M102] = HMcoll[M102] + cs2*u[0];
        RMcoll[M012] = HMcoll[M012] + cs2*u[1];
        
        RMcoll[M111] = HMcoll[M111];

        RMcoll[M220] = HMcoll[M220] + cs2*(HMcoll[M200] + HMcoll[M020]) + cs4;
        RMcoll[M202] = HMcoll[M202] + cs2*(HMcoll[M200] + HMcoll[M002]) + cs4;
        RMcoll[M022] = HMcoll[M022] + cs2*(HMcoll[M020] + HMcoll[M002]) + cs4;

        RMcoll[M211] = HMcoll[M211] + cs2*HMcoll[M011];
        RMcoll[M121] = HMcoll[M121] + cs2*HMcoll[M101];
        RMcoll[M112] = HMcoll[M112] + cs2*HMcoll[M110];

        RMcoll[M221] = HMcoll[M221] + cs2*(HMcoll[M201] + HMcoll[M021]) + cs4*u[2];
        RMcoll[M212] = HMcoll[M212] + cs2*(HMcoll[M210] + HMcoll[M012]) + cs4*u[1];
        RMcoll[M122] = HMcoll[M122] + cs2*(HMcoll[M120] + HMcoll[M102]) + cs4*u[0];

        RMcoll[M222] = HMcoll[M222] + cs2*(HMcoll[M220] + HMcoll[M202] + HMcoll[M022]) + cs4*(HMcoll[M200] + HMcoll[M020] + HMcoll[M002]) + cs2*cs4;

        // Optimization based on symmetries between populations and their opposite counterpart
        std::array<double, 27> foutRM;

        foutRM[F000] = rho *(1. - RMcoll[M200] - RMcoll[M020] - RMcoll[M002] + RMcoll[M220] + RMcoll[M202] + RMcoll[M022] - RMcoll[M222]);
        
        foutRM[FP00] = 0.5*rho * ( u[0] + RMcoll[M200] - RMcoll[M120] - RMcoll[M102] - RMcoll[M220] - RMcoll[M202] + RMcoll[M122] + RMcoll[M222]);
        foutRM[FM00] =     rho * (-u[0]                + RMcoll[M120] + RMcoll[M102]                               - RMcoll[M122])+ foutRM[FP00];

        foutRM[F0P0] = 0.5*rho * ( u[1] + RMcoll[M020] - RMcoll[M210] - RMcoll[M012] - RMcoll[M220] - RMcoll[M022] + RMcoll[M212] + RMcoll[M222]);
        foutRM[F0M0] =     rho * (-u[1]                + RMcoll[M210] + RMcoll[M012]                               - RMcoll[M212])+ foutRM[F0P0];

        foutRM[F00P] = 0.5*rho * ( u[2] + RMcoll[M002] - RMcoll[M201] - RMcoll[M021] - RMcoll[M202] - RMcoll[M022] + RMcoll[M221] + RMcoll[M222]);
        foutRM[F00M] =     rho * (-u[2]                + RMcoll[M201] + RMcoll[M021]                               - RMcoll[M221])+ foutRM[F00P];

        foutRM[FPP0] = 0.25*rho * ( RMcoll[M110] + RMcoll[M210] + RMcoll[M120] - RMcoll[M112] + RMcoll[M220] - RMcoll[M212] - RMcoll[M122] - RMcoll[M222]);
        foutRM[FMP0] =  0.5*rho * (-RMcoll[M110]                - RMcoll[M120] + RMcoll[M112]                               + RMcoll[M122])+ foutRM[FPP0];
        foutRM[FPM0] =  0.5*rho * (-RMcoll[M110] - RMcoll[M210]                + RMcoll[M112]                + RMcoll[M212]               )+ foutRM[FPP0];
        foutRM[FMM0] =  0.5*rho * (              - RMcoll[M210] - RMcoll[M120]                               + RMcoll[M212] + RMcoll[M122])+ foutRM[FPP0];

        foutRM[FP0P] = 0.25*rho * ( RMcoll[M101] + RMcoll[M201] + RMcoll[M102] - RMcoll[M121] + RMcoll[M202] - RMcoll[M221] - RMcoll[M122] - RMcoll[M222]);
        foutRM[FM0P] =  0.5*rho * (-RMcoll[M101]                - RMcoll[M102] + RMcoll[M121]                               + RMcoll[M122])+ foutRM[FP0P];
        foutRM[FP0M] =  0.5*rho * (-RMcoll[M101] - RMcoll[M201]                + RMcoll[M121]                + RMcoll[M221]               )+ foutRM[FP0P];
        foutRM[FM0M] =  0.5*rho * (              - RMcoll[M201] - RMcoll[M102]                               + RMcoll[M221] + RMcoll[M122])+ foutRM[FP0P];

        foutRM[F0PP] = 0.25*rho * ( RMcoll[M011] + RMcoll[M021] + RMcoll[M012] - RMcoll[M211] + RMcoll[M022] - RMcoll[M221] - RMcoll[M212] - RMcoll[M222]);
        foutRM[F0MP] =  0.5*rho * (-RMcoll[M011]                - RMcoll[M012] + RMcoll[M211]                               + RMcoll[M212])+ foutRM[F0PP];
        foutRM[F0PM] =  0.5*rho * (-RMcoll[M011] - RMcoll[M021]                + RMcoll[M211]                + RMcoll[M221]               )+ foutRM[F0PP];
        foutRM[F0MM] =  0.5*rho * (              - RMcoll[M021] - RMcoll[M012]                               + RMcoll[M221] + RMcoll[M212])+ foutRM[F0PP];

        foutRM[FPPP] = 0.125*rho * ( RMcoll[M111] + RMcoll[M211] + RMcoll[M121] + RMcoll[M112] + RMcoll[M221] + RMcoll[M212] + RMcoll[M122] + RMcoll[M222]);
        foutRM[FMPP] =  0.25*rho * (-RMcoll[M111]                - RMcoll[M121] - RMcoll[M112]                               - RMcoll[M122])+ foutRM[FPPP];
        foutRM[FPMP] =  0.25*rho * (-RMcoll[M111] - RMcoll[M211]                - RMcoll[M112]                - RMcoll[M212]               )+ foutRM[FPPP];
        foutRM[FPPM] =  0.25*rho * (-RMcoll[M111] - RMcoll[M211] - RMcoll[M121]                - RMcoll[M221]                              )+ foutRM[FPPP];
        foutRM[FMMP] =  0.25*rho * (              - RMcoll[M211] - RMcoll[M121]                               - RMcoll[M212] - RMcoll[M122])+ foutRM[FPPP];
        foutRM[FMPM] =  0.25*rho * (              - RMcoll[M211]                - RMcoll[M112] - RMcoll[M221]                - RMcoll[M122])+ foutRM[FPPP];
        foutRM[FPMM] =  0.25*rho * (                             - RMcoll[M121] - RMcoll[M112] - RMcoll[M221] - RMcoll[M212]               )+ foutRM[FPPP];
        foutRM[FMMM] =  0.25*rho * (-RMcoll[M111]                                              - RMcoll[M221] - RMcoll[M212] - RMcoll[M122])+ foutRM[FPPP];

        f(i,F000) = foutRM[F000];

        f(i,FP00) = foutRM[FM00];
        f(i,FM00) = foutRM[FP00];

        f(i,F0P0) = foutRM[F0M0];
        f(i,F0M0) = foutRM[F0P0];

        f(i,F00P) = foutRM[F00M];
        f(i,F00M) = foutRM[F00P];

        f(i,FPP0) = foutRM[FMM0];
        f(i,FMP0) = foutRM[FPM0];
        f(i,FPM0) = foutRM[FMP0];
        f(i,FMM0) = foutRM[FPP0];

        f(i,FP0P) = foutRM[FM0M];
        f(i,FM0P) = foutRM[FP0M];
        f(i,FP0M) = foutRM[FM0P];
        f(i,FM0M) = foutRM[FP0P];

        f(i,F0PP) = foutRM[F0MM];
        f(i,F0MP) = foutRM[F0PM];
        f(i,F0PM) = foutRM[F0MP];
        f(i,F0MM) = foutRM[F0PP];

        f(i,FPPP) = foutRM[FMMM];
        f(i,FMPP) = foutRM[FPMM];
        f(i,FPMP) = foutRM[FMPM];
        f(i,FPPM) = foutRM[FMMP];
        f(i,FMMP) = foutRM[FPPM];
        f(i,FMPM) = foutRM[FPMP];
        f(i,FPMM) = foutRM[FMPP];
        f(i,FMMM) = foutRM[FPPP];
    }


    void iterateCHM(double& f0) {
        int i = &f0 - lattice;
        if (flag[i] == CellType::bulk) {
            std::array<double, 27> pop;
            for (int k = 0; k < 27; ++k) {
                pop[k] = f(i, k);
            }
/*          auto CHM = computeCHM(pop);
            auto CHM = computeCHMopt(pop);*/
            auto CHM = computeCHMopt2(pop);
            double rho = CHM[M000];
            std::array<double, 3> u = {CHM[M100], CHM[M010], CHM[M001]};
            auto CHMeq = computeCHMeq();
            collideCHM(i, rho, u, CHM, CHMeq);
        }
    }

    void operator() (double& f0) {
        iterateCHM(f0);
    }
};

struct Odd : public LBM {

    auto collideCHM(std::array<double, 27>& pop, double rho, std::array<double, 3> const& u,
        std::array<double, 27> const& CHM, std::array<double, 27> const& CHMeq)
    {
        double cs2 = 1./3.;
        double cs4 = cs2*cs2;
        double ux2  = u[0]*u[0];
        double uy2  = u[1]*u[1];
        double uz2  = u[2]*u[2];
        double uxyz = u[0]*u[1]*u[2];

        // Post-collision moments.
        std::array<double, 27> RMcoll;
        std::array<double, 27> HMcoll;
        std::array<double, 27> CHMcoll;

        // Collision in the central Hermite moment space (CHMeq are replaced by their values)
        // Order 2
        if (omegaBulk == 0.) { // Don't adjust bulk viscosity
            CHMcoll[M200] = (1. - omega1) * CHM[M200];
            CHMcoll[M020] = (1. - omega1) * CHM[M020];
            CHMcoll[M002] = (1. - omega1) * CHM[M002];
        }
        else { // Adjust bulk viscosity
            const double omegaBulk = omega1;
            const double omegaMinus = (omegaBulk - omega1)/3.; // Notation used by Fei
            const double omegaPlus  = omegaMinus + omega1;     // Notation used by Fei
            CHMcoll[M200] = CHM[M200] - omegaPlus  * (CHM[M200]) - omegaMinus * (CHM[M020]) - omegaMinus * (CHM[M002]) ;
            CHMcoll[M020] = CHM[M020] - omegaMinus * (CHM[M200]) - omegaPlus  * (CHM[M020]) - omegaMinus * (CHM[M002]) ;
            CHMcoll[M002] = CHM[M002] - omegaMinus * (CHM[M200]) - omegaMinus * (CHM[M020]) - omegaPlus  * (CHM[M002]) ;
        }

        CHMcoll[M110] = (1. - omega2) * CHM[M110];
        CHMcoll[M101] = (1. - omega2) * CHM[M101];
        CHMcoll[M011] = (1. - omega2) * CHM[M011];
        // Order 3
        CHMcoll[M210] = (1. - omega3) * CHM[M210];
        CHMcoll[M201] = (1. - omega3) * CHM[M201];
        CHMcoll[M021] = (1. - omega3) * CHM[M021];
        CHMcoll[M120] = (1. - omega3) * CHM[M120];
        CHMcoll[M102] = (1. - omega3) * CHM[M102];
        CHMcoll[M012] = (1. - omega3) * CHM[M012];
        CHMcoll[M111] = (1. - omega4) * CHM[M111];
        // Order 4
        CHMcoll[M220] = (1. - omega5) * CHM[M220];
        CHMcoll[M202] = (1. - omega5) * CHM[M202];
        CHMcoll[M022] = (1. - omega5) * CHM[M022];
        CHMcoll[M211] = (1. - omega6) * CHM[M211];
        CHMcoll[M121] = (1. - omega6) * CHM[M121];
        CHMcoll[M112] = (1. - omega6) * CHM[M112];
        // Order 5
        CHMcoll[M221] = (1. - omega7) * CHM[M221];
        CHMcoll[M212] = (1. - omega7) * CHM[M212];
        CHMcoll[M122] = (1. - omega7) * CHM[M122];
        // Order 6
        CHMcoll[M222] = (1. - omega8) * CHM[M222];

        // Come back to HMcoll using binomial formulas
        HMcoll[M200] = CHMcoll[M200] + ux2;
        HMcoll[M020] = CHMcoll[M020] + uy2;
        HMcoll[M002] = CHMcoll[M002] + uz2;
        
        HMcoll[M110] = CHMcoll[M110] + u[0]*u[1];
        HMcoll[M101] = CHMcoll[M101] + u[0]*u[2];
        HMcoll[M011] = CHMcoll[M011] + u[1]*u[2];

        HMcoll[M210] = CHMcoll[M210] + u[1]*CHMcoll[M200] + 2.*u[0]*CHMcoll[M110] + ux2*u[1];
        HMcoll[M201] = CHMcoll[M201] + u[2]*CHMcoll[M200] + 2.*u[0]*CHMcoll[M101] + ux2*u[2];
        HMcoll[M021] = CHMcoll[M021] + u[2]*CHMcoll[M020] + 2.*u[1]*CHMcoll[M011] + uy2*u[2];
        HMcoll[M120] = CHMcoll[M120] + u[0]*CHMcoll[M020] + 2.*u[1]*CHMcoll[M110] + u[0]*uy2;
        HMcoll[M102] = CHMcoll[M102] + u[0]*CHMcoll[M002] + 2.*u[2]*CHMcoll[M101] + u[0]*uz2;
        HMcoll[M012] = CHMcoll[M012] + u[1]*CHMcoll[M002] + 2.*u[2]*CHMcoll[M011] + u[1]*uz2;
        
        HMcoll[M111] = CHMcoll[M111] + u[2]*CHMcoll[M110] + u[1]*CHMcoll[M101] + u[0]*CHMcoll[M011] + uxyz ;

        HMcoll[M220] = CHMcoll[M220] + 2.*u[1]*CHMcoll[M210] + 2.*u[0]*CHMcoll[M120] + uy2*CHMcoll[M200] + ux2*CHMcoll[M020] + 4.*u[0]*u[1]*CHMcoll[M110] + ux2*uy2;
        HMcoll[M202] = CHMcoll[M202] + 2.*u[2]*CHMcoll[M201] + 2.*u[0]*CHMcoll[M102] + uz2*CHMcoll[M200] + ux2*CHMcoll[M002] + 4.*u[0]*u[2]*CHMcoll[M101] + ux2*uz2;
        HMcoll[M022] = CHMcoll[M022] + 2.*u[2]*CHMcoll[M021] + 2.*u[1]*CHMcoll[M012] + uz2*CHMcoll[M020] + uy2*CHMcoll[M002] + 4.*u[1]*u[2]*CHMcoll[M011] + uy2*uz2;

        HMcoll[M211] = CHMcoll[M211] + u[2]*CHMcoll[M210] + u[1]*CHMcoll[M201] + 2.*u[0]*CHMcoll[M111] + u[1]*u[2]*CHMcoll[M200] + 2.*u[0]*u[2]*CHMcoll[M110] + 2.*u[0]*u[1]*CHMcoll[M101] + ux2*CHMcoll[M011] + ux2*u[1]*u[2];
        HMcoll[M121] = CHMcoll[M121] + u[2]*CHMcoll[M120] + u[0]*CHMcoll[M021] + 2.*u[1]*CHMcoll[M111] + u[0]*u[2]*CHMcoll[M020] + 2.*u[1]*u[2]*CHMcoll[M110] + 2.*u[0]*u[1]*CHMcoll[M011] + uy2*CHMcoll[M101] + u[0]*uy2*u[2];
        HMcoll[M112] = CHMcoll[M112] + u[1]*CHMcoll[M102] + u[0]*CHMcoll[M012] + 2.*u[2]*CHMcoll[M111] + u[0]*u[1]*CHMcoll[M002] + 2.*u[1]*u[2]*CHMcoll[M101] + 2.*u[0]*u[2]*CHMcoll[M011] + uz2*CHMcoll[M110] + u[0]*u[1]*uz2;

        HMcoll[M221] = CHMcoll[M221] + u[2]*CHMcoll[M220] + 2.*u[1]*CHMcoll[M211] + 2.*u[0]*CHMcoll[M121] + 2.*u[1]*u[2]*CHMcoll[M210] + uy2*CHMcoll[M201] + ux2*CHMcoll[M021] + 2.*u[0]*u[2]*CHMcoll[M120] + 4.*u[0]*u[1]*CHMcoll[M111] + uy2*u[2]*CHMcoll[M200] + ux2*u[2]*CHMcoll[M020] + 4.*uxyz*CHMcoll[M110] + 2.*u[0]*uy2*CHMcoll[M101] + 2.*ux2*u[1]*CHMcoll[M011] + ux2*uy2*u[2];
        HMcoll[M212] = CHMcoll[M212] + u[1]*CHMcoll[M202] + 2.*u[2]*CHMcoll[M211] + 2.*u[0]*CHMcoll[M112] + 2.*u[1]*u[2]*CHMcoll[M201] + uz2*CHMcoll[M210] + ux2*CHMcoll[M012] + 2.*u[0]*u[1]*CHMcoll[M102] + 4.*u[0]*u[2]*CHMcoll[M111] + u[1]*uz2*CHMcoll[M200] + ux2*u[1]*CHMcoll[M002] + 4.*uxyz*CHMcoll[M101] + 2.*u[0]*uz2*CHMcoll[M110] + 2.*ux2*u[2]*CHMcoll[M011] + ux2*u[1]*uz2;
        HMcoll[M122] = CHMcoll[M122] + u[0]*CHMcoll[M022] + 2.*u[2]*CHMcoll[M121] + 2.*u[1]*CHMcoll[M112] + 2.*u[0]*u[2]*CHMcoll[M021] + uz2*CHMcoll[M120] + uy2*CHMcoll[M102] + 2.*u[0]*u[1]*CHMcoll[M012] + 4.*u[1]*u[2]*CHMcoll[M111] + u[0]*uz2*CHMcoll[M020] + u[0]*uy2*CHMcoll[M002] + 4.*uxyz*CHMcoll[M011] + 2.*u[1]*uz2*CHMcoll[M110] + 2.*uy2*u[2]*CHMcoll[M101] + u[0]*uy2*uz2;

        HMcoll[M222] = CHMcoll[M222] + 2.*u[2]*CHMcoll[M221] + 2.*u[1]*CHMcoll[M212] + 2.*u[0]*CHMcoll[M122] + uz2*CHMcoll[M220] + uy2*CHMcoll[M202] + ux2*CHMcoll[M022] + 4.*u[1]*u[2]*CHMcoll[M211] + 4.*u[0]*u[2]*CHMcoll[M121] + 4.*u[0]*u[1]*CHMcoll[M112] + 2.*u[1]*uz2*CHMcoll[M210] + 2.*uy2*u[2]*CHMcoll[M201] + 2.*ux2*u[2]*CHMcoll[M021] + 2.*u[0]*uz2*CHMcoll[M120] + 2.*u[0]*uy2*CHMcoll[M102] + 2.*ux2*u[1]*CHMcoll[M012] + 8.*uxyz*CHMcoll[M111] + uy2*uz2*CHMcoll[M200] + ux2*uz2*CHMcoll[M020] + ux2*uy2*CHMcoll[M002] + 4.*u[0]*u[1]*uz2*CHMcoll[M110] + 4.*u[0]*uy2*u[2]*CHMcoll[M101] + 4.*ux2*u[1]*u[2]*CHMcoll[M011] + ux2*uy2*uz2;

        // Come back to RMcoll using relationships between HMs and RMs
        RMcoll[M200] = HMcoll[M200] + cs2;
        RMcoll[M020] = HMcoll[M020] + cs2;
        RMcoll[M002] = HMcoll[M002] + cs2;
        
        RMcoll[M110] = HMcoll[M110];
        RMcoll[M101] = HMcoll[M101];
        RMcoll[M011] = HMcoll[M011];

        RMcoll[M210] = HMcoll[M210] + cs2*u[1];
        RMcoll[M201] = HMcoll[M201] + cs2*u[2];
        RMcoll[M021] = HMcoll[M021] + cs2*u[2];
        RMcoll[M120] = HMcoll[M120] + cs2*u[0];
        RMcoll[M102] = HMcoll[M102] + cs2*u[0];
        RMcoll[M012] = HMcoll[M012] + cs2*u[1];
        
        RMcoll[M111] = HMcoll[M111];

        RMcoll[M220] = HMcoll[M220] + cs2*(HMcoll[M200] + HMcoll[M020]) + cs4;
        RMcoll[M202] = HMcoll[M202] + cs2*(HMcoll[M200] + HMcoll[M002]) + cs4;
        RMcoll[M022] = HMcoll[M022] + cs2*(HMcoll[M020] + HMcoll[M002]) + cs4;

        RMcoll[M211] = HMcoll[M211] + cs2*HMcoll[M011];
        RMcoll[M121] = HMcoll[M121] + cs2*HMcoll[M101];
        RMcoll[M112] = HMcoll[M112] + cs2*HMcoll[M110];

        RMcoll[M221] = HMcoll[M221] + cs2*(HMcoll[M201] + HMcoll[M021]) + cs4*u[2];
        RMcoll[M212] = HMcoll[M212] + cs2*(HMcoll[M210] + HMcoll[M012]) + cs4*u[1];
        RMcoll[M122] = HMcoll[M122] + cs2*(HMcoll[M120] + HMcoll[M102]) + cs4*u[0];

        RMcoll[M222] = HMcoll[M222] + cs2*(HMcoll[M220] + HMcoll[M202] + HMcoll[M022]) + cs4*(HMcoll[M200] + HMcoll[M020] + HMcoll[M002]) + cs2*cs4;

        // Optimization based on symmetries between populations and their opposite counterpart
        pop[F000] = rho *(1. - RMcoll[M200] - RMcoll[M020] - RMcoll[M002] + RMcoll[M220] + RMcoll[M202] + RMcoll[M022] - RMcoll[M222]);
        
        pop[FP00] = 0.5*rho * ( u[0] + RMcoll[M200] - RMcoll[M120] - RMcoll[M102] - RMcoll[M220] - RMcoll[M202] + RMcoll[M122] + RMcoll[M222]);
        pop[FM00] =     rho * (-u[0]                + RMcoll[M120] + RMcoll[M102]                               - RMcoll[M122])+ pop[FP00];

        pop[F0P0] = 0.5*rho * ( u[1] + RMcoll[M020] - RMcoll[M210] - RMcoll[M012] - RMcoll[M220] - RMcoll[M022] + RMcoll[M212] + RMcoll[M222]);
        pop[F0M0] =     rho * (-u[1]                + RMcoll[M210] + RMcoll[M012]                               - RMcoll[M212])+ pop[F0P0];

        pop[F00P] = 0.5*rho * ( u[2] + RMcoll[M002] - RMcoll[M201] - RMcoll[M021] - RMcoll[M202] - RMcoll[M022] + RMcoll[M221] + RMcoll[M222]);
        pop[F00M] =     rho * (-u[2]                + RMcoll[M201] + RMcoll[M021]                               - RMcoll[M221])+ pop[F00P];

        pop[FPP0] = 0.25*rho * ( RMcoll[M110] + RMcoll[M210] + RMcoll[M120] - RMcoll[M112] + RMcoll[M220] - RMcoll[M212] - RMcoll[M122] - RMcoll[M222]);
        pop[FMP0] =  0.5*rho * (-RMcoll[M110]                - RMcoll[M120] + RMcoll[M112]                               + RMcoll[M122])+ pop[FPP0];
        pop[FPM0] =  0.5*rho * (-RMcoll[M110] - RMcoll[M210]                + RMcoll[M112]                + RMcoll[M212]               )+ pop[FPP0];
        pop[FMM0] =  0.5*rho * (              - RMcoll[M210] - RMcoll[M120]                               + RMcoll[M212] + RMcoll[M122])+ pop[FPP0];

        pop[FP0P] = 0.25*rho * ( RMcoll[M101] + RMcoll[M201] + RMcoll[M102] - RMcoll[M121] + RMcoll[M202] - RMcoll[M221] - RMcoll[M122] - RMcoll[M222]);
        pop[FM0P] =  0.5*rho * (-RMcoll[M101]                - RMcoll[M102] + RMcoll[M121]                               + RMcoll[M122])+ pop[FP0P];
        pop[FP0M] =  0.5*rho * (-RMcoll[M101] - RMcoll[M201]                + RMcoll[M121]                + RMcoll[M221]               )+ pop[FP0P];
        pop[FM0M] =  0.5*rho * (              - RMcoll[M201] - RMcoll[M102]                               + RMcoll[M221] + RMcoll[M122])+ pop[FP0P];

        pop[F0PP] = 0.25*rho * ( RMcoll[M011] + RMcoll[M021] + RMcoll[M012] - RMcoll[M211] + RMcoll[M022] - RMcoll[M221] - RMcoll[M212] - RMcoll[M222]);
        pop[F0MP] =  0.5*rho * (-RMcoll[M011]                - RMcoll[M012] + RMcoll[M211]                               + RMcoll[M212])+ pop[F0PP];
        pop[F0PM] =  0.5*rho * (-RMcoll[M011] - RMcoll[M021]                + RMcoll[M211]                + RMcoll[M221]               )+ pop[F0PP];
        pop[F0MM] =  0.5*rho * (              - RMcoll[M021] - RMcoll[M012]                               + RMcoll[M221] + RMcoll[M212])+ pop[F0PP];

        pop[FPPP] = 0.125*rho * ( RMcoll[M111] + RMcoll[M211] + RMcoll[M121] + RMcoll[M112] + RMcoll[M221] + RMcoll[M212] + RMcoll[M122] + RMcoll[M222]);
        pop[FMPP] =  0.25*rho * (-RMcoll[M111]                - RMcoll[M121] - RMcoll[M112]                               - RMcoll[M122])+ pop[FPPP];
        pop[FPMP] =  0.25*rho * (-RMcoll[M111] - RMcoll[M211]                - RMcoll[M112]                - RMcoll[M212]               )+ pop[FPPP];
        pop[FPPM] =  0.25*rho * (-RMcoll[M111] - RMcoll[M211] - RMcoll[M121]                - RMcoll[M221]                              )+ pop[FPPP];
        pop[FMMP] =  0.25*rho * (              - RMcoll[M211] - RMcoll[M121]                               - RMcoll[M212] - RMcoll[M122])+ pop[FPPP];
        pop[FMPM] =  0.25*rho * (              - RMcoll[M211]                - RMcoll[M112] - RMcoll[M221]                - RMcoll[M122])+ pop[FPPP];
        pop[FPMM] =  0.25*rho * (                             - RMcoll[M121] - RMcoll[M112] - RMcoll[M221] - RMcoll[M212]               )+ pop[FPPP];
        pop[FMMM] =  0.25*rho * (-RMcoll[M111]                                              - RMcoll[M221] - RMcoll[M212] - RMcoll[M122])+ pop[FPPP];
    }

    auto streamingPull(int i, int iX, int iY, int iZ, std::array<double, 27>& pop)
    {
        if(periodic){
            for (int k = 0; k < 27; ++k) {
                int XX = (iX - c[k][0] + dim.nx) % dim.nx;
                int YY = (iY - c[k][1] + dim.ny) % dim.ny;
                int ZZ = (iZ - c[k][2] + dim.nz) % dim.nz;
                size_t nb = xyz_to_i(XX, YY, ZZ);
                CellType nbCellType = flag[nb];
                if (nbCellType == CellType::bounce_back) {
                    pop[k] = f(i, k) + f(nb, opp[k]);
                }
                else {
                    pop[k] = f(nb, opp[k]);
                }
            }
        }
        else {
            for (int k = 0; k < 27; ++k) {
                int XX = iX - c[k][0];
                int YY = iY - c[k][1];
                int ZZ = iZ - c[k][2];
                size_t nb = xyz_to_i(XX, YY, ZZ);
                CellType nbCellType = flag[nb];
                if (nbCellType == CellType::bounce_back) {
                    pop[k] = f(i, k) + f(nb, opp[k]);
                }
                else {
                    pop[k] = f(nb, opp[k]);
                }
            }
        }
    }

    auto streamingPush(int i, int iX, int iY, int iZ, std::array<double, 27>& pop)
    {
        if(periodic){
            for (int k = 0; k < 27; ++k) {
                int XX = (iX + c[k][0] + dim.nx) % dim.nx;
                int YY = (iY + c[k][1] + dim.ny) % dim.ny;
                int ZZ = (iZ + c[k][2] + dim.nz) % dim.nz;
                size_t nb = xyz_to_i(XX, YY, ZZ);
                CellType nbCellType = flag[nb];
                if (nbCellType == CellType::bounce_back) {
                    f(i, opp[k]) = pop[k] + f(nb, k);
                }
                else {
                    f(nb, k) = pop[k];
                }
            }
        }
        else {
            for (int k = 0; k < 27; ++k) {
                int XX = iX + c[k][0];
                int YY = iY + c[k][1];
                int ZZ = iZ + c[k][2];
                size_t nb = xyz_to_i(XX, YY, ZZ);
                CellType nbCellType = flag[nb];
                if (nbCellType == CellType::bounce_back) {
                    f(i, opp[k]) = pop[k] + f(nb, k);
                }
                else {
                    f(nb, k) = pop[k];
                }
            }
        }
    }

    void iterateCHM(double& f0) {
        int i = &f0 - lattice;
        auto[iX, iY, iZ] = i_to_xyz(i);
        CellType cellType = flag[i];

        if (cellType == CellType::bulk) {
            std::array<double, 27> pop;

            streamingPull(i, iX, iY, iZ, pop);

/*          auto CHM = computeCHM(pop);
            auto CHM = computeCHMopt(pop);*/
            auto CHM = computeCHMopt2(pop);
            double rho = CHM[M000];
            std::array<double, 3> u = {CHM[M100], CHM[M010], CHM[M001]};
            auto CHMeq = computeCHMeq();

            collideCHM(pop, rho, u, CHM, CHMeq);

            streamingPush(i, iX, iY, iZ, pop);
        }
    }

    void operator() (double& f0) {
        iterateCHM(f0);
    }
};

} // namespace aa_soa_chm
