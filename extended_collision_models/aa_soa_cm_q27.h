// *****************************************************************************
// STLBM SOFTWARE LIBRARY

// Copyright © 2020 University of Geneva
// Authors: Jonas Latt, Christophe Coreixas, Joël Beny
// Contact: Jonas.Latt@unige.ch

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// *****************************************************************************

#pragma once

#include "lbm_q27.h"
#include <vector>
#include <array>
#include <tuple>

namespace aa_soa_cm {

struct LBM {
    using CellData = double;
    static size_t sizeOfLattice(size_t nelem) { return 27 * nelem; }

    CellData* lattice;
    CellType* flag;
    std::array<int, 3>* c;
    int* opp;
    double* t;
    double omega1;
    Dim dim;
    LBModel model;
    bool periodic = false;
    double omegaBulk = 0.;
    double omega2 = omega1;
    double omega3 = omega1;
    double omega4 = omega1;
    double omega5 = omega1;
    double omega6 = omega1;
    double omega7 = omega1;
    double omega8 = omega1;

    auto i_to_xyz (int i) const {
        int iX = i / (dim.ny * dim.nz);
        int remainder = i % (dim.ny * dim.nz);
        int iY = remainder / dim.nz;
        int iZ = remainder % dim.nz;
        return std::make_tuple(iX, iY, iZ);
    };

    size_t xyz_to_i (int x, int y, int z) const {
        return z + dim.nz * (y + dim.ny * x);
    };

    double& f (int i, int k) {
        return lattice[k * dim.nelem + i];
    }

    auto iniLattice (double& f0) {
        auto i = &f0 - lattice;
        for (int k = 0; k < 27; ++k) {
            f(i, k) = t[k];
        }
    };

    auto iniLattice (double& f0, double rho, std::array<double, 3> const& u) {
        auto i = &f0 - lattice;
        double usqr = 1.5 * (u[0] * u[0] + u[1] * u[1] + u[2] * u[2]);
        for (int k = 0; k < 27; ++k) {
            double ck_u = c[k][0] * u[0] + c[k][1] * u[1] + c[k][2] * u[2];
            f(i, k) = rho * t[k] * (1. + 3. * ck_u + 4.5 * ck_u * ck_u - usqr);
        }
    };

    auto macro (double const& f0) {
        auto i = &f0 - lattice;
        double X_M1 = f(i, 0) + f(i, 3) + f(i, 4) + f(i, 5) + f(i, 6) + f(i, 9) + f(i,10) + f(i,11) + f(i,12);
        double X_P1 = f(i,14) + f(i,17) + f(i,18) + f(i,19) + f(i,20) + f(i,23) + f(i,24) + f(i,25) + f(i,26);
        double X_0  = f(i, 1) + f(i, 2) + f(i, 7) + f(i, 8) + f(i,13) + f(i,15) + f(i,16) + f(i,21) + f(i,22);

        double Y_M1 = f(i, 1) + f(i, 3) + f(i, 7) + f(i, 8) + f(i, 9) + f(i,10) + f(i,18) + f(i,25) + f(i,26);
        double Y_P1 = f(i,15) + f(i,17) + f(i,21) + f(i,22) + f(i,23) + f(i,24) + f(i, 4) + f(i,11) + f(i,12);

        double Z_M1 = f(i, 2) + f(i, 5) + f(i, 7) + f(i, 9) + f(i,11) + f(i,20) + f(i,22) + f(i,24) + f(i,26);
        double Z_P1 = f(i,16) + f(i,19) + f(i,21) + f(i,23) + f(i,25) + f(i, 6) + f(i, 8) + f(i,10) + f(i,12);

        double rho = X_M1 + X_P1 + X_0;
        std::array<double, 3> u{ (X_P1 - X_M1) / rho, (Y_P1 - Y_M1) / rho, (Z_P1 - Z_M1) / rho };
        return make_pair(rho, u);
    }

    auto macropop (std::array<double, 27> const& pop) {
        double X_M1 = pop[ 0] + pop[ 3] + pop[ 4] + pop[ 5] + pop[ 6] + pop[ 9] + pop[10] + pop[11] + pop[12];
        double X_P1 = pop[14] + pop[17] + pop[18] + pop[19] + pop[20] + pop[23] + pop[24] + pop[25] + pop[26];
        double X_0  = pop[ 1] + pop[ 2] + pop[ 7] + pop[ 8] + pop[13] + pop[15] + pop[16] + pop[21] + pop[22];

        double Y_M1 = pop[ 1] + pop[ 3] + pop[ 7] + pop[ 8] + pop[ 9] + pop[10] + pop[18] + pop[25] + pop[26];
        double Y_P1 = pop[15] + pop[17] + pop[21] + pop[22] + pop[23] + pop[24] + pop[ 4] + pop[11] + pop[12];

        double Z_M1 = pop[ 2] + pop[ 5] + pop[ 7] + pop[ 9] + pop[11] + pop[20] + pop[22] + pop[24] + pop[26];
        double Z_P1 = pop[16] + pop[19] + pop[21] + pop[23] + pop[25] + pop[ 6] + pop[ 8] + pop[10] + pop[12];

        double rho = X_M1 + X_P1 + X_0;
        std::array<double, 3> u{ (X_P1 - X_M1) / rho, (Y_P1 - Y_M1) / rho, (Z_P1 - Z_M1) / rho };
        return make_pair(rho, u);
    };

    // Naive way to compute CM and macros
    auto computeCM(std::array<double, 27> const& pop) {
        std::array<double, 27> CM;
        std::fill(CM.begin(), CM.end(), 0.);

        // Preliminary computation of density and velocity vector
        double X_M1 = pop[ 0] + pop[ 3] + pop[ 4] + pop[ 5] + pop[ 6] + pop[ 9] + pop[10] + pop[11] + pop[12];
        double X_P1 = pop[14] + pop[17] + pop[18] + pop[19] + pop[20] + pop[23] + pop[24] + pop[25] + pop[26];
        double X_0  = pop[ 1] + pop[ 2] + pop[ 7] + pop[ 8] + pop[13] + pop[15] + pop[16] + pop[21] + pop[22];

        double Y_M1 = pop[ 1] + pop[ 3] + pop[ 7] + pop[ 8] + pop[ 9] + pop[10] + pop[18] + pop[25] + pop[26];
        double Y_P1 = pop[15] + pop[17] + pop[21] + pop[22] + pop[23] + pop[24] + pop[ 4] + pop[11] + pop[12];

        double Z_M1 = pop[ 2] + pop[ 5] + pop[ 7] + pop[ 9] + pop[11] + pop[20] + pop[22] + pop[24] + pop[26];
        double Z_P1 = pop[16] + pop[19] + pop[21] + pop[23] + pop[25] + pop[ 6] + pop[ 8] + pop[10] + pop[12];

        double rho = X_M1 + X_P1 + X_0;
        std::array<double, 3> u{ (X_P1 - X_M1) / rho, (Y_P1 - Y_M1) / rho, (Z_P1 - Z_M1) / rho };
        CM[M000] = rho;
        CM[M100] = u[0];  // CM is used to store the velocity component 
        CM[M010] = u[1];  // CM is used to store the velocity component
        CM[M001] = u[2];  // CM is used to store the velocity component

        double cMux, cMuy, cMuz;
        // Naive computation of CHMs
        for (int k = 0; k<27; ++k) {
            cMux = c[k][0]- u[0];
            cMuy = c[k][1]- u[1];
            cMuz = c[k][2]- u[2];
            
            // Order 2
            CM[M200] += cMux * cMux * pop[k];
            CM[M020] += cMuy * cMuy * pop[k];
            CM[M002] += cMuz * cMuz * pop[k];
            CM[M110] += cMux * cMuy * pop[k];
            CM[M101] += cMux * cMuz * pop[k];
            CM[M011] += cMuy * cMuz * pop[k];
            // Order 3
            CM[M210] += cMux * cMux * cMuy * pop[k];
            CM[M201] += cMux * cMux * cMuz * pop[k];
            CM[M021] += cMuy * cMuy * cMuz * pop[k];
            CM[M120] += cMux * cMuy * cMuy * pop[k];
            CM[M102] += cMux * cMuz * cMuz * pop[k];
            CM[M012] += cMuy * cMuz * cMuz * pop[k];
            CM[M111] += cMux * cMuy * cMuz * pop[k];
            // Order 4
            CM[M220] += cMux * cMux * cMuy * cMuy * pop[k];
            CM[M202] += cMux * cMux * cMuz * cMuz * pop[k];
            CM[M022] += cMuy * cMuy * cMuz * cMuz * pop[k];
            CM[M211] += cMux * cMux * cMuy * cMuz * pop[k];
            CM[M121] += cMux * cMuy * cMuy * cMuz * pop[k];
            CM[M112] += cMux * cMuy * cMuz * cMuz * pop[k];
            // Order 5
            CM[M221] += cMux * cMux * cMuy * cMuy * cMuz * pop[k];
            CM[M212] += cMux * cMux * cMuy * cMuz * cMuz * pop[k];
            CM[M122] += cMux * cMuy * cMuy * cMuz * cMuz * pop[k];
            // Order 6
            CM[M222] += cMux * cMux * cMuy * cMuy * cMuz * cMuz * pop[k];
        }

        double invRho = 1. / CM[M000];
        for (int k = 4; k<27; ++k) {
            CM[k] *= invRho;
        }
        return CM;
    }

    // First optimization (loop unrolling)
    auto computeCMopt(std::array<double, 27> const& pop) {
        std::array<double, 27> CM;
        std::fill(CM.begin(), CM.end(), 0.);
        // Order 0
        CM[M000] =  pop[ 0] + pop[ 1] + pop[ 2] + pop[ 3] + pop[ 4] + pop[ 5] + pop[ 6] + pop[ 7] + pop[ 8] + pop[ 9] + pop[10] + pop[11] + pop[12] + pop[13] + pop[14] + pop[15] + pop[16] + pop[17] + pop[18] + pop[19] + pop[20] + pop[21] + pop[22] + pop[23] + pop[24] + pop[25] + pop[26];
        double invRho = 1./CM[M000];
        // Order 1
        CM[M100] = invRho * (- pop[ 0] - pop[ 3] - pop[ 4] - pop[ 5] - pop[ 6] - pop[ 9] - pop[10] - pop[11] - pop[12] + pop[14] + pop[17] + pop[18] + pop[19] + pop[20] + pop[23] + pop[24] + pop[25] + pop[26]);
        CM[M010] = invRho * (- pop[ 1] - pop[ 3] + pop[ 4] - pop[ 7] - pop[ 8] - pop[ 9] - pop[10] + pop[11] + pop[12] + pop[15] + pop[17] - pop[18] + pop[21] + pop[22] + pop[23] + pop[24] - pop[25] - pop[26]);
        CM[M001] = invRho * (- pop[ 2] - pop[ 5] + pop[ 6] - pop[ 7] + pop[ 8] - pop[ 9] + pop[10] - pop[11] + pop[12] + pop[16] + pop[19] - pop[20] + pop[21] - pop[22] + pop[23] - pop[24] + pop[25] - pop[26]);
        // Order 2
        CM[M200] = invRho * (  pop[ 0] + pop[ 3] + pop[ 4] + pop[ 5] + pop[ 6] + pop[ 9] + pop[10] + pop[11] + pop[12] + pop[14] + pop[17] + pop[18] + pop[19] + pop[20] + pop[23] + pop[24] + pop[25] + pop[26]);
        CM[M020] = invRho * (  pop[ 1] + pop[ 3] + pop[ 4] + pop[ 7] + pop[ 8] + pop[ 9] + pop[10] + pop[11] + pop[12] + pop[15] + pop[17] + pop[18] + pop[21] + pop[22] + pop[23] + pop[24] + pop[25] + pop[26]);
        CM[M002] = invRho * (  pop[ 2] + pop[ 5] + pop[ 6] + pop[ 7] + pop[ 8] + pop[ 9] + pop[10] + pop[11] + pop[12] + pop[16] + pop[19] + pop[20] + pop[21] + pop[22] + pop[23] + pop[24] + pop[25] + pop[26]);
        CM[M110] = invRho * (  pop[ 3] - pop[ 4] + pop[ 9] + pop[10] - pop[11] - pop[12] + pop[17] - pop[18] + pop[23] + pop[24] - pop[25] - pop[26]);
        CM[M101] = invRho * (  pop[ 5] - pop[ 6] + pop[ 9] - pop[10] + pop[11] - pop[12] + pop[19] - pop[20] + pop[23] - pop[24] + pop[25] - pop[26]);
        CM[M011] = invRho * (  pop[ 7] - pop[ 8] + pop[ 9] - pop[10] - pop[11] + pop[12] + pop[21] - pop[22] + pop[23] - pop[24] - pop[25] + pop[26]);
        // Order 3
        CM[M210] = invRho * (- pop[ 3] + pop[ 4] - pop[ 9] - pop[10] + pop[11] + pop[12] + pop[17] - pop[18] + pop[23] + pop[24] - pop[25] - pop[26]);
        CM[M201] = invRho * (- pop[ 5] + pop[ 6] - pop[ 9] + pop[10] - pop[11] + pop[12] + pop[19] - pop[20] + pop[23] - pop[24] + pop[25] - pop[26]);
        CM[M021] = invRho * (- pop[ 7] + pop[ 8] - pop[ 9] + pop[10] - pop[11] + pop[12] + pop[21] - pop[22] + pop[23] - pop[24] + pop[25] - pop[26]);
        CM[M120] = invRho * (- pop[ 3] - pop[ 4] - pop[ 9] - pop[10] - pop[11] - pop[12] + pop[17] + pop[18] + pop[23] + pop[24] + pop[25] + pop[26]);
        CM[M102] = invRho * (- pop[ 5] - pop[ 6] - pop[ 9] - pop[10] - pop[11] - pop[12] + pop[19] + pop[20] + pop[23] + pop[24] + pop[25] + pop[26]);
        CM[M012] = invRho * (- pop[ 7] - pop[ 8] - pop[ 9] - pop[10] + pop[11] + pop[12] + pop[21] + pop[22] + pop[23] + pop[24] - pop[25] - pop[26]);
        CM[M111] = invRho * (- pop[ 9] + pop[10] + pop[11] - pop[12] + pop[23] - pop[24] - pop[25] + pop[26]);
        // Order 4
        CM[M220] = invRho * (  pop[ 3] + pop[ 4] + pop[ 9] + pop[10] + pop[11] + pop[12] + pop[17] + pop[18] + pop[23] + pop[24] + pop[25] + pop[26]);
        CM[M202] = invRho * (  pop[ 5] + pop[ 6] + pop[ 9] + pop[10] + pop[11] + pop[12] + pop[19] + pop[20] + pop[23] + pop[24] + pop[25] + pop[26]);
        CM[M022] = invRho * (  pop[ 7] + pop[ 8] + pop[ 9] + pop[10] + pop[11] + pop[12] + pop[21] + pop[22] + pop[23] + pop[24] + pop[25] + pop[26]);
        CM[M211] = invRho * (  pop[ 9] - pop[10] - pop[11] + pop[12] + pop[23] - pop[24] - pop[25] + pop[26]);
        CM[M121] = invRho * (  pop[ 9] - pop[10] + pop[11] - pop[12] + pop[23] - pop[24] + pop[25] - pop[26]);
        CM[M112] = invRho * (  pop[ 9] + pop[10] - pop[11] - pop[12] + pop[23] + pop[24] - pop[25] - pop[26]);
        // Order 5
        CM[M221] = invRho * (- pop[ 9] + pop[10] - pop[11] + pop[12] + pop[23] - pop[24] + pop[25] - pop[26]);
        CM[M212] = invRho * (- pop[ 9] - pop[10] + pop[11] + pop[12] + pop[23] + pop[24] - pop[25] - pop[26]);
        CM[M122] = invRho * (- pop[ 9] - pop[10] - pop[11] - pop[12] + pop[23] + pop[24] + pop[25] + pop[26]);
        // Order 6
        CM[M222] = invRho * (  pop[ 9] + pop[10] + pop[11] + pop[12] + pop[23] + pop[24] + pop[25] + pop[26]);

        // Compute CMs from RMs using binomial formulas
        double ux   = CM[M100];
        double uy   = CM[M010];
        double uz   = CM[M001];
        double ux2  = CM[M100]*CM[M100];
        double uy2  = CM[M010]*CM[M010];
        double uz2  = CM[M001]*CM[M001];
        double uxyz = CM[M100]*CM[M010]*CM[M001];

        CM[M200] -= (ux2);
        CM[M020] -= (uy2);
        CM[M002] -= (uz2);
        
        CM[M110] -= (ux*uy);
        CM[M101] -= (ux*uz);
        CM[M011] -= (uy*uz);

        CM[M210] -= (uy*CM[M200] + 2.*ux*CM[M110] + ux2*uy);
        CM[M201] -= (uz*CM[M200] + 2.*ux*CM[M101] + ux2*uz);
        CM[M021] -= (uz*CM[M020] + 2.*uy*CM[M011] + uy2*uz);
        CM[M120] -= (ux*CM[M020] + 2.*uy*CM[M110] + ux*uy2);
        CM[M102] -= (ux*CM[M002] + 2.*uz*CM[M101] + ux*uz2);
        CM[M012] -= (uy*CM[M002] + 2.*uz*CM[M011] + uy*uz2);
        
        CM[M111] -= (uz*CM[M110] + uy*CM[M101] + ux*CM[M011] + uxyz );

        CM[M220] -= (2.*uy*CM[M210] + 2.*ux*CM[M120] + uy2*CM[M200] + ux2*CM[M020] + 4.*ux*uy*CM[M110] + ux2*uy2);
        CM[M202] -= (2.*uz*CM[M201] + 2.*ux*CM[M102] + uz2*CM[M200] + ux2*CM[M002] + 4.*ux*uz*CM[M101] + ux2*uz2);
        CM[M022] -= (2.*uz*CM[M021] + 2.*uy*CM[M012] + uz2*CM[M020] + uy2*CM[M002] + 4.*uy*uz*CM[M011] + uy2*uz2);

        CM[M211] -= (uz*CM[M210] + uy*CM[M201] + 2.*ux*CM[M111] + uy*uz*CM[M200] + 2.*ux*uz*CM[M110] + 2.*ux*uy*CM[M101] + ux2*CM[M011] + ux2*uy*uz);
        CM[M121] -= (uz*CM[M120] + ux*CM[M021] + 2.*uy*CM[M111] + ux*uz*CM[M020] + 2.*uy*uz*CM[M110] + 2.*ux*uy*CM[M011] + uy2*CM[M101] + ux*uy2*uz);
        CM[M112] -= (uy*CM[M102] + ux*CM[M012] + 2.*uz*CM[M111] + ux*uy*CM[M002] + 2.*uy*uz*CM[M101] + 2.*ux*uz*CM[M011] + uz2*CM[M110] + ux*uy*uz2);

        CM[M221] -= (uz*CM[M220] + 2.*uy*CM[M211] + 2.*ux*CM[M121] + 2.*uy*uz*CM[M210] + uy2*CM[M201] + ux2*CM[M021] + 2.*ux*uz*CM[M120] + 4.*ux*uy*CM[M111] + uy2*uz*CM[M200] + ux2*uz*CM[M020] + 4.*uxyz*CM[M110] + 2.*ux*uy2*CM[M101] + 2.*ux2*uy*CM[M011] + ux2*uy2*uz);
        CM[M212] -= (uy*CM[M202] + 2.*uz*CM[M211] + 2.*ux*CM[M112] + 2.*uy*uz*CM[M201] + uz2*CM[M210] + ux2*CM[M012] + 2.*ux*uy*CM[M102] + 4.*ux*uz*CM[M111] + uy*uz2*CM[M200] + ux2*uy*CM[M002] + 4.*uxyz*CM[M101] + 2.*ux*uz2*CM[M110] + 2.*ux2*uz*CM[M011] + ux2*uy*uz2);
        CM[M122] -= (ux*CM[M022] + 2.*uz*CM[M121] + 2.*uy*CM[M112] + 2.*ux*uz*CM[M021] + uz2*CM[M120] + uy2*CM[M102] + 2.*ux*uy*CM[M012] + 4.*uy*uz*CM[M111] + ux*uz2*CM[M020] + ux*uy2*CM[M002] + 4.*uxyz*CM[M011] + 2.*uy*uz2*CM[M110] + 2.*uy2*uz*CM[M101] + ux*uy2*uz2);

        CM[M222] -= (2.*uz*CM[M221] + 2.*uy*CM[M212] + 2.*ux*CM[M122] + uz2*CM[M220] + uy2*CM[M202] + ux2*CM[M022] + 4.*uy*uz*CM[M211] + 4.*ux*uz*CM[M121] + 4.*ux*uy*CM[M112] + 2.*uy*uz2*CM[M210] + 2.*uy2*uz*CM[M201] + 2.*ux2*uz*CM[M021] + 2.*ux*uz2*CM[M120] + 2.*ux*uy2*CM[M102] + 2.*ux2*uy*CM[M012] + 8.*uxyz*CM[M111] + uy2*uz2*CM[M200] + ux2*uz2*CM[M020] + ux2*uy2*CM[M002] + 4.*ux*uy*uz2*CM[M110] + 4.*ux*uy2*uz*CM[M101] + 4.*ux2*uy*uz*CM[M011] + ux2*uy2*uz2);

        return CM;
    }

    // Most optimized version (loop unrolling + redundant terms)
    // Further pre-computing terms is counter-productive (less than 1% gain)
    auto computeCMopt2(std::array<double, 27> const& pop) {
        std::array<double, 27> CM;
        std::fill(CM.begin(), CM.end(), 0.);

        double A1 = pop[ 9] + pop[10] + pop[11] + pop[12];
        double A2 = pop[23] + pop[24] + pop[25] + pop[26];
        double A3 = pop[ 9] + pop[10] - pop[11] - pop[12];
        double A4 = pop[23] + pop[24] - pop[25] - pop[26];
        double A5 = pop[ 9] - pop[10] + pop[11] - pop[12];
        double A6 = pop[23] - pop[24] + pop[25] - pop[26];
        double A7 = pop[ 9] - pop[10] - pop[11] + pop[12];
        double A8 = pop[23] - pop[24] - pop[25] + pop[26];

        double X_M1 = pop[ 0] + pop[ 3] + pop[ 4] + pop[ 5] + pop[ 6] + A1;
        double X_P1 = pop[14] + pop[17] + pop[18] + pop[19] + pop[20] + A2;
        double X_0  = pop[ 1] + pop[ 2] + pop[ 7] + pop[ 8] + pop[13] + pop[15] + pop[16] + pop[21] + pop[22];

        double Y_M1 = pop[ 1] + pop[ 3] + pop[ 7] + pop[ 8] + pop[ 9] + pop[10] + pop[18] + pop[25] + pop[26];
        double Y_P1 = pop[15] + pop[17] + pop[21] + pop[22] + pop[23] + pop[24] + pop[ 4] + pop[11] + pop[12];

        double Z_M1 = pop[ 2] + pop[ 5] + pop[ 7] + pop[ 9] + pop[11] + pop[20] + pop[22] + pop[24] + pop[26];
        double Z_P1 = pop[16] + pop[19] + pop[21] + pop[23] + pop[25] + pop[ 6] + pop[ 8] + pop[10] + pop[12];

        // Order 0
        CM[M000] = X_M1 + X_P1 + X_0;
        double invRho = 1. / CM[M000];

        // Order 6
        CM[M222] = invRho * (  A1 + A2);
        // Order 5
        CM[M221] = invRho * (- A5 + A6);
        CM[M212] = invRho * (- A3 + A4);
        CM[M122] = invRho * (- A1 + A2);
        // Order 4
        CM[M220] = invRho * (  pop[ 3] + pop[ 4] + pop[17] + pop[18]) + CM[M222];
        CM[M202] = invRho * (  pop[ 5] + pop[ 6] + pop[19] + pop[20]) + CM[M222];
        CM[M022] = invRho * (  pop[ 7] + pop[ 8] + pop[21] + pop[22]) + CM[M222];
        CM[M211] = invRho * (  A7 + A8);
        CM[M121] = invRho * (  A5 + A6);
        CM[M112] = invRho * (  A3 + A4);
        // Order 3
        CM[M210] = invRho * (- pop[ 3] + pop[ 4] + pop[17] - pop[18]) + CM[M212];
        CM[M201] = invRho * (- pop[ 5] + pop[ 6] + pop[19] - pop[20]) + CM[M221];
        CM[M021] = invRho * (- pop[ 7] + pop[ 8] + pop[21] - pop[22]) + CM[M221];
        CM[M120] = invRho * (- pop[ 3] - pop[ 4] + pop[17] + pop[18]) + CM[M122];
        CM[M102] = invRho * (- pop[ 5] - pop[ 6] + pop[19] + pop[20]) + CM[M122];
        CM[M012] = invRho * (- pop[ 7] - pop[ 8] + pop[21] + pop[22]) + CM[M212];
        CM[M111] = invRho * (- A7 + A8);
        // Order 2
        CM[M200] = invRho * (X_P1 + X_M1);
        CM[M020] = invRho * (Y_P1 + Y_M1); 
        CM[M002] = invRho * (Z_P1 + Z_M1);
        CM[M110] = invRho * (  pop[ 3] - pop[ 4] + pop[17] - pop[18]) + CM[M112];
        CM[M101] = invRho * (  pop[ 5] - pop[ 6] + pop[19] - pop[20]) + CM[M121];
        CM[M011] = invRho * (  pop[ 7] - pop[ 8] + pop[21] - pop[22]) + CM[M211];
        // Order 1
        CM[M100] = invRho * (X_P1 - X_M1);
        CM[M010] = invRho * (Y_P1 - Y_M1); 
        CM[M001] = invRho * (Z_P1 - Z_M1);

        // Compute CMs from RMs using binomial formulas
        double ux   = CM[M100];
        double uy   = CM[M010];
        double uz   = CM[M001];
        double ux2  = CM[M100]*CM[M100];
        double uy2  = CM[M010]*CM[M010];
        double uz2  = CM[M001]*CM[M001];
        double uxyz = CM[M100]*CM[M010]*CM[M001];

        CM[M200] -= (ux2);
        CM[M020] -= (uy2);
        CM[M002] -= (uz2);
        
        CM[M110] -= (ux*uy);
        CM[M101] -= (ux*uz);
        CM[M011] -= (uy*uz);

        CM[M210] -= (uy*CM[M200] + 2.*ux*CM[M110] + ux2*uy);
        CM[M201] -= (uz*CM[M200] + 2.*ux*CM[M101] + ux2*uz);
        CM[M021] -= (uz*CM[M020] + 2.*uy*CM[M011] + uy2*uz);
        CM[M120] -= (ux*CM[M020] + 2.*uy*CM[M110] + ux*uy2);
        CM[M102] -= (ux*CM[M002] + 2.*uz*CM[M101] + ux*uz2);
        CM[M012] -= (uy*CM[M002] + 2.*uz*CM[M011] + uy*uz2);
        
        CM[M111] -= (uz*CM[M110] + uy*CM[M101] + ux*CM[M011] + uxyz );

        CM[M220] -= (2.*uy*CM[M210] + 2.*ux*CM[M120] + uy2*CM[M200] + ux2*CM[M020] + 4.*ux*uy*CM[M110] + ux2*uy2);
        CM[M202] -= (2.*uz*CM[M201] + 2.*ux*CM[M102] + uz2*CM[M200] + ux2*CM[M002] + 4.*ux*uz*CM[M101] + ux2*uz2);
        CM[M022] -= (2.*uz*CM[M021] + 2.*uy*CM[M012] + uz2*CM[M020] + uy2*CM[M002] + 4.*uy*uz*CM[M011] + uy2*uz2);

        CM[M211] -= (uz*CM[M210] + uy*CM[M201] + 2.*ux*CM[M111] + uy*uz*CM[M200] + 2.*ux*uz*CM[M110] + 2.*ux*uy*CM[M101] + ux2*CM[M011] + ux2*uy*uz);
        CM[M121] -= (uz*CM[M120] + ux*CM[M021] + 2.*uy*CM[M111] + ux*uz*CM[M020] + 2.*uy*uz*CM[M110] + 2.*ux*uy*CM[M011] + uy2*CM[M101] + ux*uy2*uz);
        CM[M112] -= (uy*CM[M102] + ux*CM[M012] + 2.*uz*CM[M111] + ux*uy*CM[M002] + 2.*uy*uz*CM[M101] + 2.*ux*uz*CM[M011] + uz2*CM[M110] + ux*uy*uz2);

        CM[M221] -= (uz*CM[M220] + 2.*uy*CM[M211] + 2.*ux*CM[M121] + 2.*uy*uz*CM[M210] + uy2*CM[M201] + ux2*CM[M021] + 2.*ux*uz*CM[M120] + 4.*ux*uy*CM[M111] + uy2*uz*CM[M200] + ux2*uz*CM[M020] + 4.*uxyz*CM[M110] + 2.*ux*uy2*CM[M101] + 2.*ux2*uy*CM[M011] + ux2*uy2*uz);
        CM[M212] -= (uy*CM[M202] + 2.*uz*CM[M211] + 2.*ux*CM[M112] + 2.*uy*uz*CM[M201] + uz2*CM[M210] + ux2*CM[M012] + 2.*ux*uy*CM[M102] + 4.*ux*uz*CM[M111] + uy*uz2*CM[M200] + ux2*uy*CM[M002] + 4.*uxyz*CM[M101] + 2.*ux*uz2*CM[M110] + 2.*ux2*uz*CM[M011] + ux2*uy*uz2);
        CM[M122] -= (ux*CM[M022] + 2.*uz*CM[M121] + 2.*uy*CM[M112] + 2.*ux*uz*CM[M021] + uz2*CM[M120] + uy2*CM[M102] + 2.*ux*uy*CM[M012] + 4.*uy*uz*CM[M111] + ux*uz2*CM[M020] + ux*uy2*CM[M002] + 4.*uxyz*CM[M011] + 2.*uy*uz2*CM[M110] + 2.*uy2*uz*CM[M101] + ux*uy2*uz2);

        CM[M222] -= (2.*uz*CM[M221] + 2.*uy*CM[M212] + 2.*ux*CM[M122] + uz2*CM[M220] + uy2*CM[M202] + ux2*CM[M022] + 4.*uy*uz*CM[M211] + 4.*ux*uz*CM[M121] + 4.*ux*uy*CM[M112] + 2.*uy*uz2*CM[M210] + 2.*uy2*uz*CM[M201] + 2.*ux2*uz*CM[M021] + 2.*ux*uz2*CM[M120] + 2.*ux*uy2*CM[M102] + 2.*ux2*uy*CM[M012] + 8.*uxyz*CM[M111] + uy2*uz2*CM[M200] + ux2*uz2*CM[M020] + ux2*uy2*CM[M002] + 4.*ux*uy*uz2*CM[M110] + 4.*ux*uy2*uz*CM[M101] + 4.*ux2*uy*uz*CM[M011] + ux2*uy2*uz2);

        return CM;
    }

    // The function below has only an educational purpose since it is discarded
    // during the compilation because CMeq are not used in collideCM()
    auto computeCMeq() {

        std::array<double, 27> CMeq;
        double cs2 = 1./3.;
        double cs4 = cs2*cs2;
        // Order 2
        CMeq[M200] = cs2;
        CMeq[M020] = cs2;
        CMeq[M002] = cs2;
        CMeq[M110] = 0.;
        CMeq[M101] = 0.;
        CMeq[M011] = 0.;
        // Order 3
        CMeq[M210] = 0.;
        CMeq[M201] = 0.;
        CMeq[M021] = 0.;
        CMeq[M120] = 0.;
        CMeq[M102] = 0.;
        CMeq[M012] = 0.;
        CMeq[M111] = 0.;
        // Order 4
        CMeq[M220] = cs4;
        CMeq[M202] = cs4;
        CMeq[M022] = cs4;
        CMeq[M211] = 0.;
        CMeq[M121] = 0.;
        CMeq[M112] = 0.;
        // Order 5
        CMeq[M221] = 0.;
        CMeq[M212] = 0.;
        CMeq[M122] = 0.;
        // Order 6
        CMeq[M222] = cs2 * cs4;

        return CMeq;
    }
};

struct Even : public LBM {

    auto collideCM(int i, double rho, std::array<double, 3> const& u, std::array<double, 27> const& CM , std::array<double, 27> const& CMeq)
    {
        double cs2 = 1./3.;
        double cs4 = cs2*cs2;
        double ux2  = u[0]*u[0];
        double uy2  = u[1]*u[1];
        double uz2  = u[2]*u[2];
        double uxyz = u[0]*u[1]*u[2];

        // Post-collision moments.
        std::array<double, 27> CMcoll;
        std::array<double, 27> RMcoll;

        // Collision in the central moment space (CMeq are replaced by their values)
        // Order 2
        if (omegaBulk == 0.) { // Don't adjust bulk viscosity
            CMcoll[M200] = (1. - omega1) * CM[M200] + omega1 * cs2;
            CMcoll[M020] = (1. - omega1) * CM[M020] + omega1 * cs2;
            CMcoll[M002] = (1. - omega1) * CM[M002] + omega1 * cs2;
        }
        else { // Adjust bulk viscosity
            const double omegaBulk = omega1;
            const double omegaMinus = (omegaBulk - omega1)/3.; // Notation used by Fei
            const double omegaPlus  = omegaMinus + omega1;     // Notation used by Fei
            CMcoll[M200] = CM[M200] - omegaPlus  * (CM[M200]-cs2) - omegaMinus * (CM[M020]-cs2) - omegaMinus * (CM[M002]-cs2) ;
            CMcoll[M020] = CM[M020] - omegaMinus * (CM[M200]-cs2) - omegaPlus  * (CM[M020]-cs2) - omegaMinus * (CM[M002]-cs2) ;
            CMcoll[M002] = CM[M002] - omegaMinus * (CM[M200]-cs2) - omegaMinus * (CM[M020]-cs2) - omegaPlus  * (CM[M002]-cs2) ;
        }
        
        CMcoll[M110] = (1. - omega2) * CM[M110];
        CMcoll[M101] = (1. - omega2) * CM[M101];
        CMcoll[M011] = (1. - omega2) * CM[M011];
        // Order 3
        CMcoll[M210] = (1. - omega3) * CM[M210];
        CMcoll[M201] = (1. - omega3) * CM[M201];
        CMcoll[M021] = (1. - omega3) * CM[M021];
        CMcoll[M120] = (1. - omega3) * CM[M120];
        CMcoll[M102] = (1. - omega3) * CM[M102];
        CMcoll[M012] = (1. - omega3) * CM[M012];
        CMcoll[M111] = (1. - omega4) * CM[M111];
        // Order 4
        CMcoll[M220] = (1. - omega5) * CM[M220] + omega5 * cs4 ;
        CMcoll[M202] = (1. - omega5) * CM[M202] + omega5 * cs4 ;
        CMcoll[M022] = (1. - omega5) * CM[M022] + omega5 * cs4 ;
        CMcoll[M211] = (1. - omega6) * CM[M211];
        CMcoll[M121] = (1. - omega6) * CM[M121];
        CMcoll[M112] = (1. - omega6) * CM[M112];
        // Order 5
        CMcoll[M221] = (1. - omega7) * CM[M221];
        CMcoll[M212] = (1. - omega7) * CM[M212];
        CMcoll[M122] = (1. - omega7) * CM[M122];
        // Order 6
        CMcoll[M222] = (1. - omega8) * CM[M222] + omega8 * cs4*cs2 ;

        // Come back to RMcoll using binomial formulas
        RMcoll[M200] = CMcoll[M200] + ux2;
        RMcoll[M020] = CMcoll[M020] + uy2;
        RMcoll[M002] = CMcoll[M002] + uz2;
        
        RMcoll[M110] = CMcoll[M110] + u[0]*u[1];
        RMcoll[M101] = CMcoll[M101] + u[0]*u[2];
        RMcoll[M011] = CMcoll[M011] + u[1]*u[2];

        RMcoll[M210] = CMcoll[M210] + u[1]*CMcoll[M200] + 2.*u[0]*CMcoll[M110] + ux2*u[1];
        RMcoll[M201] = CMcoll[M201] + u[2]*CMcoll[M200] + 2.*u[0]*CMcoll[M101] + ux2*u[2];
        RMcoll[M021] = CMcoll[M021] + u[2]*CMcoll[M020] + 2.*u[1]*CMcoll[M011] + uy2*u[2];
        RMcoll[M120] = CMcoll[M120] + u[0]*CMcoll[M020] + 2.*u[1]*CMcoll[M110] + u[0]*uy2;
        RMcoll[M102] = CMcoll[M102] + u[0]*CMcoll[M002] + 2.*u[2]*CMcoll[M101] + u[0]*uz2;
        RMcoll[M012] = CMcoll[M012] + u[1]*CMcoll[M002] + 2.*u[2]*CMcoll[M011] + u[1]*uz2;
        
        RMcoll[M111] = CMcoll[M111] + u[2]*CMcoll[M110] + u[1]*CMcoll[M101] + u[0]*CMcoll[M011] + uxyz ;

        RMcoll[M220] = CMcoll[M220] + 2.*u[1]*CMcoll[M210] + 2.*u[0]*CMcoll[M120] + uy2*CMcoll[M200] + ux2*CMcoll[M020] + 4.*u[0]*u[1]*CMcoll[M110] + ux2*uy2;
        RMcoll[M202] = CMcoll[M202] + 2.*u[2]*CMcoll[M201] + 2.*u[0]*CMcoll[M102] + uz2*CMcoll[M200] + ux2*CMcoll[M002] + 4.*u[0]*u[2]*CMcoll[M101] + ux2*uz2;
        RMcoll[M022] = CMcoll[M022] + 2.*u[2]*CMcoll[M021] + 2.*u[1]*CMcoll[M012] + uz2*CMcoll[M020] + uy2*CMcoll[M002] + 4.*u[1]*u[2]*CMcoll[M011] + uy2*uz2;

        RMcoll[M211] = CMcoll[M211] + u[2]*CMcoll[M210] + u[1]*CMcoll[M201] + 2.*u[0]*CMcoll[M111] + u[1]*u[2]*CMcoll[M200] + 2.*u[0]*u[2]*CMcoll[M110] + 2.*u[0]*u[1]*CMcoll[M101] + ux2*CMcoll[M011] + ux2*u[1]*u[2];
        RMcoll[M121] = CMcoll[M121] + u[2]*CMcoll[M120] + u[0]*CMcoll[M021] + 2.*u[1]*CMcoll[M111] + u[0]*u[2]*CMcoll[M020] + 2.*u[1]*u[2]*CMcoll[M110] + 2.*u[0]*u[1]*CMcoll[M011] + uy2*CMcoll[M101] + u[0]*uy2*u[2];
        RMcoll[M112] = CMcoll[M112] + u[1]*CMcoll[M102] + u[0]*CMcoll[M012] + 2.*u[2]*CMcoll[M111] + u[0]*u[1]*CMcoll[M002] + 2.*u[1]*u[2]*CMcoll[M101] + 2.*u[0]*u[2]*CMcoll[M011] + uz2*CMcoll[M110] + u[0]*u[1]*uz2;

        RMcoll[M221] = CMcoll[M221] + u[2]*CMcoll[M220] + 2.*u[1]*CMcoll[M211] + 2.*u[0]*CMcoll[M121] + 2.*u[1]*u[2]*CMcoll[M210] + uy2*CMcoll[M201] + ux2*CMcoll[M021] + 2.*u[0]*u[2]*CMcoll[M120] + 4.*u[0]*u[1]*CMcoll[M111] + uy2*u[2]*CMcoll[M200] + ux2*u[2]*CMcoll[M020] + 4.*uxyz*CMcoll[M110] + 2.*u[0]*uy2*CMcoll[M101] + 2.*ux2*u[1]*CMcoll[M011] + ux2*uy2*u[2];
        RMcoll[M212] = CMcoll[M212] + u[1]*CMcoll[M202] + 2.*u[2]*CMcoll[M211] + 2.*u[0]*CMcoll[M112] + 2.*u[1]*u[2]*CMcoll[M201] + uz2*CMcoll[M210] + ux2*CMcoll[M012] + 2.*u[0]*u[1]*CMcoll[M102] + 4.*u[0]*u[2]*CMcoll[M111] + u[1]*uz2*CMcoll[M200] + ux2*u[1]*CMcoll[M002] + 4.*uxyz*CMcoll[M101] + 2.*u[0]*uz2*CMcoll[M110] + 2.*ux2*u[2]*CMcoll[M011] + ux2*u[1]*uz2;
        RMcoll[M122] = CMcoll[M122] + u[0]*CMcoll[M022] + 2.*u[2]*CMcoll[M121] + 2.*u[1]*CMcoll[M112] + 2.*u[0]*u[2]*CMcoll[M021] + uz2*CMcoll[M120] + uy2*CMcoll[M102] + 2.*u[0]*u[1]*CMcoll[M012] + 4.*u[1]*u[2]*CMcoll[M111] + u[0]*uz2*CMcoll[M020] + u[0]*uy2*CMcoll[M002] + 4.*uxyz*CMcoll[M011] + 2.*u[1]*uz2*CMcoll[M110] + 2.*uy2*u[2]*CMcoll[M101] + u[0]*uy2*uz2;

        RMcoll[M222] = CMcoll[M222] + 2.*u[2]*CMcoll[M221] + 2.*u[1]*CMcoll[M212] + 2.*u[0]*CMcoll[M122] + uz2*CMcoll[M220] + uy2*CMcoll[M202] + ux2*CMcoll[M022] + 4.*u[1]*u[2]*CMcoll[M211] + 4.*u[0]*u[2]*CMcoll[M121] + 4.*u[0]*u[1]*CMcoll[M112] + 2.*u[1]*uz2*CMcoll[M210] + 2.*uy2*u[2]*CMcoll[M201] + 2.*ux2*u[2]*CMcoll[M021] + 2.*u[0]*uz2*CMcoll[M120] + 2.*u[0]*uy2*CMcoll[M102] + 2.*ux2*u[1]*CMcoll[M012] + 8.*uxyz*CMcoll[M111] + uy2*uz2*CMcoll[M200] + ux2*uz2*CMcoll[M020] + ux2*uy2*CMcoll[M002] + 4.*u[0]*u[1]*uz2*CMcoll[M110] + 4.*u[0]*uy2*u[2]*CMcoll[M101] + 4.*ux2*u[1]*u[2]*CMcoll[M011] + ux2*uy2*uz2;

        // Optimization based on symmetries between populations and their opposite counterpart
        std::array<double, 27> foutRM;

        foutRM[F000] = rho *(1. - RMcoll[M200] - RMcoll[M020] - RMcoll[M002] + RMcoll[M220] + RMcoll[M202] + RMcoll[M022] - RMcoll[M222]);
        
        foutRM[FP00] = 0.5*rho * ( u[0] + RMcoll[M200] - RMcoll[M120] - RMcoll[M102] - RMcoll[M220] - RMcoll[M202] + RMcoll[M122] + RMcoll[M222]);
        foutRM[FM00] =     rho * (-u[0]                + RMcoll[M120] + RMcoll[M102]                               - RMcoll[M122])+ foutRM[FP00];

        foutRM[F0P0] = 0.5*rho * ( u[1] + RMcoll[M020] - RMcoll[M210] - RMcoll[M012] - RMcoll[M220] - RMcoll[M022] + RMcoll[M212] + RMcoll[M222]);
        foutRM[F0M0] =     rho * (-u[1]                + RMcoll[M210] + RMcoll[M012]                               - RMcoll[M212])+ foutRM[F0P0];

        foutRM[F00P] = 0.5*rho * ( u[2] + RMcoll[M002] - RMcoll[M201] - RMcoll[M021] - RMcoll[M202] - RMcoll[M022] + RMcoll[M221] + RMcoll[M222]);
        foutRM[F00M] =     rho * (-u[2]                + RMcoll[M201] + RMcoll[M021]                               - RMcoll[M221])+ foutRM[F00P];

        foutRM[FPP0] = 0.25*rho * ( RMcoll[M110] + RMcoll[M210] + RMcoll[M120] - RMcoll[M112] + RMcoll[M220] - RMcoll[M212] - RMcoll[M122] - RMcoll[M222]);
        foutRM[FMP0] =  0.5*rho * (-RMcoll[M110]                - RMcoll[M120] + RMcoll[M112]                               + RMcoll[M122])+ foutRM[FPP0];
        foutRM[FPM0] =  0.5*rho * (-RMcoll[M110] - RMcoll[M210]                + RMcoll[M112]                + RMcoll[M212]               )+ foutRM[FPP0];
        foutRM[FMM0] =  0.5*rho * (              - RMcoll[M210] - RMcoll[M120]                               + RMcoll[M212] + RMcoll[M122])+ foutRM[FPP0];

        foutRM[FP0P] = 0.25*rho * ( RMcoll[M101] + RMcoll[M201] + RMcoll[M102] - RMcoll[M121] + RMcoll[M202] - RMcoll[M221] - RMcoll[M122] - RMcoll[M222]);
        foutRM[FM0P] =  0.5*rho * (-RMcoll[M101]                - RMcoll[M102] + RMcoll[M121]                               + RMcoll[M122])+ foutRM[FP0P];
        foutRM[FP0M] =  0.5*rho * (-RMcoll[M101] - RMcoll[M201]                + RMcoll[M121]                + RMcoll[M221]               )+ foutRM[FP0P];
        foutRM[FM0M] =  0.5*rho * (              - RMcoll[M201] - RMcoll[M102]                               + RMcoll[M221] + RMcoll[M122])+ foutRM[FP0P];

        foutRM[F0PP] = 0.25*rho * ( RMcoll[M011] + RMcoll[M021] + RMcoll[M012] - RMcoll[M211] + RMcoll[M022] - RMcoll[M221] - RMcoll[M212] - RMcoll[M222]);
        foutRM[F0MP] =  0.5*rho * (-RMcoll[M011]                - RMcoll[M012] + RMcoll[M211]                               + RMcoll[M212])+ foutRM[F0PP];
        foutRM[F0PM] =  0.5*rho * (-RMcoll[M011] - RMcoll[M021]                + RMcoll[M211]                + RMcoll[M221]               )+ foutRM[F0PP];
        foutRM[F0MM] =  0.5*rho * (              - RMcoll[M021] - RMcoll[M012]                               + RMcoll[M221] + RMcoll[M212])+ foutRM[F0PP];

        foutRM[FPPP] = 0.125*rho * ( RMcoll[M111] + RMcoll[M211] + RMcoll[M121] + RMcoll[M112] + RMcoll[M221] + RMcoll[M212] + RMcoll[M122] + RMcoll[M222]);
        foutRM[FMPP] =  0.25*rho * (-RMcoll[M111]                - RMcoll[M121] - RMcoll[M112]                               - RMcoll[M122])+ foutRM[FPPP];
        foutRM[FPMP] =  0.25*rho * (-RMcoll[M111] - RMcoll[M211]                - RMcoll[M112]                - RMcoll[M212]               )+ foutRM[FPPP];
        foutRM[FPPM] =  0.25*rho * (-RMcoll[M111] - RMcoll[M211] - RMcoll[M121]                - RMcoll[M221]                              )+ foutRM[FPPP];
        foutRM[FMMP] =  0.25*rho * (              - RMcoll[M211] - RMcoll[M121]                               - RMcoll[M212] - RMcoll[M122])+ foutRM[FPPP];
        foutRM[FMPM] =  0.25*rho * (              - RMcoll[M211]                - RMcoll[M112] - RMcoll[M221]                - RMcoll[M122])+ foutRM[FPPP];
        foutRM[FPMM] =  0.25*rho * (                             - RMcoll[M121] - RMcoll[M112] - RMcoll[M221] - RMcoll[M212]               )+ foutRM[FPPP];
        foutRM[FMMM] =  0.25*rho * (-RMcoll[M111]                                              - RMcoll[M221] - RMcoll[M212] - RMcoll[M122])+ foutRM[FPPP];

        f(i,F000) = foutRM[F000];

        f(i,FP00) = foutRM[FM00];
        f(i,FM00) = foutRM[FP00];

        f(i,F0P0) = foutRM[F0M0];
        f(i,F0M0) = foutRM[F0P0];

        f(i,F00P) = foutRM[F00M];
        f(i,F00M) = foutRM[F00P];

        f(i,FPP0) = foutRM[FMM0];
        f(i,FMP0) = foutRM[FPM0];
        f(i,FPM0) = foutRM[FMP0];
        f(i,FMM0) = foutRM[FPP0];

        f(i,FP0P) = foutRM[FM0M];
        f(i,FM0P) = foutRM[FP0M];
        f(i,FP0M) = foutRM[FM0P];
        f(i,FM0M) = foutRM[FP0P];

        f(i,F0PP) = foutRM[F0MM];
        f(i,F0MP) = foutRM[F0PM];
        f(i,F0PM) = foutRM[F0MP];
        f(i,F0MM) = foutRM[F0PP];

        f(i,FPPP) = foutRM[FMMM];
        f(i,FMPP) = foutRM[FPMM];
        f(i,FPMP) = foutRM[FMPM];
        f(i,FPPM) = foutRM[FMMP];
        f(i,FMMP) = foutRM[FPPM];
        f(i,FMPM) = foutRM[FPMP];
        f(i,FPMM) = foutRM[FMPP];
        f(i,FMMM) = foutRM[FPPP];
    }


    void iterateCM(double& f0) {
        int i = &f0 - lattice;
        if (flag[i] == CellType::bulk) {
            std::array<double, 27> pop;
            for (int k = 0; k < 27; ++k) {
                pop[k] = f(i, k);
            }
/*          auto CM = computeCM(pop);
            auto CM = computeCMopt(pop);*/
            auto CM = computeCMopt2(pop);
            double rho = CM[M000];
            std::array<double, 3> u = {CM[M100], CM[M010], CM[M001]};
            auto CMeq = computeCMeq();
            collideCM(i, rho, u, CM, CMeq);
        }
    }

    void operator() (double& f0) {
        iterateCM(f0);
    }
};

struct Odd : public LBM {

    auto collideCM(std::array<double, 27>& pop, double rho, std::array<double, 3> const& u,
                   std::array<double, 27> const& CM, std::array<double, 27> const& CMeq)
    {
        double cs2 = 1./3.;
        double cs4 = cs2*cs2;
        double ux2  = u[0]*u[0];
        double uy2  = u[1]*u[1];
        double uz2  = u[2]*u[2];
        double uxyz = u[0]*u[1]*u[2];

        // Post-collision moments.
        std::array<double, 27> CMcoll;
        std::array<double, 27> RMcoll;

        // Collision in the central moment space (CMeq are replaced by their values)
        // Order 2
        if (omegaBulk == 0.) { // Don't adjust bulk viscosity
            CMcoll[M200] = (1. - omega1) * CM[M200] + omega1 * cs2;
            CMcoll[M020] = (1. - omega1) * CM[M020] + omega1 * cs2;
            CMcoll[M002] = (1. - omega1) * CM[M002] + omega1 * cs2;
        }
        else { // Adjust bulk viscosity
            const double omegaBulk = omega1;
            const double omegaMinus = (omegaBulk - omega1)/3.; // Notation used by Fei
            const double omegaPlus  = omegaMinus + omega1;     // Notation used by Fei
            CMcoll[M200] = CM[M200] - omegaPlus  * (CM[M200]-cs2) - omegaMinus * (CM[M020]-cs2) - omegaMinus * (CM[M002]-cs2) ;
            CMcoll[M020] = CM[M020] - omegaMinus * (CM[M200]-cs2) - omegaPlus  * (CM[M020]-cs2) - omegaMinus * (CM[M002]-cs2) ;
            CMcoll[M002] = CM[M002] - omegaMinus * (CM[M200]-cs2) - omegaMinus * (CM[M020]-cs2) - omegaPlus  * (CM[M002]-cs2) ;
        }
        
        CMcoll[M110] = (1. - omega2) * CM[M110];
        CMcoll[M101] = (1. - omega2) * CM[M101];
        CMcoll[M011] = (1. - omega2) * CM[M011];
        // Order 3
        CMcoll[M210] = (1. - omega3) * CM[M210];
        CMcoll[M201] = (1. - omega3) * CM[M201];
        CMcoll[M021] = (1. - omega3) * CM[M021];
        CMcoll[M120] = (1. - omega3) * CM[M120];
        CMcoll[M102] = (1. - omega3) * CM[M102];
        CMcoll[M012] = (1. - omega3) * CM[M012];
        CMcoll[M111] = (1. - omega4) * CM[M111];
        // Order 4
        CMcoll[M220] = (1. - omega5) * CM[M220] + omega5 * cs4 ;
        CMcoll[M202] = (1. - omega5) * CM[M202] + omega5 * cs4 ;
        CMcoll[M022] = (1. - omega5) * CM[M022] + omega5 * cs4 ;
        CMcoll[M211] = (1. - omega6) * CM[M211];
        CMcoll[M121] = (1. - omega6) * CM[M121];
        CMcoll[M112] = (1. - omega6) * CM[M112];
        // Order 5
        CMcoll[M221] = (1. - omega7) * CM[M221];
        CMcoll[M212] = (1. - omega7) * CM[M212];
        CMcoll[M122] = (1. - omega7) * CM[M122];
        // Order 6
        CMcoll[M222] = (1. - omega8) * CM[M222] + omega8 * cs4*cs2 ;

        // Come back to RMcoll using binomial formulas
        RMcoll[M200] = CMcoll[M200] + ux2;
        RMcoll[M020] = CMcoll[M020] + uy2;
        RMcoll[M002] = CMcoll[M002] + uz2;
        
        RMcoll[M110] = CMcoll[M110] + u[0]*u[1];
        RMcoll[M101] = CMcoll[M101] + u[0]*u[2];
        RMcoll[M011] = CMcoll[M011] + u[1]*u[2];

        RMcoll[M210] = CMcoll[M210] + u[1]*CMcoll[M200] + 2.*u[0]*CMcoll[M110] + ux2*u[1];
        RMcoll[M201] = CMcoll[M201] + u[2]*CMcoll[M200] + 2.*u[0]*CMcoll[M101] + ux2*u[2];
        RMcoll[M021] = CMcoll[M021] + u[2]*CMcoll[M020] + 2.*u[1]*CMcoll[M011] + uy2*u[2];
        RMcoll[M120] = CMcoll[M120] + u[0]*CMcoll[M020] + 2.*u[1]*CMcoll[M110] + u[0]*uy2;
        RMcoll[M102] = CMcoll[M102] + u[0]*CMcoll[M002] + 2.*u[2]*CMcoll[M101] + u[0]*uz2;
        RMcoll[M012] = CMcoll[M012] + u[1]*CMcoll[M002] + 2.*u[2]*CMcoll[M011] + u[1]*uz2;
        
        RMcoll[M111] = CMcoll[M111] + u[2]*CMcoll[M110] + u[1]*CMcoll[M101] + u[0]*CMcoll[M011] + uxyz ;

        RMcoll[M220] = CMcoll[M220] + 2.*u[1]*CMcoll[M210] + 2.*u[0]*CMcoll[M120] + uy2*CMcoll[M200] + ux2*CMcoll[M020] + 4.*u[0]*u[1]*CMcoll[M110] + ux2*uy2;
        RMcoll[M202] = CMcoll[M202] + 2.*u[2]*CMcoll[M201] + 2.*u[0]*CMcoll[M102] + uz2*CMcoll[M200] + ux2*CMcoll[M002] + 4.*u[0]*u[2]*CMcoll[M101] + ux2*uz2;
        RMcoll[M022] = CMcoll[M022] + 2.*u[2]*CMcoll[M021] + 2.*u[1]*CMcoll[M012] + uz2*CMcoll[M020] + uy2*CMcoll[M002] + 4.*u[1]*u[2]*CMcoll[M011] + uy2*uz2;

        RMcoll[M211] = CMcoll[M211] + u[2]*CMcoll[M210] + u[1]*CMcoll[M201] + 2.*u[0]*CMcoll[M111] + u[1]*u[2]*CMcoll[M200] + 2.*u[0]*u[2]*CMcoll[M110] + 2.*u[0]*u[1]*CMcoll[M101] + ux2*CMcoll[M011] + ux2*u[1]*u[2];
        RMcoll[M121] = CMcoll[M121] + u[2]*CMcoll[M120] + u[0]*CMcoll[M021] + 2.*u[1]*CMcoll[M111] + u[0]*u[2]*CMcoll[M020] + 2.*u[1]*u[2]*CMcoll[M110] + 2.*u[0]*u[1]*CMcoll[M011] + uy2*CMcoll[M101] + u[0]*uy2*u[2];
        RMcoll[M112] = CMcoll[M112] + u[1]*CMcoll[M102] + u[0]*CMcoll[M012] + 2.*u[2]*CMcoll[M111] + u[0]*u[1]*CMcoll[M002] + 2.*u[1]*u[2]*CMcoll[M101] + 2.*u[0]*u[2]*CMcoll[M011] + uz2*CMcoll[M110] + u[0]*u[1]*uz2;

        RMcoll[M221] = CMcoll[M221] + u[2]*CMcoll[M220] + 2.*u[1]*CMcoll[M211] + 2.*u[0]*CMcoll[M121] + 2.*u[1]*u[2]*CMcoll[M210] + uy2*CMcoll[M201] + ux2*CMcoll[M021] + 2.*u[0]*u[2]*CMcoll[M120] + 4.*u[0]*u[1]*CMcoll[M111] + uy2*u[2]*CMcoll[M200] + ux2*u[2]*CMcoll[M020] + 4.*uxyz*CMcoll[M110] + 2.*u[0]*uy2*CMcoll[M101] + 2.*ux2*u[1]*CMcoll[M011] + ux2*uy2*u[2];
        RMcoll[M212] = CMcoll[M212] + u[1]*CMcoll[M202] + 2.*u[2]*CMcoll[M211] + 2.*u[0]*CMcoll[M112] + 2.*u[1]*u[2]*CMcoll[M201] + uz2*CMcoll[M210] + ux2*CMcoll[M012] + 2.*u[0]*u[1]*CMcoll[M102] + 4.*u[0]*u[2]*CMcoll[M111] + u[1]*uz2*CMcoll[M200] + ux2*u[1]*CMcoll[M002] + 4.*uxyz*CMcoll[M101] + 2.*u[0]*uz2*CMcoll[M110] + 2.*ux2*u[2]*CMcoll[M011] + ux2*u[1]*uz2;
        RMcoll[M122] = CMcoll[M122] + u[0]*CMcoll[M022] + 2.*u[2]*CMcoll[M121] + 2.*u[1]*CMcoll[M112] + 2.*u[0]*u[2]*CMcoll[M021] + uz2*CMcoll[M120] + uy2*CMcoll[M102] + 2.*u[0]*u[1]*CMcoll[M012] + 4.*u[1]*u[2]*CMcoll[M111] + u[0]*uz2*CMcoll[M020] + u[0]*uy2*CMcoll[M002] + 4.*uxyz*CMcoll[M011] + 2.*u[1]*uz2*CMcoll[M110] + 2.*uy2*u[2]*CMcoll[M101] + u[0]*uy2*uz2;

        RMcoll[M222] = CMcoll[M222] + 2.*u[2]*CMcoll[M221] + 2.*u[1]*CMcoll[M212] + 2.*u[0]*CMcoll[M122] + uz2*CMcoll[M220] + uy2*CMcoll[M202] + ux2*CMcoll[M022] + 4.*u[1]*u[2]*CMcoll[M211] + 4.*u[0]*u[2]*CMcoll[M121] + 4.*u[0]*u[1]*CMcoll[M112] + 2.*u[1]*uz2*CMcoll[M210] + 2.*uy2*u[2]*CMcoll[M201] + 2.*ux2*u[2]*CMcoll[M021] + 2.*u[0]*uz2*CMcoll[M120] + 2.*u[0]*uy2*CMcoll[M102] + 2.*ux2*u[1]*CMcoll[M012] + 8.*uxyz*CMcoll[M111] + uy2*uz2*CMcoll[M200] + ux2*uz2*CMcoll[M020] + ux2*uy2*CMcoll[M002] + 4.*u[0]*u[1]*uz2*CMcoll[M110] + 4.*u[0]*uy2*u[2]*CMcoll[M101] + 4.*ux2*u[1]*u[2]*CMcoll[M011] + ux2*uy2*uz2;

        // Optimization based on symmetries between populations and their opposite counterpart
        pop[F000] = rho *(1. - RMcoll[M200] - RMcoll[M020] - RMcoll[M002] + RMcoll[M220] + RMcoll[M202] + RMcoll[M022] - RMcoll[M222]);
        
        pop[FP00] = 0.5*rho * ( u[0] + RMcoll[M200] - RMcoll[M120] - RMcoll[M102] - RMcoll[M220] - RMcoll[M202] + RMcoll[M122] + RMcoll[M222]);
        pop[FM00] =     rho * (-u[0]                + RMcoll[M120] + RMcoll[M102]                               - RMcoll[M122])+ pop[FP00];

        pop[F0P0] = 0.5*rho * ( u[1] + RMcoll[M020] - RMcoll[M210] - RMcoll[M012] - RMcoll[M220] - RMcoll[M022] + RMcoll[M212] + RMcoll[M222]);
        pop[F0M0] =     rho * (-u[1]                + RMcoll[M210] + RMcoll[M012]                               - RMcoll[M212])+ pop[F0P0];

        pop[F00P] = 0.5*rho * ( u[2] + RMcoll[M002] - RMcoll[M201] - RMcoll[M021] - RMcoll[M202] - RMcoll[M022] + RMcoll[M221] + RMcoll[M222]);
        pop[F00M] =     rho * (-u[2]                + RMcoll[M201] + RMcoll[M021]                               - RMcoll[M221])+ pop[F00P];

        pop[FPP0] = 0.25*rho * ( RMcoll[M110] + RMcoll[M210] + RMcoll[M120] - RMcoll[M112] + RMcoll[M220] - RMcoll[M212] - RMcoll[M122] - RMcoll[M222]);
        pop[FMP0] =  0.5*rho * (-RMcoll[M110]                - RMcoll[M120] + RMcoll[M112]                               + RMcoll[M122])+ pop[FPP0];
        pop[FPM0] =  0.5*rho * (-RMcoll[M110] - RMcoll[M210]                + RMcoll[M112]                + RMcoll[M212]               )+ pop[FPP0];
        pop[FMM0] =  0.5*rho * (              - RMcoll[M210] - RMcoll[M120]                               + RMcoll[M212] + RMcoll[M122])+ pop[FPP0];

        pop[FP0P] = 0.25*rho * ( RMcoll[M101] + RMcoll[M201] + RMcoll[M102] - RMcoll[M121] + RMcoll[M202] - RMcoll[M221] - RMcoll[M122] - RMcoll[M222]);
        pop[FM0P] =  0.5*rho * (-RMcoll[M101]                - RMcoll[M102] + RMcoll[M121]                               + RMcoll[M122])+ pop[FP0P];
        pop[FP0M] =  0.5*rho * (-RMcoll[M101] - RMcoll[M201]                + RMcoll[M121]                + RMcoll[M221]               )+ pop[FP0P];
        pop[FM0M] =  0.5*rho * (              - RMcoll[M201] - RMcoll[M102]                               + RMcoll[M221] + RMcoll[M122])+ pop[FP0P];

        pop[F0PP] = 0.25*rho * ( RMcoll[M011] + RMcoll[M021] + RMcoll[M012] - RMcoll[M211] + RMcoll[M022] - RMcoll[M221] - RMcoll[M212] - RMcoll[M222]);
        pop[F0MP] =  0.5*rho * (-RMcoll[M011]                - RMcoll[M012] + RMcoll[M211]                               + RMcoll[M212])+ pop[F0PP];
        pop[F0PM] =  0.5*rho * (-RMcoll[M011] - RMcoll[M021]                + RMcoll[M211]                + RMcoll[M221]               )+ pop[F0PP];
        pop[F0MM] =  0.5*rho * (              - RMcoll[M021] - RMcoll[M012]                               + RMcoll[M221] + RMcoll[M212])+ pop[F0PP];

        pop[FPPP] = 0.125*rho * ( RMcoll[M111] + RMcoll[M211] + RMcoll[M121] + RMcoll[M112] + RMcoll[M221] + RMcoll[M212] + RMcoll[M122] + RMcoll[M222]);
        pop[FMPP] =  0.25*rho * (-RMcoll[M111]                - RMcoll[M121] - RMcoll[M112]                               - RMcoll[M122])+ pop[FPPP];
        pop[FPMP] =  0.25*rho * (-RMcoll[M111] - RMcoll[M211]                - RMcoll[M112]                - RMcoll[M212]               )+ pop[FPPP];
        pop[FPPM] =  0.25*rho * (-RMcoll[M111] - RMcoll[M211] - RMcoll[M121]                - RMcoll[M221]                              )+ pop[FPPP];
        pop[FMMP] =  0.25*rho * (              - RMcoll[M211] - RMcoll[M121]                               - RMcoll[M212] - RMcoll[M122])+ pop[FPPP];
        pop[FMPM] =  0.25*rho * (              - RMcoll[M211]                - RMcoll[M112] - RMcoll[M221]                - RMcoll[M122])+ pop[FPPP];
        pop[FPMM] =  0.25*rho * (                             - RMcoll[M121] - RMcoll[M112] - RMcoll[M221] - RMcoll[M212]               )+ pop[FPPP];
        pop[FMMM] =  0.25*rho * (-RMcoll[M111]                                              - RMcoll[M221] - RMcoll[M212] - RMcoll[M122])+ pop[FPPP];
    }

    auto streamingPull(int i, int iX, int iY, int iZ, std::array<double, 27>& pop)
    {
        if(periodic){
            for (int k = 0; k < 27; ++k) {
                int XX = (iX - c[k][0] + dim.nx) % dim.nx;
                int YY = (iY - c[k][1] + dim.ny) % dim.ny;
                int ZZ = (iZ - c[k][2] + dim.nz) % dim.nz;
                size_t nb = xyz_to_i(XX, YY, ZZ);
                CellType nbCellType = flag[nb];
                if (nbCellType == CellType::bounce_back) {
                    pop[k] = f(i, k) + f(nb, opp[k]);
                }
                else {
                    pop[k] = f(nb, opp[k]);
                }
            }
        }
        else {
            for (int k = 0; k < 27; ++k) {
                int XX = iX - c[k][0];
                int YY = iY - c[k][1];
                int ZZ = iZ - c[k][2];
                size_t nb = xyz_to_i(XX, YY, ZZ);
                CellType nbCellType = flag[nb];
                if (nbCellType == CellType::bounce_back) {
                    pop[k] = f(i, k) + f(nb, opp[k]);
                }
                else {
                    pop[k] = f(nb, opp[k]);
                }
            }
        }
    }

    auto streamingPush(int i, int iX, int iY, int iZ, std::array<double, 27>& pop)
    {
        if(periodic){
            for (int k = 0; k < 27; ++k) {
                int XX = (iX + c[k][0] + dim.nx) % dim.nx;
                int YY = (iY + c[k][1] + dim.ny) % dim.ny;
                int ZZ = (iZ + c[k][2] + dim.nz) % dim.nz;
                size_t nb = xyz_to_i(XX, YY, ZZ);
                CellType nbCellType = flag[nb];
                if (nbCellType == CellType::bounce_back) {
                    f(i, opp[k]) = pop[k] + f(nb, k);
                }
                else {
                    f(nb, k) = pop[k];
                }
            }
        }
        else {
            for (int k = 0; k < 27; ++k) {
                int XX = iX + c[k][0];
                int YY = iY + c[k][1];
                int ZZ = iZ + c[k][2];
                size_t nb = xyz_to_i(XX, YY, ZZ);
                CellType nbCellType = flag[nb];
                if (nbCellType == CellType::bounce_back) {
                    f(i, opp[k]) = pop[k] + f(nb, k);
                }
                else {
                    f(nb, k) = pop[k];
                }
            }
        }
    }

    void iterateCM(double& f0) {
        int i = &f0 - lattice;
        auto[iX, iY, iZ] = i_to_xyz(i);
        CellType cellType = flag[i];

        if (cellType == CellType::bulk) {
            std::array<double, 27> pop;

            streamingPull(i, iX, iY, iZ, pop);

/*          auto CM = computeCM(pop);
            auto CM = computeCMopt(pop);*/
            auto CM = computeCMopt2(pop);
            double rho = CM[M000];
            std::array<double, 3> u = {CM[M100], CM[M010], CM[M001]};
            auto CMeq = computeCMeq();

            collideCM(pop, rho, u, CM, CMeq);

            streamingPush(i, iX, iY, iZ, pop);
        }
    }

    void operator() (double& f0) {
        iterateCM(f0);
    }
};

} // namespace aa_soa_cm
