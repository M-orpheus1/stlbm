// *****************************************************************************
// STLBM SOFTWARE LIBRARY

// Copyright © 2020 University of Geneva
// Authors: Jonas Latt, Christophe Coreixas, Joël Beny
// Contact: Jonas.Latt@unige.ch

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// *****************************************************************************

#pragma once

#include "lbm_q27.h"
#include <vector>
#include <array>
#include <tuple>

namespace aa_soa_hm {

struct LBM {
    using CellData = double;
    static size_t sizeOfLattice(size_t nelem) { return 27 * nelem; }

    CellData* lattice;
    CellType* flag;
    std::array<int, 3>* c;
    int* opp;
    double* t;
    double omega1;
    Dim dim;
    LBModel model;
    bool periodic = false;
    double omegaBulk = 0.;
    double omega2 = omega1;
    double omega3 = omega1;
    double omega4 = omega1;
    double omega5 = omega1;
    double omega6 = omega1;
    double omega7 = omega1;
    double omega8 = omega1;

    auto i_to_xyz (int i) const {
        int iX = i / (dim.ny * dim.nz);
        int remainder = i % (dim.ny * dim.nz);
        int iY = remainder / dim.nz;
        int iZ = remainder % dim.nz;
        return std::make_tuple(iX, iY, iZ);
    };

    size_t xyz_to_i (int x, int y, int z) const {
        return z + dim.nz * (y + dim.ny * x);
    };

    double& f (int i, int k) {
        return lattice[k * dim.nelem + i];
    }

    auto iniLattice (double& f0) {
        auto i = &f0 - lattice;
        for (int k = 0; k < 27; ++k) {
            f(i, k) = t[k];
        }
    };

    auto iniLattice (double& f0, double rho, std::array<double, 3> const& u) {
        auto i = &f0 - lattice;
        double usqr = 1.5 * (u[0] * u[0] + u[1] * u[1] + u[2] * u[2]);
        for (int k = 0; k < 27; ++k) {
            double ck_u = c[k][0] * u[0] + c[k][1] * u[1] + c[k][2] * u[2];
            f(i, k) = rho * t[k] * (1. + 3. * ck_u + 4.5 * ck_u * ck_u - usqr);
        }
    };

    auto macro (double const& f0) {
        auto i = &f0 - lattice;
        double X_M1 = f(i, 0) + f(i, 3) + f(i, 4) + f(i, 5) + f(i, 6) + f(i, 9) + f(i,10) + f(i,11) + f(i,12);
        double X_P1 = f(i,14) + f(i,17) + f(i,18) + f(i,19) + f(i,20) + f(i,23) + f(i,24) + f(i,25) + f(i,26);
        double X_0  = f(i, 1) + f(i, 2) + f(i, 7) + f(i, 8) + f(i,13) + f(i,15) + f(i,16) + f(i,21) + f(i,22);

        double Y_M1 = f(i, 1) + f(i, 3) + f(i, 7) + f(i, 8) + f(i, 9) + f(i,10) + f(i,18) + f(i,25) + f(i,26);
        double Y_P1 = f(i,15) + f(i,17) + f(i,21) + f(i,22) + f(i,23) + f(i,24) + f(i, 4) + f(i,11) + f(i,12);

        double Z_M1 = f(i, 2) + f(i, 5) + f(i, 7) + f(i, 9) + f(i,11) + f(i,20) + f(i,22) + f(i,24) + f(i,26);
        double Z_P1 = f(i,16) + f(i,19) + f(i,21) + f(i,23) + f(i,25) + f(i, 6) + f(i, 8) + f(i,10) + f(i,12);

        double rho = X_M1 + X_P1 + X_0;
        std::array<double, 3> u{ (X_P1 - X_M1) / rho, (Y_P1 - Y_M1) / rho, (Z_P1 - Z_M1) / rho };
        return make_pair(rho, u);
    }

    auto macropop (std::array<double, 27> const& pop) {
        double X_M1 = pop[ 0] + pop[ 3] + pop[ 4] + pop[ 5] + pop[ 6] + pop[ 9] + pop[10] + pop[11] + pop[12];
        double X_P1 = pop[14] + pop[17] + pop[18] + pop[19] + pop[20] + pop[23] + pop[24] + pop[25] + pop[26];
        double X_0  = pop[ 1] + pop[ 2] + pop[ 7] + pop[ 8] + pop[13] + pop[15] + pop[16] + pop[21] + pop[22];

        double Y_M1 = pop[ 1] + pop[ 3] + pop[ 7] + pop[ 8] + pop[ 9] + pop[10] + pop[18] + pop[25] + pop[26];
        double Y_P1 = pop[15] + pop[17] + pop[21] + pop[22] + pop[23] + pop[24] + pop[ 4] + pop[11] + pop[12];

        double Z_M1 = pop[ 2] + pop[ 5] + pop[ 7] + pop[ 9] + pop[11] + pop[20] + pop[22] + pop[24] + pop[26];
        double Z_P1 = pop[16] + pop[19] + pop[21] + pop[23] + pop[25] + pop[ 6] + pop[ 8] + pop[10] + pop[12];

        double rho = X_M1 + X_P1 + X_0;
        std::array<double, 3> u{ (X_P1 - X_M1) / rho, (Y_P1 - Y_M1) / rho, (Z_P1 - Z_M1) / rho };
        return make_pair(rho, u);
    };

    // Naive way to compute HM and macros
    auto computeHM(std::array<double, 27> const& pop) {
        std::array<double, 27> HM;
        std::fill(HM.begin(), HM.end(), 0.);
        double Hxx, Hyy, Hzz;
        double cs2 = 1./3.;
        for (int k = 0; k<27; ++k) {
            Hxx = c[k][0] * c[k][0] - cs2;
            Hyy = c[k][1] * c[k][1] - cs2;
            Hzz = c[k][2] * c[k][2] - cs2;
            // Order 0
            HM[M000] += pop[k];
            // Order 1
            HM[M100] += c[k][0] * pop[k];
            HM[M010] += c[k][1] * pop[k];
            HM[M001] += c[k][2] * pop[k];
            // Order 2
            HM[M200] += Hxx * pop[k];
            HM[M020] += Hyy * pop[k];
            HM[M002] += Hzz * pop[k];
            HM[M110] += c[k][0] * c[k][1] * pop[k];
            HM[M101] += c[k][0] * c[k][2] * pop[k];
            HM[M011] += c[k][1] * c[k][2] * pop[k];
            // Order 3
            HM[M210] += Hxx * c[k][1] * pop[k];
            HM[M201] += Hxx * c[k][2] * pop[k];
            HM[M021] += Hyy * c[k][2] * pop[k];
            HM[M120] += c[k][0] * Hyy * pop[k];
            HM[M102] += c[k][0] * Hzz * pop[k];
            HM[M012] += c[k][1] * Hzz * pop[k];
            HM[M111] += c[k][0] * c[k][1] * c[k][2] * pop[k];
            // Order 4
            HM[M220] += Hxx * Hyy * pop[k];
            HM[M202] += Hxx * Hzz * pop[k];
            HM[M022] += Hyy * Hzz * pop[k];
            HM[M211] += Hxx * c[k][1] * c[k][2] * pop[k];
            HM[M121] += c[k][0] * Hyy * c[k][2] * pop[k];
            HM[M112] += c[k][0] * c[k][1] * Hzz * pop[k];
            // Order 5
            HM[M221] += Hxx * Hyy * c[k][2] * pop[k];
            HM[M212] += Hxx * c[k][1] * Hzz * pop[k];
            HM[M122] += c[k][0] * Hyy * Hzz * pop[k];
            // Order 6
            HM[M222] += Hxx * Hyy * Hzz * pop[k];
        }
        double invRho = 1. / HM[M000];
        for (int k = 1; k<27; ++k) {
            HM[k] *= invRho;
        }
        return HM;
    }

    // First optimization (loop unrolling)
    auto computeHMopt(std::array<double, 27> const& pop) {
        std::array<double, 27> HM;
        std::fill(HM.begin(), HM.end(), 0.);
        // Order 0
        HM[M000] =  pop[ 0] + pop[ 1] + pop[ 2] + pop[ 3] + pop[ 4] + pop[ 5] + pop[ 6] + pop[ 7] + pop[ 8] + pop[ 9] + pop[10] + pop[11] + pop[12] + pop[13] + pop[14] + pop[15] + pop[16] + pop[17] + pop[18] + pop[19] + pop[20] + pop[21] + pop[22] + pop[23] + pop[24] + pop[25] + pop[26];
        double invRho = 1./HM[M000];
        // Order 1
        HM[M100] = invRho * (- pop[ 0] - pop[ 3] - pop[ 4] - pop[ 5] - pop[ 6] - pop[ 9] - pop[10] - pop[11] - pop[12] + pop[14] + pop[17] + pop[18] + pop[19] + pop[20] + pop[23] + pop[24] + pop[25] + pop[26]);
        HM[M010] = invRho * (- pop[ 1] - pop[ 3] + pop[ 4] - pop[ 7] - pop[ 8] - pop[ 9] - pop[10] + pop[11] + pop[12] + pop[15] + pop[17] - pop[18] + pop[21] + pop[22] + pop[23] + pop[24] - pop[25] - pop[26]);
        HM[M001] = invRho * (- pop[ 2] - pop[ 5] + pop[ 6] - pop[ 7] + pop[ 8] - pop[ 9] + pop[10] - pop[11] + pop[12] + pop[16] + pop[19] - pop[20] + pop[21] - pop[22] + pop[23] - pop[24] + pop[25] - pop[26]);
        // Order 2
        HM[M200] = invRho * (  pop[ 0] + pop[ 3] + pop[ 4] + pop[ 5] + pop[ 6] + pop[ 9] + pop[10] + pop[11] + pop[12] + pop[14] + pop[17] + pop[18] + pop[19] + pop[20] + pop[23] + pop[24] + pop[25] + pop[26]);
        HM[M020] = invRho * (  pop[ 1] + pop[ 3] + pop[ 4] + pop[ 7] + pop[ 8] + pop[ 9] + pop[10] + pop[11] + pop[12] + pop[15] + pop[17] + pop[18] + pop[21] + pop[22] + pop[23] + pop[24] + pop[25] + pop[26]);
        HM[M002] = invRho * (  pop[ 2] + pop[ 5] + pop[ 6] + pop[ 7] + pop[ 8] + pop[ 9] + pop[10] + pop[11] + pop[12] + pop[16] + pop[19] + pop[20] + pop[21] + pop[22] + pop[23] + pop[24] + pop[25] + pop[26]);
        HM[M110] = invRho * (  pop[ 3] - pop[ 4] + pop[ 9] + pop[10] - pop[11] - pop[12] + pop[17] - pop[18] + pop[23] + pop[24] - pop[25] - pop[26]);
        HM[M101] = invRho * (  pop[ 5] - pop[ 6] + pop[ 9] - pop[10] + pop[11] - pop[12] + pop[19] - pop[20] + pop[23] - pop[24] + pop[25] - pop[26]);
        HM[M011] = invRho * (  pop[ 7] - pop[ 8] + pop[ 9] - pop[10] - pop[11] + pop[12] + pop[21] - pop[22] + pop[23] - pop[24] - pop[25] + pop[26]);
        // Order 3
        HM[M210] = invRho * (- pop[ 3] + pop[ 4] - pop[ 9] - pop[10] + pop[11] + pop[12] + pop[17] - pop[18] + pop[23] + pop[24] - pop[25] - pop[26]);
        HM[M201] = invRho * (- pop[ 5] + pop[ 6] - pop[ 9] + pop[10] - pop[11] + pop[12] + pop[19] - pop[20] + pop[23] - pop[24] + pop[25] - pop[26]);
        HM[M021] = invRho * (- pop[ 7] + pop[ 8] - pop[ 9] + pop[10] - pop[11] + pop[12] + pop[21] - pop[22] + pop[23] - pop[24] + pop[25] - pop[26]);
        HM[M120] = invRho * (- pop[ 3] - pop[ 4] - pop[ 9] - pop[10] - pop[11] - pop[12] + pop[17] + pop[18] + pop[23] + pop[24] + pop[25] + pop[26]);
        HM[M102] = invRho * (- pop[ 5] - pop[ 6] - pop[ 9] - pop[10] - pop[11] - pop[12] + pop[19] + pop[20] + pop[23] + pop[24] + pop[25] + pop[26]);
        HM[M012] = invRho * (- pop[ 7] - pop[ 8] - pop[ 9] - pop[10] + pop[11] + pop[12] + pop[21] + pop[22] + pop[23] + pop[24] - pop[25] - pop[26]);
        HM[M111] = invRho * (- pop[ 9] + pop[10] + pop[11] - pop[12] + pop[23] - pop[24] - pop[25] + pop[26]);
        // Order 4
        HM[M220] = invRho * (  pop[ 3] + pop[ 4] + pop[ 9] + pop[10] + pop[11] + pop[12] + pop[17] + pop[18] + pop[23] + pop[24] + pop[25] + pop[26]);
        HM[M202] = invRho * (  pop[ 5] + pop[ 6] + pop[ 9] + pop[10] + pop[11] + pop[12] + pop[19] + pop[20] + pop[23] + pop[24] + pop[25] + pop[26]);
        HM[M022] = invRho * (  pop[ 7] + pop[ 8] + pop[ 9] + pop[10] + pop[11] + pop[12] + pop[21] + pop[22] + pop[23] + pop[24] + pop[25] + pop[26]);
        HM[M211] = invRho * (  pop[ 9] - pop[10] - pop[11] + pop[12] + pop[23] - pop[24] - pop[25] + pop[26]);
        HM[M121] = invRho * (  pop[ 9] - pop[10] + pop[11] - pop[12] + pop[23] - pop[24] + pop[25] - pop[26]);
        HM[M112] = invRho * (  pop[ 9] + pop[10] - pop[11] - pop[12] + pop[23] + pop[24] - pop[25] - pop[26]);
        // Order 5
        HM[M221] = invRho * (- pop[ 9] + pop[10] - pop[11] + pop[12] + pop[23] - pop[24] + pop[25] - pop[26]);
        HM[M212] = invRho * (- pop[ 9] - pop[10] + pop[11] + pop[12] + pop[23] + pop[24] - pop[25] - pop[26]);
        HM[M122] = invRho * (- pop[ 9] - pop[10] - pop[11] - pop[12] + pop[23] + pop[24] + pop[25] + pop[26]);
        // Order 6
        HM[M222] = invRho * (  pop[ 9] + pop[10] + pop[11] + pop[12] + pop[23] + pop[24] + pop[25] + pop[26]);

        // We come back to Hermite moments
        double cs2 = 1./3.;
        double cs4 = cs2*cs2;
        HM[M200] -= cs2;
        HM[M020] -= cs2;
        HM[M002] -= cs2;

        HM[M210] -= cs2 * HM[M010];
        HM[M201] -= cs2 * HM[M001];
        HM[M021] -= cs2 * HM[M001];
        HM[M120] -= cs2 * HM[M100];
        HM[M102] -= cs2 * HM[M100];
        HM[M012] -= cs2 * HM[M010];

        HM[M220] -= (cs2*(HM[M200] + HM[M020]) + cs4);
        HM[M202] -= (cs2*(HM[M200] + HM[M002]) + cs4);
        HM[M022] -= (cs2*(HM[M020] + HM[M002]) + cs4);
        HM[M211] -= (cs2*HM[M011]);
        HM[M121] -= (cs2*HM[M101]);
        HM[M112] -= (cs2*HM[M110]);

        HM[M221] -= (cs2*(HM[M201] + HM[M021]) + cs4*HM[M001]);
        HM[M212] -= (cs2*(HM[M210] + HM[M012]) + cs4*HM[M010]);
        HM[M122] -= (cs2*(HM[M120] + HM[M102]) + cs4*HM[M100]);

        HM[M222] -= (cs2*(HM[M220] + HM[M202] + HM[M022]) + cs4*(HM[M200] + HM[M020] + HM[M002]) + cs2*cs4);

        return HM;
    }

    // Most optimized version (loop unrolling + redundant terms)
    // Further pre-computing terms is counter-productive (less than 1% gain)
    auto computeHMopt2(std::array<double, 27> const& pop) {
        std::array<double, 27> HM;
        std::fill(HM.begin(), HM.end(), 0.);

        double A1 = pop[ 9] + pop[10] + pop[11] + pop[12];
        double A2 = pop[23] + pop[24] + pop[25] + pop[26];
        double A3 = pop[ 9] + pop[10] - pop[11] - pop[12];
        double A4 = pop[23] + pop[24] - pop[25] - pop[26];
        double A5 = pop[ 9] - pop[10] + pop[11] - pop[12];
        double A6 = pop[23] - pop[24] + pop[25] - pop[26];
        double A7 = pop[ 9] - pop[10] - pop[11] + pop[12];
        double A8 = pop[23] - pop[24] - pop[25] + pop[26];

        double X_M1 = pop[ 0] + pop[ 3] + pop[ 4] + pop[ 5] + pop[ 6] + A1;
        double X_P1 = pop[14] + pop[17] + pop[18] + pop[19] + pop[20] + A2;
        double X_0  = pop[ 1] + pop[ 2] + pop[ 7] + pop[ 8] + pop[13] + pop[15] + pop[16] + pop[21] + pop[22];

        double Y_M1 = pop[ 1] + pop[ 3] + pop[ 7] + pop[ 8] + pop[ 9] + pop[10] + pop[18] + pop[25] + pop[26];
        double Y_P1 = pop[15] + pop[17] + pop[21] + pop[22] + pop[23] + pop[24] + pop[ 4] + pop[11] + pop[12];

        double Z_M1 = pop[ 2] + pop[ 5] + pop[ 7] + pop[ 9] + pop[11] + pop[20] + pop[22] + pop[24] + pop[26];
        double Z_P1 = pop[16] + pop[19] + pop[21] + pop[23] + pop[25] + pop[ 6] + pop[ 8] + pop[10] + pop[12];

        // Order 0
        HM[M000] = X_M1 + X_P1 + X_0;
        double invRho = 1. / HM[M000];

        // Order 6
        HM[M222] = invRho * (  A1 + A2);
        // Order 5
        HM[M221] = invRho * (- A5 + A6);
        HM[M212] = invRho * (- A3 + A4);
        HM[M122] = invRho * (- A1 + A2);
        // Order 4
        HM[M220] = invRho * (  pop[ 3] + pop[ 4] + pop[17] + pop[18]) + HM[M222];
        HM[M202] = invRho * (  pop[ 5] + pop[ 6] + pop[19] + pop[20]) + HM[M222];
        HM[M022] = invRho * (  pop[ 7] + pop[ 8] + pop[21] + pop[22]) + HM[M222];
        HM[M211] = invRho * (  A7 + A8);
        HM[M121] = invRho * (  A5 + A6);
        HM[M112] = invRho * (  A3 + A4);
        // Order 3
        HM[M210] = invRho * (- pop[ 3] + pop[ 4] + pop[17] - pop[18]) + HM[M212];
        HM[M201] = invRho * (- pop[ 5] + pop[ 6] + pop[19] - pop[20]) + HM[M221];
        HM[M021] = invRho * (- pop[ 7] + pop[ 8] + pop[21] - pop[22]) + HM[M221];
        HM[M120] = invRho * (- pop[ 3] - pop[ 4] + pop[17] + pop[18]) + HM[M122];
        HM[M102] = invRho * (- pop[ 5] - pop[ 6] + pop[19] + pop[20]) + HM[M122];
        HM[M012] = invRho * (- pop[ 7] - pop[ 8] + pop[21] + pop[22]) + HM[M212];
        HM[M111] = invRho * (- A7 + A8);
        // Order 2
        HM[M200] = invRho * (X_P1 + X_M1);
        HM[M020] = invRho * (Y_P1 + Y_M1); 
        HM[M002] = invRho * (Z_P1 + Z_M1);
        HM[M110] = invRho * (  pop[ 3] - pop[ 4] + pop[17] - pop[18]) + HM[M112];
        HM[M101] = invRho * (  pop[ 5] - pop[ 6] + pop[19] - pop[20]) + HM[M121];
        HM[M011] = invRho * (  pop[ 7] - pop[ 8] + pop[21] - pop[22]) + HM[M211];
        // Order 1
        HM[M100] = invRho * (X_P1 - X_M1);
        HM[M010] = invRho * (Y_P1 - Y_M1); 
        HM[M001] = invRho * (Z_P1 - Z_M1);

        // We come back to Hermite moments
        double cs2 = 1./3.;
        double cs4 = cs2*cs2;
        HM[M200] -= cs2;
        HM[M020] -= cs2;
        HM[M002] -= cs2;

        HM[M210] -= cs2 * HM[M010];
        HM[M201] -= cs2 * HM[M001];
        HM[M021] -= cs2 * HM[M001];
        HM[M120] -= cs2 * HM[M100];
        HM[M102] -= cs2 * HM[M100];
        HM[M012] -= cs2 * HM[M010];

        HM[M220] -= (cs2*(HM[M200] + HM[M020]) + cs4);
        HM[M202] -= (cs2*(HM[M200] + HM[M002]) + cs4);
        HM[M022] -= (cs2*(HM[M020] + HM[M002]) + cs4);
        HM[M211] -= (cs2*HM[M011]);
        HM[M121] -= (cs2*HM[M101]);
        HM[M112] -= (cs2*HM[M110]);

        HM[M221] -= (cs2*(HM[M201] + HM[M021]) + cs4*HM[M001]);
        HM[M212] -= (cs2*(HM[M210] + HM[M012]) + cs4*HM[M010]);
        HM[M122] -= (cs2*(HM[M120] + HM[M102]) + cs4*HM[M100]);

        HM[M222] -= (cs2*(HM[M220] + HM[M202] + HM[M022]) + cs4*(HM[M200] + HM[M020] + HM[M002]) + cs2*cs4);

        return HM;
    }

    auto computeHMeq(std::array<double, 3> const& u) {

        std::array<double, 27> HMeq;
        std::fill(HMeq.begin(), HMeq.end(), 0.);

        // Order 2
        HMeq[M200] = u[0] * u[0];
        HMeq[M020] = u[1] * u[1];
        HMeq[M002] = u[2] * u[2];
        HMeq[M110] = u[0] * u[1];
        HMeq[M101] = u[0] * u[2];
        HMeq[M011] = u[1] * u[2];
        // Order 3
        HMeq[M210] = HMeq[M200] * u[1];
        HMeq[M201] = HMeq[M200] * u[2];
        HMeq[M021] = HMeq[M020] * u[2];
        HMeq[M120] = HMeq[M020] * u[0];
        HMeq[M102] = HMeq[M002] * u[0];
        HMeq[M012] = HMeq[M002] * u[1];
        HMeq[M111] = HMeq[M110] * u[2];
        // Order 4
        HMeq[M220] = HMeq[M200] * HMeq[M020];
        HMeq[M202] = HMeq[M200] * HMeq[M002];
        HMeq[M022] = HMeq[M020] * HMeq[M002];
        HMeq[M211] = HMeq[M200] * HMeq[M011];
        HMeq[M121] = HMeq[M020] * HMeq[M101];
        HMeq[M112] = HMeq[M002] * HMeq[M110];
        // Order 5
        HMeq[M221] = HMeq[M220] * u[2];
        HMeq[M212] = HMeq[M202] * u[1];
        HMeq[M122] = HMeq[M022] * u[0];
        // Order 6
        HMeq[M222] = HMeq[M220] * HMeq[M002];

        return HMeq;
    }
};

struct Even : public LBM {

    auto collideHM(int i, double rho, std::array<double, 3> const& u, std::array<double, 27> const& HM, std::array<double, 27> const& HMeq)
    {
        // Post-collision  moments.
        std::array<double, 27> HMcoll;
        std::array<double, 27> RMcoll;

        // Collision in the Hermite moment space
        // Order 2
        if (omegaBulk == 0.) { // Don't adjust bulk viscosity 
            HMcoll[M200] = (1. - omega1) * HM[M200] + omega1 * HMeq[M200];
            HMcoll[M020] = (1. - omega1) * HM[M020] + omega1 * HMeq[M020];
            HMcoll[M002] = (1. - omega1) * HM[M002] + omega1 * HMeq[M002];
        }
        else { // Adjust bulk viscosity
            const double omegaMinus = (omegaBulk - omega1)/3.; // Notation used by Fei
            const double omegaPlus  = omegaMinus + omega1;     // Notation used by Fei
            HMcoll[M200] = HM[M200] - omegaPlus  * (HM[M200]-HMeq[M200]) - omegaMinus * (HM[M020]-HMeq[M020]) - omegaMinus * (HM[M002]-HMeq[M002]) ;
            HMcoll[M020] = HM[M020] - omegaMinus * (HM[M200]-HMeq[M200]) - omegaPlus  * (HM[M020]-HMeq[M020]) - omegaMinus * (HM[M002]-HMeq[M002]) ;
            HMcoll[M002] = HM[M002] - omegaMinus * (HM[M200]-HMeq[M200]) - omegaMinus * (HM[M020]-HMeq[M020]) - omegaPlus  * (HM[M002]-HMeq[M002]) ;
        }

        HMcoll[M110] = (1. - omega2) * HM[M110] + omega2 * HMeq[M110] ;
        HMcoll[M101] = (1. - omega2) * HM[M101] + omega2 * HMeq[M101] ;
        HMcoll[M011] = (1. - omega2) * HM[M011] + omega2 * HMeq[M011] ;
        // Order 3
        HMcoll[M210] = (1. - omega3) * HM[M210] + omega3 * HMeq[M210] ;
        HMcoll[M201] = (1. - omega3) * HM[M201] + omega3 * HMeq[M201] ;
        HMcoll[M021] = (1. - omega3) * HM[M021] + omega3 * HMeq[M021] ;
        HMcoll[M120] = (1. - omega3) * HM[M120] + omega3 * HMeq[M120] ;
        HMcoll[M102] = (1. - omega3) * HM[M102] + omega3 * HMeq[M102] ;
        HMcoll[M012] = (1. - omega3) * HM[M012] + omega3 * HMeq[M012] ;
        HMcoll[M111] = (1. - omega4) * HM[M111] + omega4 * HMeq[M111] ;
        // Order 4
        HMcoll[M220] = (1. - omega5) * HM[M220] + omega5 * HMeq[M220] ;
        HMcoll[M202] = (1. - omega5) * HM[M202] + omega5 * HMeq[M202] ;
        HMcoll[M022] = (1. - omega5) * HM[M022] + omega5 * HMeq[M022] ;
        HMcoll[M211] = (1. - omega6) * HM[M211] + omega6 * HMeq[M211] ;
        HMcoll[M121] = (1. - omega6) * HM[M121] + omega6 * HMeq[M121] ;
        HMcoll[M112] = (1. - omega6) * HM[M112] + omega6 * HMeq[M112] ;
        // Order 5
        HMcoll[M221] = (1. - omega7) * HM[M221] + omega7 * HMeq[M221] ;
        HMcoll[M212] = (1. - omega7) * HM[M212] + omega7 * HMeq[M212] ;
        HMcoll[M122] = (1. - omega7) * HM[M122] + omega7 * HMeq[M122] ;
        // Order 6
        HMcoll[M222] = (1. - omega8) * HM[M222] + omega8 * HMeq[M222] ;

        // Come back to RMcoll using relationships between HMs and RMs
        double cs2 = 1./3.;
        double cs4 = cs2*cs2;

        RMcoll[M200] = HMcoll[M200] + cs2;
        RMcoll[M020] = HMcoll[M020] + cs2;
        RMcoll[M002] = HMcoll[M002] + cs2;
        
        RMcoll[M110] = HMcoll[M110];
        RMcoll[M101] = HMcoll[M101];
        RMcoll[M011] = HMcoll[M011];

        RMcoll[M210] = HMcoll[M210] + cs2*u[1];
        RMcoll[M201] = HMcoll[M201] + cs2*u[2];
        RMcoll[M021] = HMcoll[M021] + cs2*u[2];
        RMcoll[M120] = HMcoll[M120] + cs2*u[0];
        RMcoll[M102] = HMcoll[M102] + cs2*u[0];
        RMcoll[M012] = HMcoll[M012] + cs2*u[1];
        
        RMcoll[M111] = HMcoll[M111];

        RMcoll[M220] = HMcoll[M220] + cs2*(HMcoll[M200] + HMcoll[M020]) + cs4;
        RMcoll[M202] = HMcoll[M202] + cs2*(HMcoll[M200] + HMcoll[M002]) + cs4;
        RMcoll[M022] = HMcoll[M022] + cs2*(HMcoll[M020] + HMcoll[M002]) + cs4;

        RMcoll[M211] = HMcoll[M211] + cs2*HMcoll[M011];
        RMcoll[M121] = HMcoll[M121] + cs2*HMcoll[M101];
        RMcoll[M112] = HMcoll[M112] + cs2*HMcoll[M110];

        RMcoll[M221] = HMcoll[M221] + cs2*(HMcoll[M201] + HMcoll[M021]) + cs4*u[2];
        RMcoll[M212] = HMcoll[M212] + cs2*(HMcoll[M210] + HMcoll[M012]) + cs4*u[1];
        RMcoll[M122] = HMcoll[M122] + cs2*(HMcoll[M120] + HMcoll[M102]) + cs4*u[0];

        RMcoll[M222] = HMcoll[M222] + cs2*(HMcoll[M220] + HMcoll[M202] + HMcoll[M022]) + cs4*(HMcoll[M200] + HMcoll[M020] + HMcoll[M002]) + cs2*cs4;

        // Optimization based on symmetries between populations and their opposite counterpart
        std::array<double, 27> foutRM;

        foutRM[F000] = rho *(1. - RMcoll[M200] - RMcoll[M020] - RMcoll[M002] + RMcoll[M220] + RMcoll[M202] + RMcoll[M022] - RMcoll[M222]);
        
        foutRM[FP00] = 0.5*rho * ( u[0] + RMcoll[M200] - RMcoll[M120] - RMcoll[M102] - RMcoll[M220] - RMcoll[M202] + RMcoll[M122] + RMcoll[M222]);
        foutRM[FM00] =     rho * (-u[0]                + RMcoll[M120] + RMcoll[M102]                               - RMcoll[M122])+ foutRM[FP00];

        foutRM[F0P0] = 0.5*rho * ( u[1] + RMcoll[M020] - RMcoll[M210] - RMcoll[M012] - RMcoll[M220] - RMcoll[M022] + RMcoll[M212] + RMcoll[M222]);
        foutRM[F0M0] =     rho * (-u[1]                + RMcoll[M210] + RMcoll[M012]                               - RMcoll[M212])+ foutRM[F0P0];

        foutRM[F00P] = 0.5*rho * ( u[2] + RMcoll[M002] - RMcoll[M201] - RMcoll[M021] - RMcoll[M202] - RMcoll[M022] + RMcoll[M221] + RMcoll[M222]);
        foutRM[F00M] =     rho * (-u[2]                + RMcoll[M201] + RMcoll[M021]                               - RMcoll[M221])+ foutRM[F00P];

        foutRM[FPP0] = 0.25*rho * ( RMcoll[M110] + RMcoll[M210] + RMcoll[M120] - RMcoll[M112] + RMcoll[M220] - RMcoll[M212] - RMcoll[M122] - RMcoll[M222]);
        foutRM[FMP0] =  0.5*rho * (-RMcoll[M110]                - RMcoll[M120] + RMcoll[M112]                               + RMcoll[M122])+ foutRM[FPP0];
        foutRM[FPM0] =  0.5*rho * (-RMcoll[M110] - RMcoll[M210]                + RMcoll[M112]                + RMcoll[M212]               )+ foutRM[FPP0];
        foutRM[FMM0] =  0.5*rho * (              - RMcoll[M210] - RMcoll[M120]                               + RMcoll[M212] + RMcoll[M122])+ foutRM[FPP0];

        foutRM[FP0P] = 0.25*rho * ( RMcoll[M101] + RMcoll[M201] + RMcoll[M102] - RMcoll[M121] + RMcoll[M202] - RMcoll[M221] - RMcoll[M122] - RMcoll[M222]);
        foutRM[FM0P] =  0.5*rho * (-RMcoll[M101]                - RMcoll[M102] + RMcoll[M121]                               + RMcoll[M122])+ foutRM[FP0P];
        foutRM[FP0M] =  0.5*rho * (-RMcoll[M101] - RMcoll[M201]                + RMcoll[M121]                + RMcoll[M221]               )+ foutRM[FP0P];
        foutRM[FM0M] =  0.5*rho * (              - RMcoll[M201] - RMcoll[M102]                               + RMcoll[M221] + RMcoll[M122])+ foutRM[FP0P];

        foutRM[F0PP] = 0.25*rho * ( RMcoll[M011] + RMcoll[M021] + RMcoll[M012] - RMcoll[M211] + RMcoll[M022] - RMcoll[M221] - RMcoll[M212] - RMcoll[M222]);
        foutRM[F0MP] =  0.5*rho * (-RMcoll[M011]                - RMcoll[M012] + RMcoll[M211]                               + RMcoll[M212])+ foutRM[F0PP];
        foutRM[F0PM] =  0.5*rho * (-RMcoll[M011] - RMcoll[M021]                + RMcoll[M211]                + RMcoll[M221]               )+ foutRM[F0PP];
        foutRM[F0MM] =  0.5*rho * (              - RMcoll[M021] - RMcoll[M012]                               + RMcoll[M221] + RMcoll[M212])+ foutRM[F0PP];

        foutRM[FPPP] = 0.125*rho * ( RMcoll[M111] + RMcoll[M211] + RMcoll[M121] + RMcoll[M112] + RMcoll[M221] + RMcoll[M212] + RMcoll[M122] + RMcoll[M222]);
        foutRM[FMPP] =  0.25*rho * (-RMcoll[M111]                - RMcoll[M121] - RMcoll[M112]                               - RMcoll[M122])+ foutRM[FPPP];
        foutRM[FPMP] =  0.25*rho * (-RMcoll[M111] - RMcoll[M211]                - RMcoll[M112]                - RMcoll[M212]               )+ foutRM[FPPP];
        foutRM[FPPM] =  0.25*rho * (-RMcoll[M111] - RMcoll[M211] - RMcoll[M121]                - RMcoll[M221]                              )+ foutRM[FPPP];
        foutRM[FMMP] =  0.25*rho * (              - RMcoll[M211] - RMcoll[M121]                               - RMcoll[M212] - RMcoll[M122])+ foutRM[FPPP];
        foutRM[FMPM] =  0.25*rho * (              - RMcoll[M211]                - RMcoll[M112] - RMcoll[M221]                - RMcoll[M122])+ foutRM[FPPP];
        foutRM[FPMM] =  0.25*rho * (                             - RMcoll[M121] - RMcoll[M112] - RMcoll[M221] - RMcoll[M212]               )+ foutRM[FPPP];
        foutRM[FMMM] =  0.25*rho * (-RMcoll[M111]                                              - RMcoll[M221] - RMcoll[M212] - RMcoll[M122])+ foutRM[FPPP];

        f(i,F000) = foutRM[F000];

        f(i,FP00) = foutRM[FM00];
        f(i,FM00) = foutRM[FP00];

        f(i,F0P0) = foutRM[F0M0];
        f(i,F0M0) = foutRM[F0P0];

        f(i,F00P) = foutRM[F00M];
        f(i,F00M) = foutRM[F00P];

        f(i,FPP0) = foutRM[FMM0];
        f(i,FMP0) = foutRM[FPM0];
        f(i,FPM0) = foutRM[FMP0];
        f(i,FMM0) = foutRM[FPP0];

        f(i,FP0P) = foutRM[FM0M];
        f(i,FM0P) = foutRM[FP0M];
        f(i,FP0M) = foutRM[FM0P];
        f(i,FM0M) = foutRM[FP0P];

        f(i,F0PP) = foutRM[F0MM];
        f(i,F0MP) = foutRM[F0PM];
        f(i,F0PM) = foutRM[F0MP];
        f(i,F0MM) = foutRM[F0PP];

        f(i,FPPP) = foutRM[FMMM];
        f(i,FMPP) = foutRM[FPMM];
        f(i,FPMP) = foutRM[FMPM];
        f(i,FPPM) = foutRM[FMMP];
        f(i,FMMP) = foutRM[FPPM];
        f(i,FMPM) = foutRM[FPMP];
        f(i,FPMM) = foutRM[FMPP];
        f(i,FMMM) = foutRM[FPPP];
    }


    void iterateHM(double& f0) {
        int i = &f0 - lattice;
        if (flag[i] == CellType::bulk) {
            std::array<double, 27> pop;
            for (int k = 0; k < 27; ++k) {
                pop[k] = f(i, k);
            }
/*          auto HM = computeHM(pop);
            auto HM = computeHMopt(pop);*/
            auto HM = computeHMopt2(pop);
            double rho = HM[M000];
            std::array<double, 3> u = {HM[M100], HM[M010], HM[M001]};
            auto HMeq = computeHMeq(u);
            collideHM(i, rho, u, HM, HMeq);
        }
    }

    void operator() (double& f0) {
        iterateHM(f0);
    }
};

struct Odd : public LBM {

    auto collideHM(std::array<double, 27>& pop, double rho, std::array<double, 3> const& u, std::array<double, 27> const& HM, std::array<double, 27> const& HMeq)
    {
        // Post-collision  moments.
        std::array<double, 27> HMcoll;
        std::array<double, 27> RMcoll;

        // Collision in the Hermite moment space
        // Order 2
        if (omegaBulk == 0.) { // Don't adjust bulk viscosity 
            HMcoll[M200] = (1. - omega1) * HM[M200] + omega1 * HMeq[M200];
            HMcoll[M020] = (1. - omega1) * HM[M020] + omega1 * HMeq[M020];
            HMcoll[M002] = (1. - omega1) * HM[M002] + omega1 * HMeq[M002];
        }
        else { // Adjust bulk viscosity
            const double omegaMinus = (omegaBulk - omega1)/3.; // Notation used by Fei
            const double omegaPlus  = omegaMinus + omega1;     // Notation used by Fei
            HMcoll[M200] = HM[M200] - omegaPlus  * (HM[M200]-HMeq[M200]) - omegaMinus * (HM[M020]-HMeq[M020]) - omegaMinus * (HM[M002]-HMeq[M002]) ;
            HMcoll[M020] = HM[M020] - omegaMinus * (HM[M200]-HMeq[M200]) - omegaPlus  * (HM[M020]-HMeq[M020]) - omegaMinus * (HM[M002]-HMeq[M002]) ;
            HMcoll[M002] = HM[M002] - omegaMinus * (HM[M200]-HMeq[M200]) - omegaMinus * (HM[M020]-HMeq[M020]) - omegaPlus  * (HM[M002]-HMeq[M002]) ;
        }

        HMcoll[M110] = (1. - omega2) * HM[M110] + omega2 * HMeq[M110] ;
        HMcoll[M101] = (1. - omega2) * HM[M101] + omega2 * HMeq[M101] ;
        HMcoll[M011] = (1. - omega2) * HM[M011] + omega2 * HMeq[M011] ;
        // Order 3
        HMcoll[M210] = (1. - omega3) * HM[M210] + omega3 * HMeq[M210] ;
        HMcoll[M201] = (1. - omega3) * HM[M201] + omega3 * HMeq[M201] ;
        HMcoll[M021] = (1. - omega3) * HM[M021] + omega3 * HMeq[M021] ;
        HMcoll[M120] = (1. - omega3) * HM[M120] + omega3 * HMeq[M120] ;
        HMcoll[M102] = (1. - omega3) * HM[M102] + omega3 * HMeq[M102] ;
        HMcoll[M012] = (1. - omega3) * HM[M012] + omega3 * HMeq[M012] ;
        HMcoll[M111] = (1. - omega4) * HM[M111] + omega4 * HMeq[M111] ;
        // Order 4
        HMcoll[M220] = (1. - omega5) * HM[M220] + omega5 * HMeq[M220] ;
        HMcoll[M202] = (1. - omega5) * HM[M202] + omega5 * HMeq[M202] ;
        HMcoll[M022] = (1. - omega5) * HM[M022] + omega5 * HMeq[M022] ;
        HMcoll[M211] = (1. - omega6) * HM[M211] + omega6 * HMeq[M211] ;
        HMcoll[M121] = (1. - omega6) * HM[M121] + omega6 * HMeq[M121] ;
        HMcoll[M112] = (1. - omega6) * HM[M112] + omega6 * HMeq[M112] ;
        // Order 5
        HMcoll[M221] = (1. - omega7) * HM[M221] + omega7 * HMeq[M221] ;
        HMcoll[M212] = (1. - omega7) * HM[M212] + omega7 * HMeq[M212] ;
        HMcoll[M122] = (1. - omega7) * HM[M122] + omega7 * HMeq[M122] ;
        // Order 6
        HMcoll[M222] = (1. - omega8) * HM[M222] + omega8 * HMeq[M222] ;

        // Come back to RMcoll using relationships between HMs and RMs
        double cs2 = 1./3.;
        double cs4 = cs2*cs2;

        RMcoll[M200] = HMcoll[M200] + cs2;
        RMcoll[M020] = HMcoll[M020] + cs2;
        RMcoll[M002] = HMcoll[M002] + cs2;
        
        RMcoll[M110] = HMcoll[M110];
        RMcoll[M101] = HMcoll[M101];
        RMcoll[M011] = HMcoll[M011];

        RMcoll[M210] = HMcoll[M210] + cs2*u[1];
        RMcoll[M201] = HMcoll[M201] + cs2*u[2];
        RMcoll[M021] = HMcoll[M021] + cs2*u[2];
        RMcoll[M120] = HMcoll[M120] + cs2*u[0];
        RMcoll[M102] = HMcoll[M102] + cs2*u[0];
        RMcoll[M012] = HMcoll[M012] + cs2*u[1];
        
        RMcoll[M111] = HMcoll[M111];

        RMcoll[M220] = HMcoll[M220] + cs2*(HMcoll[M200] + HMcoll[M020]) + cs4;
        RMcoll[M202] = HMcoll[M202] + cs2*(HMcoll[M200] + HMcoll[M002]) + cs4;
        RMcoll[M022] = HMcoll[M022] + cs2*(HMcoll[M020] + HMcoll[M002]) + cs4;

        RMcoll[M211] = HMcoll[M211] + cs2*HMcoll[M011];
        RMcoll[M121] = HMcoll[M121] + cs2*HMcoll[M101];
        RMcoll[M112] = HMcoll[M112] + cs2*HMcoll[M110];

        RMcoll[M221] = HMcoll[M221] + cs2*(HMcoll[M201] + HMcoll[M021]) + cs4*u[2];
        RMcoll[M212] = HMcoll[M212] + cs2*(HMcoll[M210] + HMcoll[M012]) + cs4*u[1];
        RMcoll[M122] = HMcoll[M122] + cs2*(HMcoll[M120] + HMcoll[M102]) + cs4*u[0];

        RMcoll[M222] = HMcoll[M222] + cs2*(HMcoll[M220] + HMcoll[M202] + HMcoll[M022]) + cs4*(HMcoll[M200] + HMcoll[M020] + HMcoll[M002]) + cs2*cs4;

        // Optimization based on symmetries between populations and their opposite counterpart
        pop[F000] = rho *(1. - RMcoll[M200] - RMcoll[M020] - RMcoll[M002] + RMcoll[M220] + RMcoll[M202] + RMcoll[M022] - RMcoll[M222]);
        
        pop[FP00] = 0.5*rho * ( u[0] + RMcoll[M200] - RMcoll[M120] - RMcoll[M102] - RMcoll[M220] - RMcoll[M202] + RMcoll[M122] + RMcoll[M222]);
        pop[FM00] =     rho * (-u[0]                + RMcoll[M120] + RMcoll[M102]                               - RMcoll[M122])+ pop[FP00];

        pop[F0P0] = 0.5*rho * ( u[1] + RMcoll[M020] - RMcoll[M210] - RMcoll[M012] - RMcoll[M220] - RMcoll[M022] + RMcoll[M212] + RMcoll[M222]);
        pop[F0M0] =     rho * (-u[1]                + RMcoll[M210] + RMcoll[M012]                               - RMcoll[M212])+ pop[F0P0];

        pop[F00P] = 0.5*rho * ( u[2] + RMcoll[M002] - RMcoll[M201] - RMcoll[M021] - RMcoll[M202] - RMcoll[M022] + RMcoll[M221] + RMcoll[M222]);
        pop[F00M] =     rho * (-u[2]                + RMcoll[M201] + RMcoll[M021]                               - RMcoll[M221])+ pop[F00P];

        pop[FPP0] = 0.25*rho * ( RMcoll[M110] + RMcoll[M210] + RMcoll[M120] - RMcoll[M112] + RMcoll[M220] - RMcoll[M212] - RMcoll[M122] - RMcoll[M222]);
        pop[FMP0] =  0.5*rho * (-RMcoll[M110]                - RMcoll[M120] + RMcoll[M112]                               + RMcoll[M122])+ pop[FPP0];
        pop[FPM0] =  0.5*rho * (-RMcoll[M110] - RMcoll[M210]                + RMcoll[M112]                + RMcoll[M212]               )+ pop[FPP0];
        pop[FMM0] =  0.5*rho * (              - RMcoll[M210] - RMcoll[M120]                               + RMcoll[M212] + RMcoll[M122])+ pop[FPP0];

        pop[FP0P] = 0.25*rho * ( RMcoll[M101] + RMcoll[M201] + RMcoll[M102] - RMcoll[M121] + RMcoll[M202] - RMcoll[M221] - RMcoll[M122] - RMcoll[M222]);
        pop[FM0P] =  0.5*rho * (-RMcoll[M101]                - RMcoll[M102] + RMcoll[M121]                               + RMcoll[M122])+ pop[FP0P];
        pop[FP0M] =  0.5*rho * (-RMcoll[M101] - RMcoll[M201]                + RMcoll[M121]                + RMcoll[M221]               )+ pop[FP0P];
        pop[FM0M] =  0.5*rho * (              - RMcoll[M201] - RMcoll[M102]                               + RMcoll[M221] + RMcoll[M122])+ pop[FP0P];

        pop[F0PP] = 0.25*rho * ( RMcoll[M011] + RMcoll[M021] + RMcoll[M012] - RMcoll[M211] + RMcoll[M022] - RMcoll[M221] - RMcoll[M212] - RMcoll[M222]);
        pop[F0MP] =  0.5*rho * (-RMcoll[M011]                - RMcoll[M012] + RMcoll[M211]                               + RMcoll[M212])+ pop[F0PP];
        pop[F0PM] =  0.5*rho * (-RMcoll[M011] - RMcoll[M021]                + RMcoll[M211]                + RMcoll[M221]               )+ pop[F0PP];
        pop[F0MM] =  0.5*rho * (              - RMcoll[M021] - RMcoll[M012]                               + RMcoll[M221] + RMcoll[M212])+ pop[F0PP];

        pop[FPPP] = 0.125*rho * ( RMcoll[M111] + RMcoll[M211] + RMcoll[M121] + RMcoll[M112] + RMcoll[M221] + RMcoll[M212] + RMcoll[M122] + RMcoll[M222]);
        pop[FMPP] =  0.25*rho * (-RMcoll[M111]                - RMcoll[M121] - RMcoll[M112]                               - RMcoll[M122])+ pop[FPPP];
        pop[FPMP] =  0.25*rho * (-RMcoll[M111] - RMcoll[M211]                - RMcoll[M112]                - RMcoll[M212]               )+ pop[FPPP];
        pop[FPPM] =  0.25*rho * (-RMcoll[M111] - RMcoll[M211] - RMcoll[M121]                - RMcoll[M221]                              )+ pop[FPPP];
        pop[FMMP] =  0.25*rho * (              - RMcoll[M211] - RMcoll[M121]                               - RMcoll[M212] - RMcoll[M122])+ pop[FPPP];
        pop[FMPM] =  0.25*rho * (              - RMcoll[M211]                - RMcoll[M112] - RMcoll[M221]                - RMcoll[M122])+ pop[FPPP];
        pop[FPMM] =  0.25*rho * (                             - RMcoll[M121] - RMcoll[M112] - RMcoll[M221] - RMcoll[M212]               )+ pop[FPPP];
        pop[FMMM] =  0.25*rho * (-RMcoll[M111]                                              - RMcoll[M221] - RMcoll[M212] - RMcoll[M122])+ pop[FPPP];
    }

    auto streamingPull(int i, int iX, int iY, int iZ, std::array<double, 27>& pop)
    {
        if(periodic){
            for (int k = 0; k < 27; ++k) {
                int XX = (iX - c[k][0] + dim.nx) % dim.nx;
                int YY = (iY - c[k][1] + dim.ny) % dim.ny;
                int ZZ = (iZ - c[k][2] + dim.nz) % dim.nz;
                size_t nb = xyz_to_i(XX, YY, ZZ);
                CellType nbCellType = flag[nb];
                if (nbCellType == CellType::bounce_back) {
                    pop[k] = f(i, k) + f(nb, opp[k]);
                }
                else {
                    pop[k] = f(nb, opp[k]);
                }
            }
        }
        else {
            for (int k = 0; k < 27; ++k) {
                int XX = iX - c[k][0];
                int YY = iY - c[k][1];
                int ZZ = iZ - c[k][2];
                size_t nb = xyz_to_i(XX, YY, ZZ);
                CellType nbCellType = flag[nb];
                if (nbCellType == CellType::bounce_back) {
                    pop[k] = f(i, k) + f(nb, opp[k]);
                }
                else {
                    pop[k] = f(nb, opp[k]);
                }
            }
        }
    }

    auto streamingPush(int i, int iX, int iY, int iZ, std::array<double, 27>& pop)
    {
        if(periodic){
            for (int k = 0; k < 27; ++k) {
                int XX = (iX + c[k][0] + dim.nx) % dim.nx;
                int YY = (iY + c[k][1] + dim.ny) % dim.ny;
                int ZZ = (iZ + c[k][2] + dim.nz) % dim.nz;
                size_t nb = xyz_to_i(XX, YY, ZZ);
                CellType nbCellType = flag[nb];
                if (nbCellType == CellType::bounce_back) {
                    f(i, opp[k]) = pop[k] + f(nb, k);
                }
                else {
                    f(nb, k) = pop[k];
                }
            }
        }
        else {
            for (int k = 0; k < 27; ++k) {
                int XX = iX + c[k][0];
                int YY = iY + c[k][1];
                int ZZ = iZ + c[k][2];
                size_t nb = xyz_to_i(XX, YY, ZZ);
                CellType nbCellType = flag[nb];
                if (nbCellType == CellType::bounce_back) {
                    f(i, opp[k]) = pop[k] + f(nb, k);
                }
                else {
                    f(nb, k) = pop[k];
                }
            }
        }
    }

    void iterateHM(double& f0) {
        int i = &f0 - lattice;
        auto[iX, iY, iZ] = i_to_xyz(i);
        CellType cellType = flag[i];

        if (cellType == CellType::bulk) {
            std::array<double, 27> pop;

            streamingPull(i, iX, iY, iZ, pop);

/*          auto HM = computeHM(pop);
            auto HM = computeHMopt(pop);*/
            auto HM = computeHMopt2(pop);
            double rho = HM[M000];
            std::array<double, 3> u = {HM[M100], HM[M010], HM[M001]};
            auto HMeq = computeHMeq(u);

            collideHM(pop, rho, u, HM, HMeq);

            streamingPush(i, iX, iY, iZ, pop);
        }
    }

    void operator() (double& f0) {
        iterateHM(f0);
    }
};

} // namespace aa_soa_hm
