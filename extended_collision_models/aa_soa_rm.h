// *****************************************************************************
// STLBM SOFTWARE LIBRARY

// Copyright © 2020 University of Geneva
// Authors: Jonas Latt, Christophe Coreixas, Joël Beny
// Contact: Jonas.Latt@unige.ch

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// *****************************************************************************

#pragma once

#include "lbm.h"
#include <vector>
#include <array>
#include <tuple>

namespace aa_soa_rm {

struct LBM {
    using CellData = double;
    static size_t sizeOfLattice(size_t nelem) { return 19 * nelem; }

    CellData* lattice;
    CellType* flag;
    std::array<int, 3>* c;
    int* opp;
    double* t;
    double omega1;
    Dim dim;
    LBModel model;
    bool periodic = false;
    double omegaBulk = 0.;
    double omega2 = omega1;
    double omega3 = omega1;
    double omega4 = omega1;

    auto i_to_xyz (int i) const {
        int iX = i / (dim.ny * dim.nz);
        int remainder = i % (dim.ny * dim.nz);
        int iY = remainder / dim.nz;
        int iZ = remainder % dim.nz;
        return std::make_tuple(iX, iY, iZ);
    };

    size_t xyz_to_i (int x, int y, int z) const {
        return z + dim.nz * (y + dim.ny * x);
    };

    double& f (int i, int k) {
        return lattice[k * dim.nelem + i];
    }

    auto iniLattice (double& f0) {
        auto i = &f0 - lattice;
        for (int k = 0; k < 19; ++k) {
            f(i, k) = t[k];
        }
    };

    auto iniLattice (double& f0, double rho, std::array<double, 3> const& u) {
        auto i = &f0 - lattice;
        double usqr = 1.5 * (u[0] * u[0] + u[1] * u[1] + u[2] * u[2]);
        for (int k = 0; k < 19; ++k) {
            double ck_u = c[k][0] * u[0] + c[k][1] * u[1] + c[k][2] * u[2];
            f(i, k) = rho * t[k] * (1. + 3. * ck_u + 4.5 * ck_u * ck_u - usqr);
        }
    };

    auto macro (double const& f0) {
        auto i = &f0 - lattice;
        double X_M1 = f(i, 0) + f(i, 3) + f(i, 4) + f(i, 5) + f(i, 6);
        double X_P1 = f(i,10) + f(i,13) + f(i,14) + f(i,15) + f(i,16);
        double X_0  = f(i, 9) + f(i, 1) + f(i, 2) + f(i, 7) + f(i, 8) + f(i,11) + f(i,12) + f(i,17) + f(i,18);

        double Y_M1 = f(i, 1) + f(i, 3) + f(i, 7) + f(i, 8) + f(i,14);
        double Y_P1 = f(i, 4) + f(i,11) + f(i,13) + f(i,17) + f(i,18);

        double Z_M1 = f(i, 2) + f(i, 5) + f(i, 7) + f(i,16) + f(i,18);
        double Z_P1 = f(i, 6) + f(i, 8) + f(i,12) + f(i,15) + f(i,17);

        double rho = X_M1 + X_P1 + X_0;
        std::array<double, 3> u{ (X_P1 - X_M1) / rho, (Y_P1 - Y_M1) / rho, (Z_P1 - Z_M1) / rho };
        return make_pair(rho, u);
    }

    auto macropop (std::array<double, 19> const& pop) {
        double X_M1 = pop[ 0] + pop[ 3] + pop[ 4] + pop[ 5] + pop[ 6];
        double X_P1 = pop[10] + pop[13] + pop[14] + pop[15] + pop[16];
        double X_0  = pop[ 9] + pop[ 1] + pop[ 2] + pop[ 7] + pop[ 8] + pop[11] + pop[12] + pop[17] + pop[18];

        double Y_M1 = pop[ 1] + pop[ 3] + pop[ 7] + pop[ 8] + pop[14];
        double Y_P1 = pop[ 4] + pop[11] + pop[13] + pop[17] + pop[18];

        double Z_M1 = pop[ 2] + pop[ 5] + pop[ 7] + pop[16] + pop[18];
        double Z_P1 = pop[ 6] + pop[ 8] + pop[12] + pop[15] + pop[17];

        double rho = X_M1 + X_P1 + X_0;
        std::array<double, 3> u{ (X_P1 - X_M1) / rho, (Y_P1 - Y_M1) / rho, (Z_P1 - Z_M1) / rho };
        return make_pair(rho, u);
    };

    auto computeRMopt(std::array<double, 19> const& pop, double const& rho) {

        std::array<double, 19> RM;
        double invRho = 1./rho;
         // Order 4
        RM[M220] = invRho * (pop[ 3] + pop[ 4] + pop[13] + pop[14]);
        RM[M202] = invRho * (pop[ 5] + pop[ 6] + pop[15] + pop[16]);
        RM[M022] = invRho * (pop[ 7] + pop[ 8] + pop[17] + pop[18]);
         // Order 2
        RM[M200] = invRho * (pop[ 0] + pop[10]) + RM[M220] + RM[M202];
        RM[M020] = invRho * (pop[ 1] + pop[11]) + RM[M220] + RM[M022];
        RM[M002] = invRho * (pop[ 2] + pop[12]) + RM[M202] + RM[M022];
        RM[M110] = RM[M220] - 2.*invRho * (pop[ 4] + pop[14]);
        RM[M101] = RM[M202] - 2.*invRho * (pop[ 6] + pop[16]);
        RM[M011] = RM[M022] - 2.*invRho * (pop[ 8] + pop[18]);
         // Order 3
        RM[M210] = RM[M220] - 2.*invRho * (pop[ 3] + pop[14]);
        RM[M201] = RM[M202] - 2.*invRho * (pop[ 5] + pop[16]);
        RM[M021] = RM[M022] - 2.*invRho * (pop[ 7] + pop[18]);
        RM[M120] = RM[M220] - 2.*invRho * (pop[ 3] + pop[ 4]);
        RM[M102] = RM[M202] - 2.*invRho * (pop[ 5] + pop[ 6]);
        RM[M012] = RM[M022] - 2.*invRho * (pop[ 7] + pop[ 8]);

        return RM;
    }

    // Further optimization through merge with the computation of macros
    auto computeRMopt2(std::array<double, 19> const& pop) {
        std::array<double, 19> RM;
        std::fill(RM.begin(), RM.end(), 0.);

        double X_M1 = pop[ 0] + pop[ 3] + pop[ 4] + pop[ 5] + pop[ 6];
        double X_P1 = pop[10] + pop[13] + pop[14] + pop[15] + pop[16];
        double X_0  = pop[ 9] + pop[ 1] + pop[ 2] + pop[ 7] + pop[ 8] + pop[11] + pop[12] + pop[17] + pop[18];

        double Y_M1 = pop[ 1] + pop[ 3] + pop[ 7] + pop[ 8] + pop[14];
        double Y_P1 = pop[ 4] + pop[11] + pop[13] + pop[17] + pop[18];

        double Z_M1 = pop[ 2] + pop[ 5] + pop[ 7] + pop[16] + pop[18];
        double Z_P1 = pop[ 6] + pop[ 8] + pop[12] + pop[15] + pop[17];

        // Order 0
        RM[M000] = X_M1 + X_P1 + X_0;
        double invRho = 1. / RM[M000];
        double two_invRho= 2.*invRho;
        // Order 1
        RM[M100] = invRho * (X_P1 - X_M1);
        RM[M010] = invRho * (Y_P1 - Y_M1); 
        RM[M001] = invRho * (Z_P1 - Z_M1);
        // Order 2
        RM[M200] = invRho * (X_M1 + X_P1);
        RM[M020] = invRho * (Y_M1 + Y_P1);
        RM[M002] = invRho * (Z_M1 + Z_P1);
        RM[M110] = invRho * ( pop[ 3] - pop[ 4] + pop[13] - pop[14]);
        RM[M101] = invRho * ( pop[ 5] - pop[ 6] + pop[15] - pop[16]);
        RM[M011] = invRho * ( pop[ 7] - pop[ 8] + pop[17] - pop[18]);
        // Order 3
        RM[M210] = RM[M110] - two_invRho * (pop[ 3] - pop[ 4]);
        RM[M201] = RM[M101] - two_invRho * (pop[ 5] - pop[ 6]);
        RM[M021] = RM[M011] - two_invRho * (pop[ 7] - pop[ 8]);
        RM[M120] = RM[M110] - two_invRho * (pop[ 3] - pop[14]);
        RM[M102] = RM[M101] - two_invRho * (pop[ 5] - pop[16]);
        RM[M012] = RM[M011] - two_invRho * (pop[ 7] - pop[18]);
        // Order 4
        RM[M220] = RM[M110] + two_invRho * (pop[ 4] + pop[14]);
        RM[M202] = RM[M101] + two_invRho * (pop[ 6] + pop[16]);
        RM[M022] = RM[M011] + two_invRho * (pop[ 8] + pop[18]);

        return RM;
    }

    auto computeRMeq(std::array<double, 3> const& u) {

        std::array<double, 19> RMeq;
        double cs2 = 1./3.;
        // Order 2
        RMeq[M200] = u[0] * u[0] + cs2;
        RMeq[M020] = u[1] * u[1] + cs2;
        RMeq[M002] = u[2] * u[2] + cs2;
        RMeq[M110] = u[0] * u[1];
        RMeq[M101] = u[0] * u[2];
        RMeq[M011] = u[1] * u[2];
        // Order 3
        RMeq[M210] = RMeq[M200] * u[1];
        RMeq[M201] = RMeq[M200] * u[2];
        RMeq[M021] = RMeq[M020] * u[2];
        RMeq[M120] = RMeq[M020] * u[0];
        RMeq[M102] = RMeq[M002] * u[0];
        RMeq[M012] = RMeq[M002] * u[1];
        // Order 4
        RMeq[M220] = RMeq[M200] * RMeq[M020];
        RMeq[M202] = RMeq[M200] * RMeq[M002];
        RMeq[M022] = RMeq[M020] * RMeq[M002];

        return RMeq;
    }
};

struct Even : public LBM {

    auto collideRM(int i, double rho, std::array<double, 3> const& u, std::array<double, 19> const& RM, std::array<double, 19> const& RMeq)
    {
        // Post-collision moments.
        std::array<double, 19> RMcoll;

        // Order 2
        if (omegaBulk == 0.) { // Don't adjust bulk viscosity 
            RMcoll[M200] = (1. - omega1) * RM[M200] + omega1 * RMeq[M200];
            RMcoll[M020] = (1. - omega1) * RM[M020] + omega1 * RMeq[M020];
            RMcoll[M002] = (1. - omega1) * RM[M002] + omega1 * RMeq[M002];
        }
        else { // Adjust bulk viscosity
            const double omegaMinus = (omegaBulk - omega1)/3.; // Notation used by Fei
            const double omegaPlus  = omegaMinus + omega1;     // Notation used by Fei
            RMcoll[M200] = RM[M200] - omegaPlus  * (RM[M200]-RMeq[M200]) - omegaMinus * (RM[M020]-RMeq[M020]) - omegaMinus * (RM[M002]-RMeq[M002]) ;
            RMcoll[M020] = RM[M020] - omegaMinus * (RM[M200]-RMeq[M200]) - omegaPlus  * (RM[M020]-RMeq[M020]) - omegaMinus * (RM[M002]-RMeq[M002]) ;
            RMcoll[M002] = RM[M002] - omegaMinus * (RM[M200]-RMeq[M200]) - omegaMinus * (RM[M020]-RMeq[M020]) - omegaPlus  * (RM[M002]-RMeq[M002]) ;
        }
        
        RMcoll[M110] = (1. - omega2) * RM[M110] + omega2 * RMeq[M110];
        RMcoll[M101] = (1. - omega2) * RM[M101] + omega2 * RMeq[M101];
        RMcoll[M011] = (1. - omega2) * RM[M011] + omega2 * RMeq[M011];

        // Order 3
        RMcoll[M210] = (1. - omega3) * RM[M210] + omega3 * RMeq[M210];
        RMcoll[M201] = (1. - omega3) * RM[M201] + omega3 * RMeq[M201];
        RMcoll[M021] = (1. - omega3) * RM[M021] + omega3 * RMeq[M021];
        RMcoll[M120] = (1. - omega3) * RM[M120] + omega3 * RMeq[M120];
        RMcoll[M102] = (1. - omega3) * RM[M102] + omega3 * RMeq[M102];
        RMcoll[M012] = (1. - omega3) * RM[M012] + omega3 * RMeq[M012];
        
        // Order 4
        RMcoll[M220] = (1. - omega4) * RM[M220] + omega4 * RMeq[M220];
        RMcoll[M202] = (1. - omega4) * RM[M202] + omega4 * RMeq[M202];
        RMcoll[M022] = (1. - omega4) * RM[M022] + omega4 * RMeq[M022];

        // Optimization based on symmetries between populations and their opposite counterpart
        double pop_out_09 = rho *(1. - RMcoll[M200] - RMcoll[M020] - RMcoll[M002] + RMcoll[M220] + RMcoll[M202] + RMcoll[M022]);
        
        double pop_out_10 = 0.5*rho * ( u[0] + RMcoll[M200] - RMcoll[M120] - RMcoll[M102] - RMcoll[M220] - RMcoll[M202]);
        double pop_out_00 =     rho * (-u[0]                + RMcoll[M120] + RMcoll[M102]) + pop_out_10;

        double pop_out_11 = 0.5*rho * ( u[1] + RMcoll[M020] - RMcoll[M210] - RMcoll[M012] - RMcoll[M220] - RMcoll[M022]);
        double pop_out_01 =     rho * (-u[1]                + RMcoll[M210] + RMcoll[M012]) + pop_out_11;

        double pop_out_12 = 0.5*rho * ( u[2] + RMcoll[M002] - RMcoll[M201] - RMcoll[M021] - RMcoll[M202] - RMcoll[M022]);
        double pop_out_02 =     rho * (-u[2]                + RMcoll[M201] + RMcoll[M021]) + pop_out_12;

        double pop_out_13 = 0.25*rho * ( RMcoll[M110] + RMcoll[M210] + RMcoll[M120] + RMcoll[M220]);
        double pop_out_04 =  0.5*rho * (-RMcoll[M110]              - RMcoll[M120]) + pop_out_13;
        double pop_out_14 =  0.5*rho * (-RMcoll[M110] - RMcoll[M210])              + pop_out_13;
        double pop_out_03 =  0.5*rho * (              - RMcoll[M210] - RMcoll[M120]) + pop_out_13;

        double pop_out_15 = 0.25*rho * ( RMcoll[M101] + RMcoll[M201] + RMcoll[M102] + RMcoll[M202]);
        double pop_out_06 =  0.5*rho * (-RMcoll[M101]              - RMcoll[M102]) + pop_out_15;
        double pop_out_16 =  0.5*rho * (-RMcoll[M101] - RMcoll[M201])              + pop_out_15;
        double pop_out_05 =  0.5*rho * (              - RMcoll[M201] - RMcoll[M102]) + pop_out_15;

        double pop_out_17 = 0.25*rho * ( RMcoll[M011] + RMcoll[M021] + RMcoll[M012] + RMcoll[M022]);
        double pop_out_08 =  0.5*rho * (-RMcoll[M011]              - RMcoll[M012]) + pop_out_17;
        double pop_out_18 =  0.5*rho * (-RMcoll[M011] - RMcoll[M021])              + pop_out_17;
        double pop_out_07 =  0.5*rho * (              - RMcoll[M021] - RMcoll[M012]) + pop_out_17;

        f(i,  0) = pop_out_10;
        f(i, 10) = pop_out_00;

        f(i,  1) = pop_out_11;
        f(i, 11) = pop_out_01;

        f(i,  2) = pop_out_12;
        f(i, 12) = pop_out_02;

        f(i,  3) = pop_out_13;
        f(i, 13) = pop_out_03;

        f(i,  4) = pop_out_14;
        f(i, 14) = pop_out_04;

        f(i,  5) = pop_out_15;
        f(i, 15) = pop_out_05;

        f(i,  6) = pop_out_16;
        f(i, 16) = pop_out_06;

        f(i,  7) = pop_out_17;
        f(i, 17) = pop_out_07;

        f(i,  8) = pop_out_18;
        f(i, 18) = pop_out_08;

        f(i, 9) = pop_out_09;
    }


    void iterateRM(double& f0) {
        int i = &f0 - lattice;
        if (flag[i] == CellType::bulk) {
            std::array<double, 19> pop;
            for (int k = 0; k < 19; ++k) {
                pop[k] = f(i, k);
            }
/*            auto[rho, u] = macropop(pop);
            auto RM = computeRMopt(pop, rho);*/
            auto RM = computeRMopt2(pop);
            double rho = RM[M000];
            std::array<double, 3> u = {RM[M100], RM[M010], RM[M001]};
            auto RMeq = computeRMeq(u);
            collideRM(i, rho, u, RM, RMeq);
        }
    }

    void operator() (double& f0) {
        iterateRM(f0);
    }
};

struct Odd : public LBM {

    auto collideRM(std::array<double, 19>& pop, double rho, std::array<double, 3> const& u, std::array<double, 19> const& RM, std::array<double, 19> const& RMeq)
    {
        // Post-collision moments.
        std::array<double, 19> RMcoll;

        // Order 2
        if (omegaBulk == 0.) { // Don't adjust bulk viscosity
            RMcoll[M200] = (1. - omega1) * RM[M200] + omega1 * RMeq[M200];
            RMcoll[M020] = (1. - omega1) * RM[M020] + omega1 * RMeq[M020];
            RMcoll[M002] = (1. - omega1) * RM[M002] + omega1 * RMeq[M002];
        }
        else { // Adjust bulk viscosity
            const double omegaBulk = omega1;
            const double omegaMinus = (omegaBulk - omega1)/3.; // Notation used by Fei
            const double omegaPlus  = omegaMinus + omega1;     // Notation used by Fei
            RMcoll[M200] = RM[M200] - omegaPlus  * (RM[M200]-RMeq[M200]) - omegaMinus * (RM[M020]-RMeq[M020]) - omegaMinus * (RM[M002]-RMeq[M002]) ;
            RMcoll[M020] = RM[M020] - omegaMinus * (RM[M200]-RMeq[M200]) - omegaPlus  * (RM[M020]-RMeq[M020]) - omegaMinus * (RM[M002]-RMeq[M002]) ;
            RMcoll[M002] = RM[M002] - omegaMinus * (RM[M200]-RMeq[M200]) - omegaMinus * (RM[M020]-RMeq[M020]) - omegaPlus  * (RM[M002]-RMeq[M002]) ;
        }
        
        RMcoll[M110] = (1. - omega2) * RM[M110] + omega2 * RMeq[M110];
        RMcoll[M101] = (1. - omega2) * RM[M101] + omega2 * RMeq[M101];
        RMcoll[M011] = (1. - omega2) * RM[M011] + omega2 * RMeq[M011];

        // Order 3
        RMcoll[M210] = (1. - omega3) * RM[M210] + omega3 * RMeq[M210];
        RMcoll[M201] = (1. - omega3) * RM[M201] + omega3 * RMeq[M201];
        RMcoll[M021] = (1. - omega3) * RM[M021] + omega3 * RMeq[M021];
        RMcoll[M120] = (1. - omega3) * RM[M120] + omega3 * RMeq[M120];
        RMcoll[M102] = (1. - omega3) * RM[M102] + omega3 * RMeq[M102];
        RMcoll[M012] = (1. - omega3) * RM[M012] + omega3 * RMeq[M012];
        
        // Order 4
        RMcoll[M220] = (1. - omega4) * RM[M220] + omega4 * RMeq[M220];
        RMcoll[M202] = (1. - omega4) * RM[M202] + omega4 * RMeq[M202];
        RMcoll[M022] = (1. - omega4) * RM[M022] + omega4 * RMeq[M022];

        // Optimization based on symmetries between populations and their opposite counterpart
        double pop_out_09 = rho *(1. - RMcoll[M200] - RMcoll[M020] - RMcoll[M002] + RMcoll[M220] + RMcoll[M202] + RMcoll[M022]);
        
        double pop_out_10 = 0.5*rho * ( u[0] + RMcoll[M200] - RMcoll[M120] - RMcoll[M102] - RMcoll[M220] - RMcoll[M202]);
        double pop_out_00 =     rho * (-u[0]                + RMcoll[M120] + RMcoll[M102]) + pop_out_10;

        double pop_out_11 = 0.5*rho * ( u[1] + RMcoll[M020] - RMcoll[M210] - RMcoll[M012] - RMcoll[M220] - RMcoll[M022]);
        double pop_out_01 =     rho * (-u[1]                + RMcoll[M210] + RMcoll[M012]) + pop_out_11;

        double pop_out_12 = 0.5*rho * ( u[2] + RMcoll[M002] - RMcoll[M201] - RMcoll[M021] - RMcoll[M202] - RMcoll[M022]);
        double pop_out_02 =     rho * (-u[2]                + RMcoll[M201] + RMcoll[M021]) + pop_out_12;

        double pop_out_13 = 0.25*rho * ( RMcoll[M110] + RMcoll[M210] + RMcoll[M120] + RMcoll[M220]);
        double pop_out_04 =  0.5*rho * (-RMcoll[M110]              - RMcoll[M120]) + pop_out_13;
        double pop_out_14 =  0.5*rho * (-RMcoll[M110] - RMcoll[M210])              + pop_out_13;
        double pop_out_03 =  0.5*rho * (              - RMcoll[M210] - RMcoll[M120]) + pop_out_13;

        double pop_out_15 = 0.25*rho * ( RMcoll[M101] + RMcoll[M201] + RMcoll[M102] + RMcoll[M202]);
        double pop_out_06 =  0.5*rho * (-RMcoll[M101]              - RMcoll[M102]) + pop_out_15;
        double pop_out_16 =  0.5*rho * (-RMcoll[M101] - RMcoll[M201])              + pop_out_15;
        double pop_out_05 =  0.5*rho * (              - RMcoll[M201] - RMcoll[M102]) + pop_out_15;

        double pop_out_17 = 0.25*rho * ( RMcoll[M011] + RMcoll[M021] + RMcoll[M012] + RMcoll[M022]);
        double pop_out_08 =  0.5*rho * (-RMcoll[M011]              - RMcoll[M012]) + pop_out_17;
        double pop_out_18 =  0.5*rho * (-RMcoll[M011] - RMcoll[M021])              + pop_out_17;
        double pop_out_07 =  0.5*rho * (              - RMcoll[M021] - RMcoll[M012]) + pop_out_17;


        pop[ 0] = pop_out_00;
        pop[10] = pop_out_10;

        pop[ 1] = pop_out_01;
        pop[11] = pop_out_11;

        pop[ 2] = pop_out_02;
        pop[12] = pop_out_12;

        pop[ 3] = pop_out_03;
        pop[13] = pop_out_13;

        pop[ 4] = pop_out_04;
        pop[14] = pop_out_14;

        pop[ 5] = pop_out_05;
        pop[15] = pop_out_15;

        pop[ 6] = pop_out_06;
        pop[16] = pop_out_16;

        pop[ 7] = pop_out_07;
        pop[17] = pop_out_17;

        pop[ 8] = pop_out_08;
        pop[18] = pop_out_18;

        pop[ 9] = pop_out_09;
    }

    auto streamingPull(int i, int iX, int iY, int iZ, std::array<double, 19>& pop)
    {
        if(periodic){
            for (int k = 0; k < 19; ++k) {
                int XX = (iX - c[k][0] + dim.nx) % dim.nx;
                int YY = (iY - c[k][1] + dim.ny) % dim.ny;
                int ZZ = (iZ - c[k][2] + dim.nz) % dim.nz;
                size_t nb = xyz_to_i(XX, YY, ZZ);
                CellType nbCellType = flag[nb];
                if (nbCellType == CellType::bounce_back) {
                    pop[k] = f(i, k) + f(nb, opp[k]);
                }
                else {
                    pop[k] = f(nb, opp[k]);
                }
            }
        }
        else {
            for (int k = 0; k < 19; ++k) {
                int XX = iX - c[k][0];
                int YY = iY - c[k][1];
                int ZZ = iZ - c[k][2];
                size_t nb = xyz_to_i(XX, YY, ZZ);
                CellType nbCellType = flag[nb];
                if (nbCellType == CellType::bounce_back) {
                    pop[k] = f(i, k) + f(nb, opp[k]);
                }
                else {
                    pop[k] = f(nb, opp[k]);
                }
            }
        }
    }

    auto streamingPush(int i, int iX, int iY, int iZ, std::array<double, 19>& pop)
    {
        if(periodic){
            for (int k = 0; k < 19; ++k) {
                int XX = (iX + c[k][0] + dim.nx) % dim.nx;
                int YY = (iY + c[k][1] + dim.ny) % dim.ny;
                int ZZ = (iZ + c[k][2] + dim.nz) % dim.nz;
                size_t nb = xyz_to_i(XX, YY, ZZ);
                CellType nbCellType = flag[nb];
                if (nbCellType == CellType::bounce_back) {
                    f(i, opp[k]) = pop[k] + f(nb, k);
                }
                else {
                    f(nb, k) = pop[k];
                }
            }
        }
        else {
            for (int k = 0; k < 19; ++k) {
                int XX = iX + c[k][0];
                int YY = iY + c[k][1];
                int ZZ = iZ + c[k][2];
                size_t nb = xyz_to_i(XX, YY, ZZ);
                CellType nbCellType = flag[nb];
                if (nbCellType == CellType::bounce_back) {
                    f(i, opp[k]) = pop[k] + f(nb, k);
                }
                else {
                    f(nb, k) = pop[k];
                }
            }
        }
    }

    void iterateRM(double& f0) {
        int i = &f0 - lattice;
        auto[iX, iY, iZ] = i_to_xyz(i);
        CellType cellType = flag[i];

        if (cellType == CellType::bulk) {
            std::array<double, 19> pop;

            streamingPull(i, iX, iY, iZ, pop);

/*            auto[rho, u] = macropop(pop);
            auto RM = computeRMopt(pop, rho);*/
            auto RM = computeRMopt2(pop);
            double rho = RM[M000];
            std::array<double, 3> u = {RM[M100], RM[M010], RM[M001]};
            auto RMeq = computeRMeq(u);

            collideRM(pop, rho, u, RM, RMeq);

            streamingPush(i, iX, iY, iZ, pop);
        }
    }

    void operator() (double& f0) {
        iterateRM(f0);
    }
};

} // namespace aa_soa_rm
