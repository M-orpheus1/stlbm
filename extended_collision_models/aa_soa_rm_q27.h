// *****************************************************************************
// STLBM SOFTWARE LIBRARY

// Copyright © 2020 University of Geneva
// Authors: Jonas Latt, Christophe Coreixas, Joël Beny
// Contact: Jonas.Latt@unige.ch

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// *****************************************************************************

#pragma once

#include "lbm_q27.h"
#include <vector>
#include <array>
#include <tuple>

namespace aa_soa_rm {

struct LBM {
    using CellData = double;
    static size_t sizeOfLattice(size_t nelem) { return 27 * nelem; }

    CellData* lattice;
    CellType* flag;
    std::array<int, 3>* c;
    int* opp;
    double* t;
    double omega1;
    Dim dim;
    LBModel model;
    bool periodic = false;
    double omegaBulk = 0.;
    double omega2 = omega1;
    double omega3 = omega1;
    double omega4 = omega1;
    double omega5 = omega1;
    double omega6 = omega1;
    double omega7 = omega1;
    double omega8 = omega1;

    auto i_to_xyz (int i) const {
        int iX = i / (dim.ny * dim.nz);
        int remainder = i % (dim.ny * dim.nz);
        int iY = remainder / dim.nz;
        int iZ = remainder % dim.nz;
        return std::make_tuple(iX, iY, iZ);
    };

    size_t xyz_to_i (int x, int y, int z) const {
        return z + dim.nz * (y + dim.ny * x);
    };

    double& f (int i, int k) {
        return lattice[k * dim.nelem + i];
    }

    auto iniLattice (double& f0) {
        auto i = &f0 - lattice;
        for (int k = 0; k < 27; ++k) {
            f(i, k) = t[k];
        }
    };

    auto iniLattice (double& f0, double rho, std::array<double, 3> const& u) {
        auto i = &f0 - lattice;
        double usqr = 1.5 * (u[0] * u[0] + u[1] * u[1] + u[2] * u[2]);
        for (int k = 0; k < 27; ++k) {
            double ck_u = c[k][0] * u[0] + c[k][1] * u[1] + c[k][2] * u[2];
            f(i, k) = rho * t[k] * (1. + 3. * ck_u + 4.5 * ck_u * ck_u - usqr);
        }
    };

    auto macro (double const& f0) {
        auto i = &f0 - lattice;
        double X_M1 = f(i, 0) + f(i, 3) + f(i, 4) + f(i, 5) + f(i, 6) + f(i, 9) + f(i,10) + f(i,11) + f(i,12);
        double X_P1 = f(i,14) + f(i,17) + f(i,18) + f(i,19) + f(i,20) + f(i,23) + f(i,24) + f(i,25) + f(i,26);
        double X_0  = f(i, 1) + f(i, 2) + f(i, 7) + f(i, 8) + f(i,13) + f(i,15) + f(i,16) + f(i,21) + f(i,22);

        double Y_M1 = f(i, 1) + f(i, 3) + f(i, 7) + f(i, 8) + f(i, 9) + f(i,10) + f(i,18) + f(i,25) + f(i,26);
        double Y_P1 = f(i,15) + f(i,17) + f(i,21) + f(i,22) + f(i,23) + f(i,24) + f(i, 4) + f(i,11) + f(i,12);

        double Z_M1 = f(i, 2) + f(i, 5) + f(i, 7) + f(i, 9) + f(i,11) + f(i,20) + f(i,22) + f(i,24) + f(i,26);
        double Z_P1 = f(i,16) + f(i,19) + f(i,21) + f(i,23) + f(i,25) + f(i, 6) + f(i, 8) + f(i,10) + f(i,12);

        double rho = X_M1 + X_P1 + X_0;
        std::array<double, 3> u{ (X_P1 - X_M1) / rho, (Y_P1 - Y_M1) / rho, (Z_P1 - Z_M1) / rho };
        return make_pair(rho, u);
    }

    auto macropop (std::array<double, 27> const& pop) {
        double X_M1 = pop[ 0] + pop[ 3] + pop[ 4] + pop[ 5] + pop[ 6] + pop[ 9] + pop[10] + pop[11] + pop[12];
        double X_P1 = pop[14] + pop[17] + pop[18] + pop[19] + pop[20] + pop[23] + pop[24] + pop[25] + pop[26];
        double X_0  = pop[ 1] + pop[ 2] + pop[ 7] + pop[ 8] + pop[13] + pop[15] + pop[16] + pop[21] + pop[22];

        double Y_M1 = pop[ 1] + pop[ 3] + pop[ 7] + pop[ 8] + pop[ 9] + pop[10] + pop[18] + pop[25] + pop[26];
        double Y_P1 = pop[15] + pop[17] + pop[21] + pop[22] + pop[23] + pop[24] + pop[ 4] + pop[11] + pop[12];

        double Z_M1 = pop[ 2] + pop[ 5] + pop[ 7] + pop[ 9] + pop[11] + pop[20] + pop[22] + pop[24] + pop[26];
        double Z_P1 = pop[16] + pop[19] + pop[21] + pop[23] + pop[25] + pop[ 6] + pop[ 8] + pop[10] + pop[12];

        double rho = X_M1 + X_P1 + X_0;
        std::array<double, 3> u{ (X_P1 - X_M1) / rho, (Y_P1 - Y_M1) / rho, (Z_P1 - Z_M1) / rho };
        return make_pair(rho, u);
    };

    // Naive way to compute RM and macros
    auto computeRM(std::array<double, 27> const& pop) {
        std::array<double, 27> RM;
        std::fill(RM.begin(), RM.end(), 0.);
        for (int k = 0; k<27; ++k) {
            // Order 0
            RM[M000] += pop[k];
            // Order 1
            RM[M100] += c[k][0] * pop[k];
            RM[M010] += c[k][1] * pop[k];
            RM[M001] += c[k][2] * pop[k];
            // Order 2
            RM[M200] += c[k][0] * c[k][0] * pop[k];
            RM[M020] += c[k][1] * c[k][1] * pop[k];
            RM[M002] += c[k][2] * c[k][2] * pop[k];
            RM[M110] += c[k][0] * c[k][1] * pop[k];
            RM[M101] += c[k][0] * c[k][2] * pop[k];
            RM[M011] += c[k][1] * c[k][2] * pop[k];
            // Order 3
            RM[M210] += c[k][0] * c[k][0] * c[k][1] * pop[k];
            RM[M201] += c[k][0] * c[k][0] * c[k][2] * pop[k];
            RM[M021] += c[k][1] * c[k][1] * c[k][2] * pop[k];
            RM[M120] += c[k][0] * c[k][1] * c[k][1] * pop[k];
            RM[M102] += c[k][0] * c[k][2] * c[k][2] * pop[k];
            RM[M012] += c[k][1] * c[k][2] * c[k][2] * pop[k];
            RM[M111] += c[k][0] * c[k][1] * c[k][2] * pop[k];
            // Order 4
            RM[M220] += c[k][0] * c[k][0] * c[k][1] * c[k][1] * pop[k];
            RM[M202] += c[k][0] * c[k][0] * c[k][2] * c[k][2] * pop[k];
            RM[M022] += c[k][1] * c[k][1] * c[k][2] * c[k][2] * pop[k];
            RM[M211] += c[k][0] * c[k][0] * c[k][1] * c[k][2] * pop[k];
            RM[M121] += c[k][0] * c[k][1] * c[k][1] * c[k][2] * pop[k];
            RM[M112] += c[k][0] * c[k][1] * c[k][2] * c[k][2] * pop[k];
            // Order 5
            RM[M221] += c[k][0] * c[k][0] * c[k][1] * c[k][1] * c[k][2] * pop[k];
            RM[M212] += c[k][0] * c[k][0] * c[k][1] * c[k][2] * c[k][2] * pop[k];
            RM[M122] += c[k][0] * c[k][1] * c[k][1] * c[k][2] * c[k][2] * pop[k];
            // Order 6
            RM[M222] += c[k][0] * c[k][0] * c[k][1] * c[k][1] * c[k][2] * c[k][2] * pop[k];
        }

        double invRho = 1. / RM[M000];
        for (int k = 1; k<27; ++k) {
            RM[k] *= invRho;
        }
        return RM;
    }

    // First optimization (loop unrolling)
    auto computeRMopt(std::array<double, 27> const& pop) {
        std::array<double, 27> RM;
        std::fill(RM.begin(), RM.end(), 0.);
        // Order 0
        RM[M000] =  pop[ 0] + pop[ 1] + pop[ 2] + pop[ 3] + pop[ 4] + pop[ 5] + pop[ 6] + pop[ 7] + pop[ 8] + pop[ 9] + pop[10] + pop[11] + pop[12] + pop[13] + pop[14] + pop[15] + pop[16] + pop[17] + pop[18] + pop[19] + pop[20] + pop[21] + pop[22] + pop[23] + pop[24] + pop[25] + pop[26];
        double invRho = 1./RM[M000];
        // Order 1
        RM[M100] = invRho * (- pop[ 0] - pop[ 3] - pop[ 4] - pop[ 5] - pop[ 6] - pop[ 9] - pop[10] - pop[11] - pop[12] + pop[14] + pop[17] + pop[18] + pop[19] + pop[20] + pop[23] + pop[24] + pop[25] + pop[26]);
        RM[M010] = invRho * (- pop[ 1] - pop[ 3] + pop[ 4] - pop[ 7] - pop[ 8] - pop[ 9] - pop[10] + pop[11] + pop[12] + pop[15] + pop[17] - pop[18] + pop[21] + pop[22] + pop[23] + pop[24] - pop[25] - pop[26]);
        RM[M001] = invRho * (- pop[ 2] - pop[ 5] + pop[ 6] - pop[ 7] + pop[ 8] - pop[ 9] + pop[10] - pop[11] + pop[12] + pop[16] + pop[19] - pop[20] + pop[21] - pop[22] + pop[23] - pop[24] + pop[25] - pop[26]);
        // Order 2
        RM[M200] = invRho * (  pop[ 0] + pop[ 3] + pop[ 4] + pop[ 5] + pop[ 6] + pop[ 9] + pop[10] + pop[11] + pop[12] + pop[14] + pop[17] + pop[18] + pop[19] + pop[20] + pop[23] + pop[24] + pop[25] + pop[26]);
        RM[M020] = invRho * (  pop[ 1] + pop[ 3] + pop[ 4] + pop[ 7] + pop[ 8] + pop[ 9] + pop[10] + pop[11] + pop[12] + pop[15] + pop[17] + pop[18] + pop[21] + pop[22] + pop[23] + pop[24] + pop[25] + pop[26]);
        RM[M002] = invRho * (  pop[ 2] + pop[ 5] + pop[ 6] + pop[ 7] + pop[ 8] + pop[ 9] + pop[10] + pop[11] + pop[12] + pop[16] + pop[19] + pop[20] + pop[21] + pop[22] + pop[23] + pop[24] + pop[25] + pop[26]);
        RM[M110] = invRho * (  pop[ 3] - pop[ 4] + pop[ 9] + pop[10] - pop[11] - pop[12] + pop[17] - pop[18] + pop[23] + pop[24] - pop[25] - pop[26]);
        RM[M101] = invRho * (  pop[ 5] - pop[ 6] + pop[ 9] - pop[10] + pop[11] - pop[12] + pop[19] - pop[20] + pop[23] - pop[24] + pop[25] - pop[26]);
        RM[M011] = invRho * (  pop[ 7] - pop[ 8] + pop[ 9] - pop[10] - pop[11] + pop[12] + pop[21] - pop[22] + pop[23] - pop[24] - pop[25] + pop[26]);
        // Order 3
        RM[M210] = invRho * (- pop[ 3] + pop[ 4] - pop[ 9] - pop[10] + pop[11] + pop[12] + pop[17] - pop[18] + pop[23] + pop[24] - pop[25] - pop[26]);
        RM[M201] = invRho * (- pop[ 5] + pop[ 6] - pop[ 9] + pop[10] - pop[11] + pop[12] + pop[19] - pop[20] + pop[23] - pop[24] + pop[25] - pop[26]);
        RM[M021] = invRho * (- pop[ 7] + pop[ 8] - pop[ 9] + pop[10] - pop[11] + pop[12] + pop[21] - pop[22] + pop[23] - pop[24] + pop[25] - pop[26]);
        RM[M120] = invRho * (- pop[ 3] - pop[ 4] - pop[ 9] - pop[10] - pop[11] - pop[12] + pop[17] + pop[18] + pop[23] + pop[24] + pop[25] + pop[26]);
        RM[M102] = invRho * (- pop[ 5] - pop[ 6] - pop[ 9] - pop[10] - pop[11] - pop[12] + pop[19] + pop[20] + pop[23] + pop[24] + pop[25] + pop[26]);
        RM[M012] = invRho * (- pop[ 7] - pop[ 8] - pop[ 9] - pop[10] + pop[11] + pop[12] + pop[21] + pop[22] + pop[23] + pop[24] - pop[25] - pop[26]);
        RM[M111] = invRho * (- pop[ 9] + pop[10] + pop[11] - pop[12] + pop[23] - pop[24] - pop[25] + pop[26]);
        // Order 4
        RM[M220] = invRho * (  pop[ 3] + pop[ 4] + pop[ 9] + pop[10] + pop[11] + pop[12] + pop[17] + pop[18] + pop[23] + pop[24] + pop[25] + pop[26]);
        RM[M202] = invRho * (  pop[ 5] + pop[ 6] + pop[ 9] + pop[10] + pop[11] + pop[12] + pop[19] + pop[20] + pop[23] + pop[24] + pop[25] + pop[26]);
        RM[M022] = invRho * (  pop[ 7] + pop[ 8] + pop[ 9] + pop[10] + pop[11] + pop[12] + pop[21] + pop[22] + pop[23] + pop[24] + pop[25] + pop[26]);
        RM[M211] = invRho * (  pop[ 9] - pop[10] - pop[11] + pop[12] + pop[23] - pop[24] - pop[25] + pop[26]);
        RM[M121] = invRho * (  pop[ 9] - pop[10] + pop[11] - pop[12] + pop[23] - pop[24] + pop[25] - pop[26]);
        RM[M112] = invRho * (  pop[ 9] + pop[10] - pop[11] - pop[12] + pop[23] + pop[24] - pop[25] - pop[26]);
        // Order 5
        RM[M221] = invRho * (- pop[ 9] + pop[10] - pop[11] + pop[12] + pop[23] - pop[24] + pop[25] - pop[26]);
        RM[M212] = invRho * (- pop[ 9] - pop[10] + pop[11] + pop[12] + pop[23] + pop[24] - pop[25] - pop[26]);
        RM[M122] = invRho * (- pop[ 9] - pop[10] - pop[11] - pop[12] + pop[23] + pop[24] + pop[25] + pop[26]);
        // Order 6
        RM[M222] = invRho * (  pop[ 9] + pop[10] + pop[11] + pop[12] + pop[23] + pop[24] + pop[25] + pop[26]);

        return RM;
    }

    // Most optimized version (loop unrolling + redundant terms)
    // Further pre-computing terms is counter-productive (less than 1% gain)
    auto computeRMopt2(std::array<double, 27> const& pop) {
        std::array<double, 27> RM;
        std::fill(RM.begin(), RM.end(), 0.);

        double A1 = pop[ 9] + pop[10] + pop[11] + pop[12];
        double A2 = pop[23] + pop[24] + pop[25] + pop[26];
        double A3 = pop[ 9] + pop[10] - pop[11] - pop[12];
        double A4 = pop[23] + pop[24] - pop[25] - pop[26];
        double A5 = pop[ 9] - pop[10] + pop[11] - pop[12];
        double A6 = pop[23] - pop[24] + pop[25] - pop[26];
        double A7 = pop[ 9] - pop[10] - pop[11] + pop[12];
        double A8 = pop[23] - pop[24] - pop[25] + pop[26];

        double X_M1 = pop[ 0] + pop[ 3] + pop[ 4] + pop[ 5] + pop[ 6] + A1;
        double X_P1 = pop[14] + pop[17] + pop[18] + pop[19] + pop[20] + A2;
        double X_0  = pop[ 1] + pop[ 2] + pop[ 7] + pop[ 8] + pop[13] + pop[15] + pop[16] + pop[21] + pop[22];

        double Y_M1 = pop[ 1] + pop[ 3] + pop[ 7] + pop[ 8] + pop[ 9] + pop[10] + pop[18] + pop[25] + pop[26];
        double Y_P1 = pop[15] + pop[17] + pop[21] + pop[22] + pop[23] + pop[24] + pop[ 4] + pop[11] + pop[12];

        double Z_M1 = pop[ 2] + pop[ 5] + pop[ 7] + pop[ 9] + pop[11] + pop[20] + pop[22] + pop[24] + pop[26];
        double Z_P1 = pop[16] + pop[19] + pop[21] + pop[23] + pop[25] + pop[ 6] + pop[ 8] + pop[10] + pop[12];

        // Order 0
        RM[M000] = X_M1 + X_P1 + X_0;
        double invRho = 1. / RM[M000];

        // Order 6
        RM[M222] = invRho * (  A1 + A2);
        // Order 5
        RM[M221] = invRho * (- A5 + A6);
        RM[M212] = invRho * (- A3 + A4);
        RM[M122] = invRho * (- A1 + A2);
        // Order 4
        RM[M220] = invRho * (  pop[ 3] + pop[ 4] + pop[17] + pop[18]) + RM[M222];
        RM[M202] = invRho * (  pop[ 5] + pop[ 6] + pop[19] + pop[20]) + RM[M222];
        RM[M022] = invRho * (  pop[ 7] + pop[ 8] + pop[21] + pop[22]) + RM[M222];
        RM[M211] = invRho * (  A7 + A8);
        RM[M121] = invRho * (  A5 + A6);
        RM[M112] = invRho * (  A3 + A4);
        // Order 3
        RM[M210] = invRho * (- pop[ 3] + pop[ 4] + pop[17] - pop[18]) + RM[M212];
        RM[M201] = invRho * (- pop[ 5] + pop[ 6] + pop[19] - pop[20]) + RM[M221];
        RM[M021] = invRho * (- pop[ 7] + pop[ 8] + pop[21] - pop[22]) + RM[M221];
        RM[M120] = invRho * (- pop[ 3] - pop[ 4] + pop[17] + pop[18]) + RM[M122];
        RM[M102] = invRho * (- pop[ 5] - pop[ 6] + pop[19] + pop[20]) + RM[M122];
        RM[M012] = invRho * (- pop[ 7] - pop[ 8] + pop[21] + pop[22]) + RM[M212];
        RM[M111] = invRho * (- A7 + A8);
        // Order 2
        RM[M200] = invRho * (X_P1 + X_M1);
        RM[M020] = invRho * (Y_P1 + Y_M1); 
        RM[M002] = invRho * (Z_P1 + Z_M1);
        RM[M110] = invRho * (  pop[ 3] - pop[ 4] + pop[17] - pop[18]) + RM[M112];
        RM[M101] = invRho * (  pop[ 5] - pop[ 6] + pop[19] - pop[20]) + RM[M121];
        RM[M011] = invRho * (  pop[ 7] - pop[ 8] + pop[21] - pop[22]) + RM[M211];
        // Order 1
        RM[M100] = invRho * (X_P1 - X_M1);
        RM[M010] = invRho * (Y_P1 - Y_M1); 
        RM[M001] = invRho * (Z_P1 - Z_M1);

        return RM;
    }

    auto computeRMeq(std::array<double, 3> const& u) {

        std::array<double, 27> RMeq;
        std::fill(RMeq.begin(), RMeq.end(), 0.);
        double cs2 = 1./3.;

        // Order 2
        RMeq[M200] = u[0] * u[0] + cs2;
        RMeq[M020] = u[1] * u[1] + cs2;
        RMeq[M002] = u[2] * u[2] + cs2;
        RMeq[M110] = u[0] * u[1];
        RMeq[M101] = u[0] * u[2];
        RMeq[M011] = u[1] * u[2];
        // Order 3
        RMeq[M210] = RMeq[M200] * u[1];
        RMeq[M201] = RMeq[M200] * u[2];
        RMeq[M021] = RMeq[M020] * u[2];
        RMeq[M120] = RMeq[M020] * u[0];
        RMeq[M102] = RMeq[M002] * u[0];
        RMeq[M012] = RMeq[M002] * u[1];
        RMeq[M111] = RMeq[M110] * u[2];
        // Order 4
        RMeq[M220] = RMeq[M200] * RMeq[M020];
        RMeq[M202] = RMeq[M200] * RMeq[M002];
        RMeq[M022] = RMeq[M020] * RMeq[M002];
        RMeq[M211] = RMeq[M200] * RMeq[M011];
        RMeq[M121] = RMeq[M020] * RMeq[M101];
        RMeq[M112] = RMeq[M002] * RMeq[M110];
        // Order 5
        RMeq[M221] = RMeq[M220] * u[2];
        RMeq[M212] = RMeq[M202] * u[1];
        RMeq[M122] = RMeq[M022] * u[0];
        // Order 6
        RMeq[M222] = RMeq[M220] * RMeq[M002];

        return RMeq;
    }
};

struct Even : public LBM {

    auto collideRM(int i, double rho, std::array<double, 3> const& u, std::array<double, 27> const& RM, std::array<double, 27> const& RMeq)
    {
        // Post-collision moments.
        std::array<double, 27> RMcoll;

        // Order 2
        if (omegaBulk == 0.) { // Don't adjust bulk viscosity 
            RMcoll[M200] = (1. - omega1) * RM[M200] + omega1 * RMeq[M200];
            RMcoll[M020] = (1. - omega1) * RM[M020] + omega1 * RMeq[M020];
            RMcoll[M002] = (1. - omega1) * RM[M002] + omega1 * RMeq[M002];
        }
        else { // Adjust bulk viscosity
            const double omegaMinus = (omegaBulk - omega1)/3.; // Notation used by Fei
            const double omegaPlus  = omegaMinus + omega1;     // Notation used by Fei
            RMcoll[M200] = RM[M200] - omegaPlus  * (RM[M200]-RMeq[M200]) - omegaMinus * (RM[M020]-RMeq[M020]) - omegaMinus * (RM[M002]-RMeq[M002]) ;
            RMcoll[M020] = RM[M020] - omegaMinus * (RM[M200]-RMeq[M200]) - omegaPlus  * (RM[M020]-RMeq[M020]) - omegaMinus * (RM[M002]-RMeq[M002]) ;
            RMcoll[M002] = RM[M002] - omegaMinus * (RM[M200]-RMeq[M200]) - omegaMinus * (RM[M020]-RMeq[M020]) - omegaPlus  * (RM[M002]-RMeq[M002]) ;
        }

        RMcoll[M110] = (1. - omega2) * RM[M110] + omega2 * RMeq[M110] ;
        RMcoll[M101] = (1. - omega2) * RM[M101] + omega2 * RMeq[M101] ;
        RMcoll[M011] = (1. - omega2) * RM[M011] + omega2 * RMeq[M011] ;

        // Order 3
        RMcoll[M210] = (1. - omega3) * RM[M210] + omega3 * RMeq[M210] ;
        RMcoll[M201] = (1. - omega3) * RM[M201] + omega3 * RMeq[M201] ;
        RMcoll[M021] = (1. - omega3) * RM[M021] + omega3 * RMeq[M021] ;
        RMcoll[M120] = (1. - omega3) * RM[M120] + omega3 * RMeq[M120] ;
        RMcoll[M102] = (1. - omega3) * RM[M102] + omega3 * RMeq[M102] ;
        RMcoll[M012] = (1. - omega3) * RM[M012] + omega3 * RMeq[M012] ;
        
        RMcoll[M111] = (1. - omega4) * RM[M111] + omega4 * RMeq[M111] ;

        // Order 4
        RMcoll[M220] = (1. - omega5) * RM[M220] + omega5 * RMeq[M220] ;
        RMcoll[M202] = (1. - omega5) * RM[M202] + omega5 * RMeq[M202] ;
        RMcoll[M022] = (1. - omega5) * RM[M022] + omega5 * RMeq[M022] ;

        RMcoll[M211] = (1. - omega6) * RM[M211] + omega6 * RMeq[M211] ;
        RMcoll[M121] = (1. - omega6) * RM[M121] + omega6 * RMeq[M121] ;
        RMcoll[M112] = (1. - omega6) * RM[M112] + omega6 * RMeq[M112] ;

        // Order 5
        RMcoll[M221] = (1. - omega7) * RM[M221] + omega7 * RMeq[M221] ;
        RMcoll[M212] = (1. - omega7) * RM[M212] + omega7 * RMeq[M212] ;
        RMcoll[M122] = (1. - omega7) * RM[M122] + omega7 * RMeq[M122] ;

        // Order 6
        RMcoll[M222] = (1. - omega8) * RM[M222] + omega8 * RMeq[M222] ;


        // Optimization based on symmetries between populations and their opposite counterpart
        std::array<double, 27> foutRM;

        foutRM[F000] = rho *(1. - RMcoll[M200] - RMcoll[M020] - RMcoll[M002] + RMcoll[M220] + RMcoll[M202] + RMcoll[M022] - RMcoll[M222]);
        
        foutRM[FP00] = 0.5*rho * ( u[0] + RMcoll[M200] - RMcoll[M120] - RMcoll[M102] - RMcoll[M220] - RMcoll[M202] + RMcoll[M122] + RMcoll[M222]);
        foutRM[FM00] =     rho * (-u[0]                + RMcoll[M120] + RMcoll[M102]                               - RMcoll[M122])+ foutRM[FP00];

        foutRM[F0P0] = 0.5*rho * ( u[1] + RMcoll[M020] - RMcoll[M210] - RMcoll[M012] - RMcoll[M220] - RMcoll[M022] + RMcoll[M212] + RMcoll[M222]);
        foutRM[F0M0] =     rho * (-u[1]                + RMcoll[M210] + RMcoll[M012]                               - RMcoll[M212])+ foutRM[F0P0];

        foutRM[F00P] = 0.5*rho * ( u[2] + RMcoll[M002] - RMcoll[M201] - RMcoll[M021] - RMcoll[M202] - RMcoll[M022] + RMcoll[M221] + RMcoll[M222]);
        foutRM[F00M] =     rho * (-u[2]                + RMcoll[M201] + RMcoll[M021]                               - RMcoll[M221])+ foutRM[F00P];

        foutRM[FPP0] = 0.25*rho * ( RMcoll[M110] + RMcoll[M210] + RMcoll[M120] - RMcoll[M112] + RMcoll[M220] - RMcoll[M212] - RMcoll[M122] - RMcoll[M222]);
        foutRM[FMP0] =  0.5*rho * (-RMcoll[M110]                - RMcoll[M120] + RMcoll[M112]                               + RMcoll[M122])+ foutRM[FPP0];
        foutRM[FPM0] =  0.5*rho * (-RMcoll[M110] - RMcoll[M210]                + RMcoll[M112]                + RMcoll[M212]               )+ foutRM[FPP0];
        foutRM[FMM0] =  0.5*rho * (              - RMcoll[M210] - RMcoll[M120]                               + RMcoll[M212] + RMcoll[M122])+ foutRM[FPP0];

        foutRM[FP0P] = 0.25*rho * ( RMcoll[M101] + RMcoll[M201] + RMcoll[M102] - RMcoll[M121] + RMcoll[M202] - RMcoll[M221] - RMcoll[M122] - RMcoll[M222]);
        foutRM[FM0P] =  0.5*rho * (-RMcoll[M101]                - RMcoll[M102] + RMcoll[M121]                               + RMcoll[M122])+ foutRM[FP0P];
        foutRM[FP0M] =  0.5*rho * (-RMcoll[M101] - RMcoll[M201]                + RMcoll[M121]                + RMcoll[M221]               )+ foutRM[FP0P];
        foutRM[FM0M] =  0.5*rho * (              - RMcoll[M201] - RMcoll[M102]                               + RMcoll[M221] + RMcoll[M122])+ foutRM[FP0P];

        foutRM[F0PP] = 0.25*rho * ( RMcoll[M011] + RMcoll[M021] + RMcoll[M012] - RMcoll[M211] + RMcoll[M022] - RMcoll[M221] - RMcoll[M212] - RMcoll[M222]);
        foutRM[F0MP] =  0.5*rho * (-RMcoll[M011]                - RMcoll[M012] + RMcoll[M211]                               + RMcoll[M212])+ foutRM[F0PP];
        foutRM[F0PM] =  0.5*rho * (-RMcoll[M011] - RMcoll[M021]                + RMcoll[M211]                + RMcoll[M221]               )+ foutRM[F0PP];
        foutRM[F0MM] =  0.5*rho * (              - RMcoll[M021] - RMcoll[M012]                               + RMcoll[M221] + RMcoll[M212])+ foutRM[F0PP];

        foutRM[FPPP] = 0.125*rho * ( RMcoll[M111] + RMcoll[M211] + RMcoll[M121] + RMcoll[M112] + RMcoll[M221] + RMcoll[M212] + RMcoll[M122] + RMcoll[M222]);
        foutRM[FMPP] =  0.25*rho * (-RMcoll[M111]                - RMcoll[M121] - RMcoll[M112]                               - RMcoll[M122])+ foutRM[FPPP];
        foutRM[FPMP] =  0.25*rho * (-RMcoll[M111] - RMcoll[M211]                - RMcoll[M112]                - RMcoll[M212]               )+ foutRM[FPPP];
        foutRM[FPPM] =  0.25*rho * (-RMcoll[M111] - RMcoll[M211] - RMcoll[M121]                - RMcoll[M221]                              )+ foutRM[FPPP];
        foutRM[FMMP] =  0.25*rho * (              - RMcoll[M211] - RMcoll[M121]                               - RMcoll[M212] - RMcoll[M122])+ foutRM[FPPP];
        foutRM[FMPM] =  0.25*rho * (              - RMcoll[M211]                - RMcoll[M112] - RMcoll[M221]                - RMcoll[M122])+ foutRM[FPPP];
        foutRM[FPMM] =  0.25*rho * (                             - RMcoll[M121] - RMcoll[M112] - RMcoll[M221] - RMcoll[M212]               )+ foutRM[FPPP];
        foutRM[FMMM] =  0.25*rho * (-RMcoll[M111]                                              - RMcoll[M221] - RMcoll[M212] - RMcoll[M122])+ foutRM[FPPP];

        f(i,F000) = foutRM[F000];

        f(i,FP00) = foutRM[FM00];
        f(i,FM00) = foutRM[FP00];

        f(i,F0P0) = foutRM[F0M0];
        f(i,F0M0) = foutRM[F0P0];

        f(i,F00P) = foutRM[F00M];
        f(i,F00M) = foutRM[F00P];

        f(i,FPP0) = foutRM[FMM0];
        f(i,FMP0) = foutRM[FPM0];
        f(i,FPM0) = foutRM[FMP0];
        f(i,FMM0) = foutRM[FPP0];

        f(i,FP0P) = foutRM[FM0M];
        f(i,FM0P) = foutRM[FP0M];
        f(i,FP0M) = foutRM[FM0P];
        f(i,FM0M) = foutRM[FP0P];

        f(i,F0PP) = foutRM[F0MM];
        f(i,F0MP) = foutRM[F0PM];
        f(i,F0PM) = foutRM[F0MP];
        f(i,F0MM) = foutRM[F0PP];

        f(i,FPPP) = foutRM[FMMM];
        f(i,FMPP) = foutRM[FPMM];
        f(i,FPMP) = foutRM[FMPM];
        f(i,FPPM) = foutRM[FMMP];
        f(i,FMMP) = foutRM[FPPM];
        f(i,FMPM) = foutRM[FPMP];
        f(i,FPMM) = foutRM[FMPP];
        f(i,FMMM) = foutRM[FPPP];
    }


    void iterateRM(double& f0) {
        int i = &f0 - lattice;
        if (flag[i] == CellType::bulk) {
            std::array<double, 27> pop;
            for (int k = 0; k < 27; ++k) {
                pop[k] = f(i, k);
            }
/*          auto RM = computeRM(pop);
            auto RM = computeRMopt(pop);*/
            auto RM = computeRMopt2(pop);
            double rho = RM[M000];
            std::array<double, 3> u = {RM[M100], RM[M010], RM[M001]};
            auto RMeq = computeRMeq(u);
            collideRM(i, rho, u, RM, RMeq);
        }
    }

    void operator() (double& f0) {
        iterateRM(f0);
    }
};

struct Odd : public LBM {

    auto collideRM(std::array<double, 27>& pop, double rho, std::array<double, 3> const& u, std::array<double, 27> const& RM, std::array<double, 27> const& RMeq)
    {
        // Post-collision moments.
        std::array<double, 27> RMcoll;

        // Order 2
        if (omegaBulk == 0.) { // Don't adjust bulk viscosity
            RMcoll[M200] = (1. - omega1) * RM[M200] + omega1 * RMeq[M200];
            RMcoll[M020] = (1. - omega1) * RM[M020] + omega1 * RMeq[M020];
            RMcoll[M002] = (1. - omega1) * RM[M002] + omega1 * RMeq[M002];
        }
        else { // Adjust bulk viscosity
            const double omegaBulk = omega1;
            const double omegaMinus = (omegaBulk - omega1)/3.; // Notation used by Fei
            const double omegaPlus  = omegaMinus + omega1;     // Notation used by Fei
            RMcoll[M200] = RM[M200] - omegaPlus  * (RM[M200]-RMeq[M200]) - omegaMinus * (RM[M020]-RMeq[M020]) - omegaMinus * (RM[M002]-RMeq[M002]) ;
            RMcoll[M020] = RM[M020] - omegaMinus * (RM[M200]-RMeq[M200]) - omegaPlus  * (RM[M020]-RMeq[M020]) - omegaMinus * (RM[M002]-RMeq[M002]) ;
            RMcoll[M002] = RM[M002] - omegaMinus * (RM[M200]-RMeq[M200]) - omegaMinus * (RM[M020]-RMeq[M020]) - omegaPlus  * (RM[M002]-RMeq[M002]) ;
        }
        
        RMcoll[M110] = (1. - omega2) * RM[M110] + omega2 * RMeq[M110] ;
        RMcoll[M101] = (1. - omega2) * RM[M101] + omega2 * RMeq[M101] ;
        RMcoll[M011] = (1. - omega2) * RM[M011] + omega2 * RMeq[M011] ;

        // Order 3
        RMcoll[M210] = (1. - omega3) * RM[M210] + omega3 * RMeq[M210] ;
        RMcoll[M201] = (1. - omega3) * RM[M201] + omega3 * RMeq[M201] ;
        RMcoll[M021] = (1. - omega3) * RM[M021] + omega3 * RMeq[M021] ;
        RMcoll[M120] = (1. - omega3) * RM[M120] + omega3 * RMeq[M120] ;
        RMcoll[M102] = (1. - omega3) * RM[M102] + omega3 * RMeq[M102] ;
        RMcoll[M012] = (1. - omega3) * RM[M012] + omega3 * RMeq[M012] ;
        
        RMcoll[M111] = (1. - omega4) * RM[M111] + omega4 * RMeq[M111] ;

        // Order 4
        RMcoll[M220] = (1. - omega5) * RM[M220] + omega5 * RMeq[M220] ;
        RMcoll[M202] = (1. - omega5) * RM[M202] + omega5 * RMeq[M202] ;
        RMcoll[M022] = (1. - omega5) * RM[M022] + omega5 * RMeq[M022] ;

        RMcoll[M211] = (1. - omega6) * RM[M211] + omega6 * RMeq[M211] ;
        RMcoll[M121] = (1. - omega6) * RM[M121] + omega6 * RMeq[M121] ;
        RMcoll[M112] = (1. - omega6) * RM[M112] + omega6 * RMeq[M112] ;

        // Order 5
        RMcoll[M221] = (1. - omega7) * RM[M221] + omega7 * RMeq[M221] ;
        RMcoll[M212] = (1. - omega7) * RM[M212] + omega7 * RMeq[M212] ;
        RMcoll[M122] = (1. - omega7) * RM[M122] + omega7 * RMeq[M122] ;

        // Order 6
        RMcoll[M222] = (1. - omega8) * RM[M222] + omega8 * RMeq[M222] ;

        // Optimization based on symmetries between populations and their opposite counterpart        
        pop[F000] = rho *(1. - RMcoll[M200] - RMcoll[M020] - RMcoll[M002] + RMcoll[M220] + RMcoll[M202] + RMcoll[M022] - RMcoll[M222]);
        
        pop[FP00] = 0.5*rho * ( u[0] + RMcoll[M200] - RMcoll[M120] - RMcoll[M102] - RMcoll[M220] - RMcoll[M202] + RMcoll[M122] + RMcoll[M222]);
        pop[FM00] =     rho * (-u[0]                + RMcoll[M120] + RMcoll[M102]                               - RMcoll[M122])+ pop[FP00];

        pop[F0P0] = 0.5*rho * ( u[1] + RMcoll[M020] - RMcoll[M210] - RMcoll[M012] - RMcoll[M220] - RMcoll[M022] + RMcoll[M212] + RMcoll[M222]);
        pop[F0M0] =     rho * (-u[1]                + RMcoll[M210] + RMcoll[M012]                               - RMcoll[M212])+ pop[F0P0];

        pop[F00P] = 0.5*rho * ( u[2] + RMcoll[M002] - RMcoll[M201] - RMcoll[M021] - RMcoll[M202] - RMcoll[M022] + RMcoll[M221] + RMcoll[M222]);
        pop[F00M] =     rho * (-u[2]                + RMcoll[M201] + RMcoll[M021]                               - RMcoll[M221])+ pop[F00P];

        pop[FPP0] = 0.25*rho * ( RMcoll[M110] + RMcoll[M210] + RMcoll[M120] - RMcoll[M112] + RMcoll[M220] - RMcoll[M212] - RMcoll[M122] - RMcoll[M222]);
        pop[FMP0] =  0.5*rho * (-RMcoll[M110]                - RMcoll[M120] + RMcoll[M112]                               + RMcoll[M122])+ pop[FPP0];
        pop[FPM0] =  0.5*rho * (-RMcoll[M110] - RMcoll[M210]                + RMcoll[M112]                + RMcoll[M212]               )+ pop[FPP0];
        pop[FMM0] =  0.5*rho * (              - RMcoll[M210] - RMcoll[M120]                               + RMcoll[M212] + RMcoll[M122])+ pop[FPP0];

        pop[FP0P] = 0.25*rho * ( RMcoll[M101] + RMcoll[M201] + RMcoll[M102] - RMcoll[M121] + RMcoll[M202] - RMcoll[M221] - RMcoll[M122] - RMcoll[M222]);
        pop[FM0P] =  0.5*rho * (-RMcoll[M101]                - RMcoll[M102] + RMcoll[M121]                               + RMcoll[M122])+ pop[FP0P];
        pop[FP0M] =  0.5*rho * (-RMcoll[M101] - RMcoll[M201]                + RMcoll[M121]                + RMcoll[M221]               )+ pop[FP0P];
        pop[FM0M] =  0.5*rho * (              - RMcoll[M201] - RMcoll[M102]                               + RMcoll[M221] + RMcoll[M122])+ pop[FP0P];

        pop[F0PP] = 0.25*rho * ( RMcoll[M011] + RMcoll[M021] + RMcoll[M012] - RMcoll[M211] + RMcoll[M022] - RMcoll[M221] - RMcoll[M212] - RMcoll[M222]);
        pop[F0MP] =  0.5*rho * (-RMcoll[M011]                - RMcoll[M012] + RMcoll[M211]                               + RMcoll[M212])+ pop[F0PP];
        pop[F0PM] =  0.5*rho * (-RMcoll[M011] - RMcoll[M021]                + RMcoll[M211]                + RMcoll[M221]               )+ pop[F0PP];
        pop[F0MM] =  0.5*rho * (              - RMcoll[M021] - RMcoll[M012]                               + RMcoll[M221] + RMcoll[M212])+ pop[F0PP];

        pop[FPPP] = 0.125*rho * ( RMcoll[M111] + RMcoll[M211] + RMcoll[M121] + RMcoll[M112] + RMcoll[M221] + RMcoll[M212] + RMcoll[M122] + RMcoll[M222]);
        pop[FMPP] =  0.25*rho * (-RMcoll[M111]                - RMcoll[M121] - RMcoll[M112]                               - RMcoll[M122])+ pop[FPPP];
        pop[FPMP] =  0.25*rho * (-RMcoll[M111] - RMcoll[M211]                - RMcoll[M112]                - RMcoll[M212]               )+ pop[FPPP];
        pop[FPPM] =  0.25*rho * (-RMcoll[M111] - RMcoll[M211] - RMcoll[M121]                - RMcoll[M221]                              )+ pop[FPPP];
        pop[FMMP] =  0.25*rho * (              - RMcoll[M211] - RMcoll[M121]                               - RMcoll[M212] - RMcoll[M122])+ pop[FPPP];
        pop[FMPM] =  0.25*rho * (              - RMcoll[M211]                - RMcoll[M112] - RMcoll[M221]                - RMcoll[M122])+ pop[FPPP];
        pop[FPMM] =  0.25*rho * (                             - RMcoll[M121] - RMcoll[M112] - RMcoll[M221] - RMcoll[M212]               )+ pop[FPPP];
        pop[FMMM] =  0.25*rho * (-RMcoll[M111]                                              - RMcoll[M221] - RMcoll[M212] - RMcoll[M122])+ pop[FPPP];
    }

    auto streamingPull(int i, int iX, int iY, int iZ, std::array<double, 27>& pop)
    {
        if(periodic){
            for (int k = 0; k < 27; ++k) {
                int XX = (iX - c[k][0] + dim.nx) % dim.nx;
                int YY = (iY - c[k][1] + dim.ny) % dim.ny;
                int ZZ = (iZ - c[k][2] + dim.nz) % dim.nz;
                size_t nb = xyz_to_i(XX, YY, ZZ);
                CellType nbCellType = flag[nb];
                if (nbCellType == CellType::bounce_back) {
                    pop[k] = f(i, k) + f(nb, opp[k]);
                }
                else {
                    pop[k] = f(nb, opp[k]);
                }
            }
        }
        else {
            for (int k = 0; k < 27; ++k) {
                int XX = iX - c[k][0];
                int YY = iY - c[k][1];
                int ZZ = iZ - c[k][2];
                size_t nb = xyz_to_i(XX, YY, ZZ);
                CellType nbCellType = flag[nb];
                if (nbCellType == CellType::bounce_back) {
                    pop[k] = f(i, k) + f(nb, opp[k]);
                }
                else {
                    pop[k] = f(nb, opp[k]);
                }
            }
        }
    }

    auto streamingPush(int i, int iX, int iY, int iZ, std::array<double, 27>& pop)
    {
        if(periodic){
            for (int k = 0; k < 27; ++k) {
                int XX = (iX + c[k][0] + dim.nx) % dim.nx;
                int YY = (iY + c[k][1] + dim.ny) % dim.ny;
                int ZZ = (iZ + c[k][2] + dim.nz) % dim.nz;
                size_t nb = xyz_to_i(XX, YY, ZZ);
                CellType nbCellType = flag[nb];
                if (nbCellType == CellType::bounce_back) {
                    f(i, opp[k]) = pop[k] + f(nb, k);
                }
                else {
                    f(nb, k) = pop[k];
                }
            }
        }
        else {
            for (int k = 0; k < 27; ++k) {
                int XX = iX + c[k][0];
                int YY = iY + c[k][1];
                int ZZ = iZ + c[k][2];
                size_t nb = xyz_to_i(XX, YY, ZZ);
                CellType nbCellType = flag[nb];
                if (nbCellType == CellType::bounce_back) {
                    f(i, opp[k]) = pop[k] + f(nb, k);
                }
                else {
                    f(nb, k) = pop[k];
                }
            }
        }
    }

    void iterateRM(double& f0) {
        int i = &f0 - lattice;
        auto[iX, iY, iZ] = i_to_xyz(i);
        CellType cellType = flag[i];

        if (cellType == CellType::bulk) {
            std::array<double, 27> pop;

            streamingPull(i, iX, iY, iZ, pop);

/*          auto RM = computeRM(pop);
            auto RM = computeRMopt(pop);*/
            auto RM = computeRMopt2(pop);
            double rho = RM[M000];
            std::array<double, 3> u = {RM[M100], RM[M010], RM[M001]};
            auto RMeq = computeRMeq(u);

            collideRM(pop, rho, u, RM, RMeq);

            streamingPush(i, iX, iY, iZ, pop);
        }
    }

    void operator() (double& f0) {
        iterateRM(f0);
    }
};

} // namespace aa_soa_rm
