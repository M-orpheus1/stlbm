// *****************************************************************************
// STLBM SOFTWARE LIBRARY

// Copyright © 2020 University of Geneva
// Authors: Jonas Latt, Christophe Coreixas, Joël Beny
// Contact: Jonas.Latt@unige.ch

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// *****************************************************************************

#pragma once

#include "lbm_q27.h"
#include <vector>
#include <array>
#include <tuple>
#include <iostream>

namespace twopop_soa_chm {

struct LBM {
    using CellData = double;
    static size_t sizeOfLattice(size_t nelem) { return 2 * 27 * nelem; }

    CellData* lattice;
    CellType* flag;
    int* parity;
    std::array<int, 3>* c;
    int* opp;
    double* t;
    double omega1;
    Dim dim;
    LBModel model;
    bool periodic = false;
    double omegaBulk = 0.;
    double omega2 = omega1;
    double omega3 = omega1;
    double omega4 = omega1;
    double omega5 = omega1;
    double omega6 = omega1;
    double omega7 = omega1;
    double omega8 = omega1;

    auto i_to_xyz (int i) const {
        int iX = i / (dim.ny * dim.nz);
        int remainder = i % (dim.ny * dim.nz);
        int iY = remainder / dim.nz;
        int iZ = remainder % dim.nz;
        return std::make_tuple(iX, iY, iZ);
    };

    size_t xyz_to_i (int x, int y, int z) const {
        return z + dim.nz * (y + dim.ny * x);
    };

    double& f (int i, int k) {
        return lattice[k * dim.nelem + i];
    }

    double& fin (int i, int k) {
        return lattice[*parity * dim.npop + k * dim.nelem + i];
    }
    
    double& fout (int i, int k) {
        return lattice[(1 - *parity) * dim.npop + k * dim.nelem + i];
    }

    auto iniLattice (double& f0) {
        auto i = &f0 - lattice;
        for (int k = 0; k < 27; ++k) {
            fin(i, k) = t[k];
        }
    };

    auto iniLattice (double& f0, double rho, std::array<double, 3> const& u) {
        auto i = &f0 - lattice;
        double usqr = 1.5 * (u[0] * u[0] + u[1] * u[1] + u[2] * u[2]);
        for (int k = 0; k < 27; ++k) {
            double ck_u = c[k][0] * u[0] + c[k][1] * u[1] + c[k][2] * u[2];
            f(i, k) = rho * t[k] * (1. + 3. * ck_u + 4.5 * ck_u * ck_u - usqr);
        }
    };

    // Required for the nonregression based on computeEnergy() 
    auto macro (double const& f0) {
        auto i = &f0 - lattice;
        double X_M1 = fin(i, 0) + fin(i, 3) + fin(i, 4) + fin(i, 5) + fin(i, 6) + fin(i, 9) + fin(i,10) + fin(i,11) + fin(i,12);
        double X_P1 = fin(i,14) + fin(i,17) + fin(i,18) + fin(i,19) + fin(i,20) + fin(i,23) + fin(i,24) + fin(i,25) + fin(i,26);
        double X_0  = fin(i, 1) + fin(i, 2) + fin(i, 7) + fin(i, 8) + fin(i,13) + fin(i,15) + fin(i,16) + fin(i,21) + fin(i,22);

        double Y_M1 = fin(i, 1) + fin(i, 3) + fin(i, 7) + fin(i, 8) + fin(i, 9) + fin(i,10) + fin(i,18) + fin(i,25) + fin(i,26);
        double Y_P1 = fin(i,15) + fin(i,17) + fin(i,21) + fin(i,22) + fin(i,23) + fin(i,24) + fin(i, 4) + fin(i,11) + fin(i,12);

        double Z_M1 = fin(i, 2) + fin(i, 5) + fin(i, 7) + fin(i, 9) + fin(i,11) + fin(i,20) + fin(i,22) + fin(i,24) + fin(i,26);
        double Z_P1 = fin(i,16) + fin(i,19) + fin(i,21) + fin(i,23) + fin(i,25) + fin(i, 6) + fin(i, 8) + fin(i,10) + fin(i,12);

        double rho = X_M1 + X_P1 + X_0;
        std::array<double, 3> u{ (X_P1 - X_M1) / rho, (Y_P1 - Y_M1) / rho, (Z_P1 - Z_M1) / rho };
        return std::make_pair(rho, u);
    }
 
    // Naive way to compute CHMs and macros
    auto computeCHM(double const& f0) {
        auto i = &f0 - lattice;
        std::array<double, 27> CHM;
        std::fill(CHM.begin(), CHM.end(), 0.);
        double cMux, cMuy, cMuz;
        double Hxx, Hyy, Hzz;
        double cs2 = 1./3.;

        // Preliminary computation of density and velocity vector
        double X_M1 = fin(i, 0) + fin(i, 3) + fin(i, 4) + fin(i, 5) + fin(i, 6) + fin(i, 9) + fin(i,10) + fin(i,11) + fin(i,12);
        double X_P1 = fin(i,14) + fin(i,17) + fin(i,18) + fin(i,19) + fin(i,20) + fin(i,23) + fin(i,24) + fin(i,25) + fin(i,26);
        double X_0  = fin(i, 1) + fin(i, 2) + fin(i, 7) + fin(i, 8) + fin(i,13) + fin(i,15) + fin(i,16) + fin(i,21) + fin(i,22);

        double Y_M1 = fin(i, 1) + fin(i, 3) + fin(i, 7) + fin(i, 8) + fin(i, 9) + fin(i,10) + fin(i,18) + fin(i,25) + fin(i,26);
        double Y_P1 = fin(i,15) + fin(i,17) + fin(i,21) + fin(i,22) + fin(i,23) + fin(i,24) + fin(i, 4) + fin(i,11) + fin(i,12);

        double Z_M1 = fin(i, 2) + fin(i, 5) + fin(i, 7) + fin(i, 9) + fin(i,11) + fin(i,20) + fin(i,22) + fin(i,24) + fin(i,26);
        double Z_P1 = fin(i,16) + fin(i,19) + fin(i,21) + fin(i,23) + fin(i,25) + fin(i, 6) + fin(i, 8) + fin(i,10) + fin(i,12);

        double rho = X_M1 + X_P1 + X_0;
        std::array<double, 3> u{ (X_P1 - X_M1) / rho, (Y_P1 - Y_M1) / rho, (Z_P1 - Z_M1) / rho };
        CHM[M000] = rho;
        CHM[M100] = u[0];  // CHM is used to store the velocity component 
        CHM[M010] = u[1];  // CHM is used to store the velocity component
        CHM[M001] = u[2];  // CHM is used to store the velocity component

        // Naive computation of CHMs
        for (int k = 0; k<27; ++k) {
            cMux = c[k][0]- u[0];
            cMuy = c[k][1]- u[1];
            cMuz = c[k][2]- u[2];
            Hxx = cMux * cMux - cs2;
            Hyy = cMuy * cMuy - cs2;
            Hzz = cMuz * cMuz - cs2;

            // Order 2
            CHM[M200] += Hxx * fin(i,k);
            CHM[M020] += Hyy * fin(i,k);
            CHM[M002] += Hzz * fin(i,k);
            CHM[M110] += cMux * cMuy * fin(i,k);
            CHM[M101] += cMux * cMuz * fin(i,k);
            CHM[M011] += cMuy * cMuz * fin(i,k);
            // Order 3
            CHM[M210] += Hxx * cMuy * fin(i,k);
            CHM[M201] += Hxx * cMuz * fin(i,k);
            CHM[M021] += Hyy * cMuz * fin(i,k);
            CHM[M120] += cMux * Hyy * fin(i,k);
            CHM[M102] += cMux * Hzz * fin(i,k);
            CHM[M012] += cMuy * Hzz * fin(i,k);
            CHM[M111] += cMux * cMuy * cMuz * fin(i,k);
            // Order 4
            CHM[M220] += Hxx * Hyy * fin(i,k);
            CHM[M202] += Hxx * Hzz * fin(i,k);
            CHM[M022] += Hyy * Hzz * fin(i,k);
            CHM[M211] += Hxx * cMuy * cMuz * fin(i,k);
            CHM[M121] += cMux * Hyy * cMuz * fin(i,k);
            CHM[M112] += cMux * cMuy * Hzz * fin(i,k);
            // Order 5
            CHM[M221] += Hxx * Hyy * cMuz * fin(i,k);
            CHM[M212] += Hxx * cMuy * Hzz * fin(i,k);
            CHM[M122] += cMux * Hyy * Hzz * fin(i,k);
            // Order 6
            CHM[M222] += Hxx * Hyy * Hzz * fin(i,k);
        }

        double invRho = 1./rho;
        for (int k = 4; k<27; ++k) {
            CHM[k] *= invRho;
        }

        return CHM;
    }

    // First optimization (loop unrolling)
    auto computeCHMopt(double const& f0) {
        auto i = &f0 - lattice;
        std::array<double, 27> CHM;
        std::fill(CHM.begin(), CHM.end(), 0.);
        // Order 0
        CHM[M000] =  fin(i, 0) + fin(i, 1) + fin(i, 2) + fin(i, 3) + fin(i, 4) + fin(i, 5) + fin(i, 6) + fin(i, 7) + fin(i, 8) + fin(i, 9) + fin(i,10) + fin(i,11) + fin(i,12) + fin(i,13) + fin(i,14) + fin(i,15) + fin(i,16) + fin(i,17) + fin(i,18) + fin(i,19) + fin(i,20) + fin(i,21) + fin(i,22) + fin(i,23) + fin(i,24) + fin(i,25) + fin(i,26);
        double invRho = 1./CHM[M000];
        // Order 1
        CHM[M100] = invRho * (- fin(i, 0) - fin(i, 3) - fin(i, 4) - fin(i, 5) - fin(i, 6) - fin(i, 9) - fin(i,10) - fin(i,11) - fin(i,12) + fin(i,14) + fin(i,17) + fin(i,18) + fin(i,19) + fin(i,20) + fin(i,23) + fin(i,24) + fin(i,25) + fin(i,26));
        CHM[M010] = invRho * (- fin(i, 1) - fin(i, 3) + fin(i, 4) - fin(i, 7) - fin(i, 8) - fin(i, 9) - fin(i,10) + fin(i,11) + fin(i,12) + fin(i,15) + fin(i,17) - fin(i,18) + fin(i,21) + fin(i,22) + fin(i,23) + fin(i,24) - fin(i,25) - fin(i,26));
        CHM[M001] = invRho * (- fin(i, 2) - fin(i, 5) + fin(i, 6) - fin(i, 7) + fin(i, 8) - fin(i, 9) + fin(i,10) - fin(i,11) + fin(i,12) + fin(i,16) + fin(i,19) - fin(i,20) + fin(i,21) - fin(i,22) + fin(i,23) - fin(i,24) + fin(i,25) - fin(i,26));
        // Order 2
        CHM[M200] = invRho * (  fin(i, 0) + fin(i, 3) + fin(i, 4) + fin(i, 5) + fin(i, 6) + fin(i, 9) + fin(i,10) + fin(i,11) + fin(i,12) + fin(i,14) + fin(i,17) + fin(i,18) + fin(i,19) + fin(i,20) + fin(i,23) + fin(i,24) + fin(i,25) + fin(i,26));
        CHM[M020] = invRho * (  fin(i, 1) + fin(i, 3) + fin(i, 4) + fin(i, 7) + fin(i, 8) + fin(i, 9) + fin(i,10) + fin(i,11) + fin(i,12) + fin(i,15) + fin(i,17) + fin(i,18) + fin(i,21) + fin(i,22) + fin(i,23) + fin(i,24) + fin(i,25) + fin(i,26));
        CHM[M002] = invRho * (  fin(i, 2) + fin(i, 5) + fin(i, 6) + fin(i, 7) + fin(i, 8) + fin(i, 9) + fin(i,10) + fin(i,11) + fin(i,12) + fin(i,16) + fin(i,19) + fin(i,20) + fin(i,21) + fin(i,22) + fin(i,23) + fin(i,24) + fin(i,25) + fin(i,26));
        CHM[M110] = invRho * (  fin(i, 3) - fin(i, 4) + fin(i, 9) + fin(i,10) - fin(i,11) - fin(i,12) + fin(i,17) - fin(i,18) + fin(i,23) + fin(i,24) - fin(i,25) - fin(i,26));
        CHM[M101] = invRho * (  fin(i, 5) - fin(i, 6) + fin(i, 9) - fin(i,10) + fin(i,11) - fin(i,12) + fin(i,19) - fin(i,20) + fin(i,23) - fin(i,24) + fin(i,25) - fin(i,26));
        CHM[M011] = invRho * (  fin(i, 7) - fin(i, 8) + fin(i, 9) - fin(i,10) - fin(i,11) + fin(i,12) + fin(i,21) - fin(i,22) + fin(i,23) - fin(i,24) - fin(i,25) + fin(i,26));
        // Order 3
        CHM[M210] = invRho * (- fin(i, 3) + fin(i, 4) - fin(i, 9) - fin(i,10) + fin(i,11) + fin(i,12) + fin(i,17) - fin(i,18) + fin(i,23) + fin(i,24) - fin(i,25) - fin(i,26));
        CHM[M201] = invRho * (- fin(i, 5) + fin(i, 6) - fin(i, 9) + fin(i,10) - fin(i,11) + fin(i,12) + fin(i,19) - fin(i,20) + fin(i,23) - fin(i,24) + fin(i,25) - fin(i,26));
        CHM[M021] = invRho * (- fin(i, 7) + fin(i, 8) - fin(i, 9) + fin(i,10) - fin(i,11) + fin(i,12) + fin(i,21) - fin(i,22) + fin(i,23) - fin(i,24) + fin(i,25) - fin(i,26));
        CHM[M120] = invRho * (- fin(i, 3) - fin(i, 4) - fin(i, 9) - fin(i,10) - fin(i,11) - fin(i,12) + fin(i,17) + fin(i,18) + fin(i,23) + fin(i,24) + fin(i,25) + fin(i,26));
        CHM[M102] = invRho * (- fin(i, 5) - fin(i, 6) - fin(i, 9) - fin(i,10) - fin(i,11) - fin(i,12) + fin(i,19) + fin(i,20) + fin(i,23) + fin(i,24) + fin(i,25) + fin(i,26));
        CHM[M012] = invRho * (- fin(i, 7) - fin(i, 8) - fin(i, 9) - fin(i,10) + fin(i,11) + fin(i,12) + fin(i,21) + fin(i,22) + fin(i,23) + fin(i,24) - fin(i,25) - fin(i,26));
        CHM[M111] = invRho * (- fin(i, 9) + fin(i,10) + fin(i,11) - fin(i,12) + fin(i,23) - fin(i,24) - fin(i,25) + fin(i,26));
        // Order 4
        CHM[M220] = invRho * (  fin(i, 3) + fin(i, 4) + fin(i, 9) + fin(i,10) + fin(i,11) + fin(i,12) + fin(i,17) + fin(i,18) + fin(i,23) + fin(i,24) + fin(i,25) + fin(i,26));
        CHM[M202] = invRho * (  fin(i, 5) + fin(i, 6) + fin(i, 9) + fin(i,10) + fin(i,11) + fin(i,12) + fin(i,19) + fin(i,20) + fin(i,23) + fin(i,24) + fin(i,25) + fin(i,26));
        CHM[M022] = invRho * (  fin(i, 7) + fin(i, 8) + fin(i, 9) + fin(i,10) + fin(i,11) + fin(i,12) + fin(i,21) + fin(i,22) + fin(i,23) + fin(i,24) + fin(i,25) + fin(i,26));
        CHM[M211] = invRho * (  fin(i, 9) - fin(i,10) - fin(i,11) + fin(i,12) + fin(i,23) - fin(i,24) - fin(i,25) + fin(i,26));
        CHM[M121] = invRho * (  fin(i, 9) - fin(i,10) + fin(i,11) - fin(i,12) + fin(i,23) - fin(i,24) + fin(i,25) - fin(i,26));
        CHM[M112] = invRho * (  fin(i, 9) + fin(i,10) - fin(i,11) - fin(i,12) + fin(i,23) + fin(i,24) - fin(i,25) - fin(i,26));
        // Order 5
        CHM[M221] = invRho * (- fin(i, 9) + fin(i,10) - fin(i,11) + fin(i,12) + fin(i,23) - fin(i,24) + fin(i,25) - fin(i,26));
        CHM[M212] = invRho * (- fin(i, 9) - fin(i,10) + fin(i,11) + fin(i,12) + fin(i,23) + fin(i,24) - fin(i,25) - fin(i,26));
        CHM[M122] = invRho * (- fin(i, 9) - fin(i,10) - fin(i,11) - fin(i,12) + fin(i,23) + fin(i,24) + fin(i,25) + fin(i,26));
        // Order 6
        CHM[M222] = invRho * (  fin(i, 9) + fin(i,10) + fin(i,11) + fin(i,12) + fin(i,23) + fin(i,24) + fin(i,25) + fin(i,26));

        // Compute CMs from RMs using binomial formulas
        double ux   = CHM[M100];
        double uy   = CHM[M010];
        double uz   = CHM[M001];
        double ux2  = CHM[M100]*CHM[M100];
        double uy2  = CHM[M010]*CHM[M010];
        double uz2  = CHM[M001]*CHM[M001];
        double uxyz = CHM[M100]*CHM[M010]*CHM[M001];

        CHM[M200] -= (ux2);
        CHM[M020] -= (uy2);
        CHM[M002] -= (uz2);
        
        CHM[M110] -= (ux*uy);
        CHM[M101] -= (ux*uz);
        CHM[M011] -= (uy*uz);

        CHM[M210] -= (uy*CHM[M200] + 2.*ux*CHM[M110] + ux2*uy);
        CHM[M201] -= (uz*CHM[M200] + 2.*ux*CHM[M101] + ux2*uz);
        CHM[M021] -= (uz*CHM[M020] + 2.*uy*CHM[M011] + uy2*uz);
        CHM[M120] -= (ux*CHM[M020] + 2.*uy*CHM[M110] + ux*uy2);
        CHM[M102] -= (ux*CHM[M002] + 2.*uz*CHM[M101] + ux*uz2);
        CHM[M012] -= (uy*CHM[M002] + 2.*uz*CHM[M011] + uy*uz2);
        
        CHM[M111] -= (uz*CHM[M110] + uy*CHM[M101] + ux*CHM[M011] + uxyz );

        CHM[M220] -= (2.*uy*CHM[M210] + 2.*ux*CHM[M120] + uy2*CHM[M200] + ux2*CHM[M020] + 4.*ux*uy*CHM[M110] + ux2*uy2);
        CHM[M202] -= (2.*uz*CHM[M201] + 2.*ux*CHM[M102] + uz2*CHM[M200] + ux2*CHM[M002] + 4.*ux*uz*CHM[M101] + ux2*uz2);
        CHM[M022] -= (2.*uz*CHM[M021] + 2.*uy*CHM[M012] + uz2*CHM[M020] + uy2*CHM[M002] + 4.*uy*uz*CHM[M011] + uy2*uz2);

        CHM[M211] -= (uz*CHM[M210] + uy*CHM[M201] + 2.*ux*CHM[M111] + uy*uz*CHM[M200] + 2.*ux*uz*CHM[M110] + 2.*ux*uy*CHM[M101] + ux2*CHM[M011] + ux2*uy*uz);
        CHM[M121] -= (uz*CHM[M120] + ux*CHM[M021] + 2.*uy*CHM[M111] + ux*uz*CHM[M020] + 2.*uy*uz*CHM[M110] + 2.*ux*uy*CHM[M011] + uy2*CHM[M101] + ux*uy2*uz);
        CHM[M112] -= (uy*CHM[M102] + ux*CHM[M012] + 2.*uz*CHM[M111] + ux*uy*CHM[M002] + 2.*uy*uz*CHM[M101] + 2.*ux*uz*CHM[M011] + uz2*CHM[M110] + ux*uy*uz2);

        CHM[M221] -= (uz*CHM[M220] + 2.*uy*CHM[M211] + 2.*ux*CHM[M121] + 2.*uy*uz*CHM[M210] + uy2*CHM[M201] + ux2*CHM[M021] + 2.*ux*uz*CHM[M120] + 4.*ux*uy*CHM[M111] + uy2*uz*CHM[M200] + ux2*uz*CHM[M020] + 4.*uxyz*CHM[M110] + 2.*ux*uy2*CHM[M101] + 2.*ux2*uy*CHM[M011] + ux2*uy2*uz);
        CHM[M212] -= (uy*CHM[M202] + 2.*uz*CHM[M211] + 2.*ux*CHM[M112] + 2.*uy*uz*CHM[M201] + uz2*CHM[M210] + ux2*CHM[M012] + 2.*ux*uy*CHM[M102] + 4.*ux*uz*CHM[M111] + uy*uz2*CHM[M200] + ux2*uy*CHM[M002] + 4.*uxyz*CHM[M101] + 2.*ux*uz2*CHM[M110] + 2.*ux2*uz*CHM[M011] + ux2*uy*uz2);
        CHM[M122] -= (ux*CHM[M022] + 2.*uz*CHM[M121] + 2.*uy*CHM[M112] + 2.*ux*uz*CHM[M021] + uz2*CHM[M120] + uy2*CHM[M102] + 2.*ux*uy*CHM[M012] + 4.*uy*uz*CHM[M111] + ux*uz2*CHM[M020] + ux*uy2*CHM[M002] + 4.*uxyz*CHM[M011] + 2.*uy*uz2*CHM[M110] + 2.*uy2*uz*CHM[M101] + ux*uy2*uz2);

        CHM[M222] -= (2.*uz*CHM[M221] + 2.*uy*CHM[M212] + 2.*ux*CHM[M122] + uz2*CHM[M220] + uy2*CHM[M202] + ux2*CHM[M022] + 4.*uy*uz*CHM[M211] + 4.*ux*uz*CHM[M121] + 4.*ux*uy*CHM[M112] + 2.*uy*uz2*CHM[M210] + 2.*uy2*uz*CHM[M201] + 2.*ux2*uz*CHM[M021] + 2.*ux*uz2*CHM[M120] + 2.*ux*uy2*CHM[M102] + 2.*ux2*uy*CHM[M012] + 8.*uxyz*CHM[M111] + uy2*uz2*CHM[M200] + ux2*uz2*CHM[M020] + ux2*uy2*CHM[M002] + 4.*ux*uy*uz2*CHM[M110] + 4.*ux*uy2*uz*CHM[M101] + 4.*ux2*uy*uz*CHM[M011] + ux2*uy2*uz2);

        // Compute central Hermite moments from central moments
        // I further assumed that 1st-order CHM = 0 since they
        // are used to store velocity components
        double cs2 = 1./3.;
        double cs4 = cs2*cs2;

        CHM[M200] -= (cs2);
        CHM[M020] -= (cs2);
        CHM[M002] -= (cs2);
        
        CHM[M220] -= (cs2*(CHM[M200] + CHM[M020]) + cs4);
        CHM[M202] -= (cs2*(CHM[M200] + CHM[M002]) + cs4);
        CHM[M022] -= (cs2*(CHM[M020] + CHM[M002]) + cs4);

        CHM[M211] -= (cs2*CHM[M011]);
        CHM[M121] -= (cs2*CHM[M101]);
        CHM[M112] -= (cs2*CHM[M110]);

        CHM[M221] -= (cs2*(CHM[M201] + CHM[M021]));
        CHM[M212] -= (cs2*(CHM[M210] + CHM[M012]));
        CHM[M122] -= (cs2*(CHM[M120] + CHM[M102]));

        CHM[M222] -= (cs2*(CHM[M220] + CHM[M202] + CHM[M022]) + cs4*(CHM[M200] + CHM[M020] + CHM[M002]) + cs2*cs4);

        return CHM;
    }

    // Most optimized version (loop unrolling + redundant terms)
    // Further pre-computing terms is counter-productive
    auto computeCHMopt2(double const& f0) {
        auto i = &f0 - lattice;
        std::array<double, 27> CHM;
        std::fill(CHM.begin(), CHM.end(), 0.);

        double A1 = fin(i, 9) + fin(i,10) + fin(i,11) + fin(i,12);
        double A2 = fin(i,23) + fin(i,24) + fin(i,25) + fin(i,26);
        double A3 = fin(i, 9) + fin(i,10) - fin(i,11) - fin(i,12);
        double A4 = fin(i,23) + fin(i,24) - fin(i,25) - fin(i,26);
        double A5 = fin(i, 9) - fin(i,10) + fin(i,11) - fin(i,12);
        double A6 = fin(i,23) - fin(i,24) + fin(i,25) - fin(i,26);
        double A7 = fin(i, 9) - fin(i,10) - fin(i,11) + fin(i,12);
        double A8 = fin(i,23) - fin(i,24) - fin(i,25) + fin(i,26);

        double X_M1 = fin(i, 0) + fin(i, 3) + fin(i, 4) + fin(i, 5) + fin(i, 6) + A1;
        double X_P1 = fin(i,14) + fin(i,17) + fin(i,18) + fin(i,19) + fin(i,20) + A2;
        double X_0  = fin(i, 1) + fin(i, 2) + fin(i, 7) + fin(i, 8) + fin(i,13) + fin(i,15) + fin(i,16) + fin(i,21) + fin(i,22);

        double Y_M1 = fin(i, 1) + fin(i, 3) + fin(i, 7) + fin(i, 8) + fin(i, 9) + fin(i,10) + fin(i,18) + fin(i,25) + fin(i,26);
        double Y_P1 = fin(i,15) + fin(i,17) + fin(i,21) + fin(i,22) + fin(i,23) + fin(i,24) + fin(i, 4) + fin(i,11) + fin(i,12);

        double Z_M1 = fin(i, 2) + fin(i, 5) + fin(i, 7) + fin(i, 9) + fin(i,11) + fin(i,20) + fin(i,22) + fin(i,24) + fin(i,26);
        double Z_P1 = fin(i,16) + fin(i,19) + fin(i,21) + fin(i,23) + fin(i,25) + fin(i, 6) + fin(i, 8) + fin(i,10) + fin(i,12);

        // Order 0
        CHM[M000] = X_M1 + X_P1 + X_0;
        double invRho = 1. / CHM[M000];

        // Order 6
        CHM[M222] = invRho * (  A1 + A2);
        // Order 5
        CHM[M221] = invRho * (- A5 + A6);
        CHM[M212] = invRho * (- A3 + A4);
        CHM[M122] = invRho * (- A1 + A2);
        // Order 4
        CHM[M220] = invRho * (  fin(i, 3) + fin(i, 4) + fin(i,17) + fin(i,18)) + CHM[M222];
        CHM[M202] = invRho * (  fin(i, 5) + fin(i, 6) + fin(i,19) + fin(i,20)) + CHM[M222];
        CHM[M022] = invRho * (  fin(i, 7) + fin(i, 8) + fin(i,21) + fin(i,22)) + CHM[M222];
        CHM[M211] = invRho * (  A7 + A8);
        CHM[M121] = invRho * (  A5 + A6);
        CHM[M112] = invRho * (  A3 + A4);
        // Order 3
        CHM[M210] = invRho * (- fin(i, 3) + fin(i, 4) + fin(i,17) - fin(i,18)) + CHM[M212];
        CHM[M201] = invRho * (- fin(i, 5) + fin(i, 6) + fin(i,19) - fin(i,20)) + CHM[M221];
        CHM[M021] = invRho * (- fin(i, 7) + fin(i, 8) + fin(i,21) - fin(i,22)) + CHM[M221];
        CHM[M120] = invRho * (- fin(i, 3) - fin(i, 4) + fin(i,17) + fin(i,18)) + CHM[M122];
        CHM[M102] = invRho * (- fin(i, 5) - fin(i, 6) + fin(i,19) + fin(i,20)) + CHM[M122];
        CHM[M012] = invRho * (- fin(i, 7) - fin(i, 8) + fin(i,21) + fin(i,22)) + CHM[M212];
        CHM[M111] = invRho * (- A7 + A8);
        // Order 2
        CHM[M200] = invRho * (X_P1 + X_M1);
        CHM[M020] = invRho * (Y_P1 + Y_M1); 
        CHM[M002] = invRho * (Z_P1 + Z_M1);
        CHM[M110] = invRho * (  fin(i, 3) - fin(i, 4) + fin(i,17) - fin(i,18)) + CHM[M112];
        CHM[M101] = invRho * (  fin(i, 5) - fin(i, 6) + fin(i,19) - fin(i,20)) + CHM[M121];
        CHM[M011] = invRho * (  fin(i, 7) - fin(i, 8) + fin(i,21) - fin(i,22)) + CHM[M211];
        // Order 1
        CHM[M100] = invRho * (X_P1 - X_M1);
        CHM[M010] = invRho * (Y_P1 - Y_M1); 
        CHM[M001] = invRho * (Z_P1 - Z_M1);

        // Compute CMs from RMs using binomial formulas
        double ux   = CHM[M100];
        double uy   = CHM[M010];
        double uz   = CHM[M001];
        double ux2  = CHM[M100]*CHM[M100];
        double uy2  = CHM[M010]*CHM[M010];
        double uz2  = CHM[M001]*CHM[M001];
        double uxyz = CHM[M100]*CHM[M010]*CHM[M001];

        CHM[M200] -= (ux2);
        CHM[M020] -= (uy2);
        CHM[M002] -= (uz2);
        
        CHM[M110] -= (ux*uy);
        CHM[M101] -= (ux*uz);
        CHM[M011] -= (uy*uz);

        CHM[M210] -= (uy*CHM[M200] + 2.*ux*CHM[M110] + ux2*uy);
        CHM[M201] -= (uz*CHM[M200] + 2.*ux*CHM[M101] + ux2*uz);
        CHM[M021] -= (uz*CHM[M020] + 2.*uy*CHM[M011] + uy2*uz);
        CHM[M120] -= (ux*CHM[M020] + 2.*uy*CHM[M110] + ux*uy2);
        CHM[M102] -= (ux*CHM[M002] + 2.*uz*CHM[M101] + ux*uz2);
        CHM[M012] -= (uy*CHM[M002] + 2.*uz*CHM[M011] + uy*uz2);
        
        CHM[M111] -= (uz*CHM[M110] + uy*CHM[M101] + ux*CHM[M011] + uxyz );

        CHM[M220] -= (2.*uy*CHM[M210] + 2.*ux*CHM[M120] + uy2*CHM[M200] + ux2*CHM[M020] + 4.*ux*uy*CHM[M110] + ux2*uy2);
        CHM[M202] -= (2.*uz*CHM[M201] + 2.*ux*CHM[M102] + uz2*CHM[M200] + ux2*CHM[M002] + 4.*ux*uz*CHM[M101] + ux2*uz2);
        CHM[M022] -= (2.*uz*CHM[M021] + 2.*uy*CHM[M012] + uz2*CHM[M020] + uy2*CHM[M002] + 4.*uy*uz*CHM[M011] + uy2*uz2);

        CHM[M211] -= (uz*CHM[M210] + uy*CHM[M201] + 2.*ux*CHM[M111] + uy*uz*CHM[M200] + 2.*ux*uz*CHM[M110] + 2.*ux*uy*CHM[M101] + ux2*CHM[M011] + ux2*uy*uz);
        CHM[M121] -= (uz*CHM[M120] + ux*CHM[M021] + 2.*uy*CHM[M111] + ux*uz*CHM[M020] + 2.*uy*uz*CHM[M110] + 2.*ux*uy*CHM[M011] + uy2*CHM[M101] + ux*uy2*uz);
        CHM[M112] -= (uy*CHM[M102] + ux*CHM[M012] + 2.*uz*CHM[M111] + ux*uy*CHM[M002] + 2.*uy*uz*CHM[M101] + 2.*ux*uz*CHM[M011] + uz2*CHM[M110] + ux*uy*uz2);

        CHM[M221] -= (uz*CHM[M220] + 2.*uy*CHM[M211] + 2.*ux*CHM[M121] + 2.*uy*uz*CHM[M210] + uy2*CHM[M201] + ux2*CHM[M021] + 2.*ux*uz*CHM[M120] + 4.*ux*uy*CHM[M111] + uy2*uz*CHM[M200] + ux2*uz*CHM[M020] + 4.*uxyz*CHM[M110] + 2.*ux*uy2*CHM[M101] + 2.*ux2*uy*CHM[M011] + ux2*uy2*uz);
        CHM[M212] -= (uy*CHM[M202] + 2.*uz*CHM[M211] + 2.*ux*CHM[M112] + 2.*uy*uz*CHM[M201] + uz2*CHM[M210] + ux2*CHM[M012] + 2.*ux*uy*CHM[M102] + 4.*ux*uz*CHM[M111] + uy*uz2*CHM[M200] + ux2*uy*CHM[M002] + 4.*uxyz*CHM[M101] + 2.*ux*uz2*CHM[M110] + 2.*ux2*uz*CHM[M011] + ux2*uy*uz2);
        CHM[M122] -= (ux*CHM[M022] + 2.*uz*CHM[M121] + 2.*uy*CHM[M112] + 2.*ux*uz*CHM[M021] + uz2*CHM[M120] + uy2*CHM[M102] + 2.*ux*uy*CHM[M012] + 4.*uy*uz*CHM[M111] + ux*uz2*CHM[M020] + ux*uy2*CHM[M002] + 4.*uxyz*CHM[M011] + 2.*uy*uz2*CHM[M110] + 2.*uy2*uz*CHM[M101] + ux*uy2*uz2);

        CHM[M222] -= (2.*uz*CHM[M221] + 2.*uy*CHM[M212] + 2.*ux*CHM[M122] + uz2*CHM[M220] + uy2*CHM[M202] + ux2*CHM[M022] + 4.*uy*uz*CHM[M211] + 4.*ux*uz*CHM[M121] + 4.*ux*uy*CHM[M112] + 2.*uy*uz2*CHM[M210] + 2.*uy2*uz*CHM[M201] + 2.*ux2*uz*CHM[M021] + 2.*ux*uz2*CHM[M120] + 2.*ux*uy2*CHM[M102] + 2.*ux2*uy*CHM[M012] + 8.*uxyz*CHM[M111] + uy2*uz2*CHM[M200] + ux2*uz2*CHM[M020] + ux2*uy2*CHM[M002] + 4.*ux*uy*uz2*CHM[M110] + 4.*ux*uy2*uz*CHM[M101] + 4.*ux2*uy*uz*CHM[M011] + ux2*uy2*uz2);

        // Compute central Hermite moments from central moments
        // I further assumed that 1st-order CHM = 0 since they
        // are used to store velocity components
        double cs2 = 1./3.;
        double cs4 = cs2*cs2;

        CHM[M200] -= (cs2);
        CHM[M020] -= (cs2);
        CHM[M002] -= (cs2);
        
        CHM[M220] -= (cs2*(CHM[M200] + CHM[M020]) + cs4);
        CHM[M202] -= (cs2*(CHM[M200] + CHM[M002]) + cs4);
        CHM[M022] -= (cs2*(CHM[M020] + CHM[M002]) + cs4);

        CHM[M211] -= (cs2*CHM[M011]);
        CHM[M121] -= (cs2*CHM[M101]);
        CHM[M112] -= (cs2*CHM[M110]);

        CHM[M221] -= (cs2*(CHM[M201] + CHM[M021]));
        CHM[M212] -= (cs2*(CHM[M210] + CHM[M012]));
        CHM[M122] -= (cs2*(CHM[M120] + CHM[M102]));

        CHM[M222] -= (cs2*(CHM[M220] + CHM[M202] + CHM[M022]) + cs4*(CHM[M200] + CHM[M020] + CHM[M002]) + cs2*cs4);

        return CHM;
    }

    // The function below has only an educational purpose since it is discarded
    // during the compilation because CHMeq are not used in collideCHM()
    auto computeCHMeq() {

        std::array<double, 27> CHMeq;
        // Order 2
        CHMeq[M200] = 0.;
        CHMeq[M020] = 0.;
        CHMeq[M002] = 0.;
        CHMeq[M110] = 0.;
        CHMeq[M101] = 0.;
        CHMeq[M011] = 0.;
        // Order 3
        CHMeq[M210] = 0.;
        CHMeq[M201] = 0.;
        CHMeq[M021] = 0.;
        CHMeq[M120] = 0.;
        CHMeq[M102] = 0.;
        CHMeq[M012] = 0.;
        CHMeq[M111] = 0.;
        // Order 4
        CHMeq[M220] = 0.;
        CHMeq[M202] = 0.;
        CHMeq[M022] = 0.;
        CHMeq[M211] = 0.;
        CHMeq[M121] = 0.;
        CHMeq[M112] = 0.;
        // Order 5
        CHMeq[M221] = 0.;
        CHMeq[M212] = 0.;
        CHMeq[M122] = 0.;
        // Order 6
        CHMeq[M222] = 0.;

        return CHMeq;
    }

    auto collideAndStreamCHM(int i, int iX, int iY, int iZ, double rho, std::array<double, 3> const& u, 
                             std::array<double, 27> const& CHM, std::array<double, 27> const& CHMeq)
    {
        double cs2 = 1./3.;
        double cs4 = cs2*cs2;
        double ux2  = u[0]*u[0];
        double uy2  = u[1]*u[1];
        double uz2  = u[2]*u[2];
        double uxyz = u[0]*u[1]*u[2];

        // Post-collision moments.
        std::array<double, 27> RMcoll;
        std::array<double, 27> HMcoll;
        std::array<double, 27> CHMcoll;

        // Collision in the central Hermite moment space (CHMeq are replaced by their values)
        // Order 2
        if (omegaBulk == 0.) { // Don't adjust bulk viscosity
            CHMcoll[M200] = (1. - omega1) * CHM[M200];
            CHMcoll[M020] = (1. - omega1) * CHM[M020];
            CHMcoll[M002] = (1. - omega1) * CHM[M002];
        }
        else { // Adjust bulk viscosity
            const double omegaBulk = omega1;
            const double omegaMinus = (omegaBulk - omega1)/3.; // Notation used by Fei
            const double omegaPlus  = omegaMinus + omega1;     // Notation used by Fei
            CHMcoll[M200] = CHM[M200] - omegaPlus  * (CHM[M200]) - omegaMinus * (CHM[M020]) - omegaMinus * (CHM[M002]) ;
            CHMcoll[M020] = CHM[M020] - omegaMinus * (CHM[M200]) - omegaPlus  * (CHM[M020]) - omegaMinus * (CHM[M002]) ;
            CHMcoll[M002] = CHM[M002] - omegaMinus * (CHM[M200]) - omegaMinus * (CHM[M020]) - omegaPlus  * (CHM[M002]) ;
        }

        CHMcoll[M110] = (1. - omega2) * CHM[M110];
        CHMcoll[M101] = (1. - omega2) * CHM[M101];
        CHMcoll[M011] = (1. - omega2) * CHM[M011];
        // Order 3
        CHMcoll[M210] = (1. - omega3) * CHM[M210];
        CHMcoll[M201] = (1. - omega3) * CHM[M201];
        CHMcoll[M021] = (1. - omega3) * CHM[M021];
        CHMcoll[M120] = (1. - omega3) * CHM[M120];
        CHMcoll[M102] = (1. - omega3) * CHM[M102];
        CHMcoll[M012] = (1. - omega3) * CHM[M012];
        CHMcoll[M111] = (1. - omega4) * CHM[M111];
        // Order 4
        CHMcoll[M220] = (1. - omega5) * CHM[M220];
        CHMcoll[M202] = (1. - omega5) * CHM[M202];
        CHMcoll[M022] = (1. - omega5) * CHM[M022];
        CHMcoll[M211] = (1. - omega6) * CHM[M211];
        CHMcoll[M121] = (1. - omega6) * CHM[M121];
        CHMcoll[M112] = (1. - omega6) * CHM[M112];
        // Order 5
        CHMcoll[M221] = (1. - omega7) * CHM[M221];
        CHMcoll[M212] = (1. - omega7) * CHM[M212];
        CHMcoll[M122] = (1. - omega7) * CHM[M122];
        // Order 6
        CHMcoll[M222] = (1. - omega8) * CHM[M222];

        // Come back to HMcoll using binomial formulas
        HMcoll[M200] = CHMcoll[M200] + ux2;
        HMcoll[M020] = CHMcoll[M020] + uy2;
        HMcoll[M002] = CHMcoll[M002] + uz2;
        
        HMcoll[M110] = CHMcoll[M110] + u[0]*u[1];
        HMcoll[M101] = CHMcoll[M101] + u[0]*u[2];
        HMcoll[M011] = CHMcoll[M011] + u[1]*u[2];

        HMcoll[M210] = CHMcoll[M210] + u[1]*CHMcoll[M200] + 2.*u[0]*CHMcoll[M110] + ux2*u[1];
        HMcoll[M201] = CHMcoll[M201] + u[2]*CHMcoll[M200] + 2.*u[0]*CHMcoll[M101] + ux2*u[2];
        HMcoll[M021] = CHMcoll[M021] + u[2]*CHMcoll[M020] + 2.*u[1]*CHMcoll[M011] + uy2*u[2];
        HMcoll[M120] = CHMcoll[M120] + u[0]*CHMcoll[M020] + 2.*u[1]*CHMcoll[M110] + u[0]*uy2;
        HMcoll[M102] = CHMcoll[M102] + u[0]*CHMcoll[M002] + 2.*u[2]*CHMcoll[M101] + u[0]*uz2;
        HMcoll[M012] = CHMcoll[M012] + u[1]*CHMcoll[M002] + 2.*u[2]*CHMcoll[M011] + u[1]*uz2;
        
        HMcoll[M111] = CHMcoll[M111] + u[2]*CHMcoll[M110] + u[1]*CHMcoll[M101] + u[0]*CHMcoll[M011] + uxyz ;

        HMcoll[M220] = CHMcoll[M220] + 2.*u[1]*CHMcoll[M210] + 2.*u[0]*CHMcoll[M120] + uy2*CHMcoll[M200] + ux2*CHMcoll[M020] + 4.*u[0]*u[1]*CHMcoll[M110] + ux2*uy2;
        HMcoll[M202] = CHMcoll[M202] + 2.*u[2]*CHMcoll[M201] + 2.*u[0]*CHMcoll[M102] + uz2*CHMcoll[M200] + ux2*CHMcoll[M002] + 4.*u[0]*u[2]*CHMcoll[M101] + ux2*uz2;
        HMcoll[M022] = CHMcoll[M022] + 2.*u[2]*CHMcoll[M021] + 2.*u[1]*CHMcoll[M012] + uz2*CHMcoll[M020] + uy2*CHMcoll[M002] + 4.*u[1]*u[2]*CHMcoll[M011] + uy2*uz2;

        HMcoll[M211] = CHMcoll[M211] + u[2]*CHMcoll[M210] + u[1]*CHMcoll[M201] + 2.*u[0]*CHMcoll[M111] + u[1]*u[2]*CHMcoll[M200] + 2.*u[0]*u[2]*CHMcoll[M110] + 2.*u[0]*u[1]*CHMcoll[M101] + ux2*CHMcoll[M011] + ux2*u[1]*u[2];
        HMcoll[M121] = CHMcoll[M121] + u[2]*CHMcoll[M120] + u[0]*CHMcoll[M021] + 2.*u[1]*CHMcoll[M111] + u[0]*u[2]*CHMcoll[M020] + 2.*u[1]*u[2]*CHMcoll[M110] + 2.*u[0]*u[1]*CHMcoll[M011] + uy2*CHMcoll[M101] + u[0]*uy2*u[2];
        HMcoll[M112] = CHMcoll[M112] + u[1]*CHMcoll[M102] + u[0]*CHMcoll[M012] + 2.*u[2]*CHMcoll[M111] + u[0]*u[1]*CHMcoll[M002] + 2.*u[1]*u[2]*CHMcoll[M101] + 2.*u[0]*u[2]*CHMcoll[M011] + uz2*CHMcoll[M110] + u[0]*u[1]*uz2;

        HMcoll[M221] = CHMcoll[M221] + u[2]*CHMcoll[M220] + 2.*u[1]*CHMcoll[M211] + 2.*u[0]*CHMcoll[M121] + 2.*u[1]*u[2]*CHMcoll[M210] + uy2*CHMcoll[M201] + ux2*CHMcoll[M021] + 2.*u[0]*u[2]*CHMcoll[M120] + 4.*u[0]*u[1]*CHMcoll[M111] + uy2*u[2]*CHMcoll[M200] + ux2*u[2]*CHMcoll[M020] + 4.*uxyz*CHMcoll[M110] + 2.*u[0]*uy2*CHMcoll[M101] + 2.*ux2*u[1]*CHMcoll[M011] + ux2*uy2*u[2];
        HMcoll[M212] = CHMcoll[M212] + u[1]*CHMcoll[M202] + 2.*u[2]*CHMcoll[M211] + 2.*u[0]*CHMcoll[M112] + 2.*u[1]*u[2]*CHMcoll[M201] + uz2*CHMcoll[M210] + ux2*CHMcoll[M012] + 2.*u[0]*u[1]*CHMcoll[M102] + 4.*u[0]*u[2]*CHMcoll[M111] + u[1]*uz2*CHMcoll[M200] + ux2*u[1]*CHMcoll[M002] + 4.*uxyz*CHMcoll[M101] + 2.*u[0]*uz2*CHMcoll[M110] + 2.*ux2*u[2]*CHMcoll[M011] + ux2*u[1]*uz2;
        HMcoll[M122] = CHMcoll[M122] + u[0]*CHMcoll[M022] + 2.*u[2]*CHMcoll[M121] + 2.*u[1]*CHMcoll[M112] + 2.*u[0]*u[2]*CHMcoll[M021] + uz2*CHMcoll[M120] + uy2*CHMcoll[M102] + 2.*u[0]*u[1]*CHMcoll[M012] + 4.*u[1]*u[2]*CHMcoll[M111] + u[0]*uz2*CHMcoll[M020] + u[0]*uy2*CHMcoll[M002] + 4.*uxyz*CHMcoll[M011] + 2.*u[1]*uz2*CHMcoll[M110] + 2.*uy2*u[2]*CHMcoll[M101] + u[0]*uy2*uz2;

        HMcoll[M222] = CHMcoll[M222] + 2.*u[2]*CHMcoll[M221] + 2.*u[1]*CHMcoll[M212] + 2.*u[0]*CHMcoll[M122] + uz2*CHMcoll[M220] + uy2*CHMcoll[M202] + ux2*CHMcoll[M022] + 4.*u[1]*u[2]*CHMcoll[M211] + 4.*u[0]*u[2]*CHMcoll[M121] + 4.*u[0]*u[1]*CHMcoll[M112] + 2.*u[1]*uz2*CHMcoll[M210] + 2.*uy2*u[2]*CHMcoll[M201] + 2.*ux2*u[2]*CHMcoll[M021] + 2.*u[0]*uz2*CHMcoll[M120] + 2.*u[0]*uy2*CHMcoll[M102] + 2.*ux2*u[1]*CHMcoll[M012] + 8.*uxyz*CHMcoll[M111] + uy2*uz2*CHMcoll[M200] + ux2*uz2*CHMcoll[M020] + ux2*uy2*CHMcoll[M002] + 4.*u[0]*u[1]*uz2*CHMcoll[M110] + 4.*u[0]*uy2*u[2]*CHMcoll[M101] + 4.*ux2*u[1]*u[2]*CHMcoll[M011] + ux2*uy2*uz2;

        // Come back to RMcoll using relationships between HMs and RMs
        RMcoll[M200] = HMcoll[M200] + cs2;
        RMcoll[M020] = HMcoll[M020] + cs2;
        RMcoll[M002] = HMcoll[M002] + cs2;
        
        RMcoll[M110] = HMcoll[M110];
        RMcoll[M101] = HMcoll[M101];
        RMcoll[M011] = HMcoll[M011];

        RMcoll[M210] = HMcoll[M210] + cs2*u[1];
        RMcoll[M201] = HMcoll[M201] + cs2*u[2];
        RMcoll[M021] = HMcoll[M021] + cs2*u[2];
        RMcoll[M120] = HMcoll[M120] + cs2*u[0];
        RMcoll[M102] = HMcoll[M102] + cs2*u[0];
        RMcoll[M012] = HMcoll[M012] + cs2*u[1];
        
        RMcoll[M111] = HMcoll[M111];

        RMcoll[M220] = HMcoll[M220] + cs2*(HMcoll[M200] + HMcoll[M020]) + cs4;
        RMcoll[M202] = HMcoll[M202] + cs2*(HMcoll[M200] + HMcoll[M002]) + cs4;
        RMcoll[M022] = HMcoll[M022] + cs2*(HMcoll[M020] + HMcoll[M002]) + cs4;

        RMcoll[M211] = HMcoll[M211] + cs2*HMcoll[M011];
        RMcoll[M121] = HMcoll[M121] + cs2*HMcoll[M101];
        RMcoll[M112] = HMcoll[M112] + cs2*HMcoll[M110];

        RMcoll[M221] = HMcoll[M221] + cs2*(HMcoll[M201] + HMcoll[M021]) + cs4*u[2];
        RMcoll[M212] = HMcoll[M212] + cs2*(HMcoll[M210] + HMcoll[M012]) + cs4*u[1];
        RMcoll[M122] = HMcoll[M122] + cs2*(HMcoll[M120] + HMcoll[M102]) + cs4*u[0];

        RMcoll[M222] = HMcoll[M222] + cs2*(HMcoll[M220] + HMcoll[M202] + HMcoll[M022]) + cs4*(HMcoll[M200] + HMcoll[M020] + HMcoll[M002]) + cs2*cs4;

        // Optimization based on symmetries between populations and their opposite counterpart
        std::array<double, 27> foutRM;

        foutRM[F000] = rho *(1. - RMcoll[M200] - RMcoll[M020] - RMcoll[M002] + RMcoll[M220] + RMcoll[M202] + RMcoll[M022] - RMcoll[M222]);
        
        foutRM[FP00] = 0.5*rho * ( u[0] + RMcoll[M200] - RMcoll[M120] - RMcoll[M102] - RMcoll[M220] - RMcoll[M202] + RMcoll[M122] + RMcoll[M222]);
        foutRM[FM00] =     rho * (-u[0]                + RMcoll[M120] + RMcoll[M102]                               - RMcoll[M122])+ foutRM[FP00];

        foutRM[F0P0] = 0.5*rho * ( u[1] + RMcoll[M020] - RMcoll[M210] - RMcoll[M012] - RMcoll[M220] - RMcoll[M022] + RMcoll[M212] + RMcoll[M222]);
        foutRM[F0M0] =     rho * (-u[1]                + RMcoll[M210] + RMcoll[M012]                               - RMcoll[M212])+ foutRM[F0P0];

        foutRM[F00P] = 0.5*rho * ( u[2] + RMcoll[M002] - RMcoll[M201] - RMcoll[M021] - RMcoll[M202] - RMcoll[M022] + RMcoll[M221] + RMcoll[M222]);
        foutRM[F00M] =     rho * (-u[2]                + RMcoll[M201] + RMcoll[M021]                               - RMcoll[M221])+ foutRM[F00P];

        foutRM[FPP0] = 0.25*rho * ( RMcoll[M110] + RMcoll[M210] + RMcoll[M120] - RMcoll[M112] + RMcoll[M220] - RMcoll[M212] - RMcoll[M122] - RMcoll[M222]);
        foutRM[FMP0] =  0.5*rho * (-RMcoll[M110]                - RMcoll[M120] + RMcoll[M112]                               + RMcoll[M122])+ foutRM[FPP0];
        foutRM[FPM0] =  0.5*rho * (-RMcoll[M110] - RMcoll[M210]                + RMcoll[M112]                + RMcoll[M212]               )+ foutRM[FPP0];
        foutRM[FMM0] =  0.5*rho * (              - RMcoll[M210] - RMcoll[M120]                               + RMcoll[M212] + RMcoll[M122])+ foutRM[FPP0];

        foutRM[FP0P] = 0.25*rho * ( RMcoll[M101] + RMcoll[M201] + RMcoll[M102] - RMcoll[M121] + RMcoll[M202] - RMcoll[M221] - RMcoll[M122] - RMcoll[M222]);
        foutRM[FM0P] =  0.5*rho * (-RMcoll[M101]                - RMcoll[M102] + RMcoll[M121]                               + RMcoll[M122])+ foutRM[FP0P];
        foutRM[FP0M] =  0.5*rho * (-RMcoll[M101] - RMcoll[M201]                + RMcoll[M121]                + RMcoll[M221]               )+ foutRM[FP0P];
        foutRM[FM0M] =  0.5*rho * (              - RMcoll[M201] - RMcoll[M102]                               + RMcoll[M221] + RMcoll[M122])+ foutRM[FP0P];

        foutRM[F0PP] = 0.25*rho * ( RMcoll[M011] + RMcoll[M021] + RMcoll[M012] - RMcoll[M211] + RMcoll[M022] - RMcoll[M221] - RMcoll[M212] - RMcoll[M222]);
        foutRM[F0MP] =  0.5*rho * (-RMcoll[M011]                - RMcoll[M012] + RMcoll[M211]                               + RMcoll[M212])+ foutRM[F0PP];
        foutRM[F0PM] =  0.5*rho * (-RMcoll[M011] - RMcoll[M021]                + RMcoll[M211]                + RMcoll[M221]               )+ foutRM[F0PP];
        foutRM[F0MM] =  0.5*rho * (              - RMcoll[M021] - RMcoll[M012]                               + RMcoll[M221] + RMcoll[M212])+ foutRM[F0PP];

        foutRM[FPPP] = 0.125*rho * ( RMcoll[M111] + RMcoll[M211] + RMcoll[M121] + RMcoll[M112] + RMcoll[M221] + RMcoll[M212] + RMcoll[M122] + RMcoll[M222]);
        foutRM[FMPP] =  0.25*rho * (-RMcoll[M111]                - RMcoll[M121] - RMcoll[M112]                               - RMcoll[M122])+ foutRM[FPPP];
        foutRM[FPMP] =  0.25*rho * (-RMcoll[M111] - RMcoll[M211]                - RMcoll[M112]                - RMcoll[M212]               )+ foutRM[FPPP];
        foutRM[FPPM] =  0.25*rho * (-RMcoll[M111] - RMcoll[M211] - RMcoll[M121]                - RMcoll[M221]                              )+ foutRM[FPPP];
        foutRM[FMMP] =  0.25*rho * (              - RMcoll[M211] - RMcoll[M121]                               - RMcoll[M212] - RMcoll[M122])+ foutRM[FPPP];
        foutRM[FMPM] =  0.25*rho * (              - RMcoll[M211]                - RMcoll[M112] - RMcoll[M221]                - RMcoll[M122])+ foutRM[FPPP];
        foutRM[FPMM] =  0.25*rho * (                             - RMcoll[M121] - RMcoll[M112] - RMcoll[M221] - RMcoll[M212]               )+ foutRM[FPPP];
        foutRM[FMMM] =  0.25*rho * (-RMcoll[M111]                                              - RMcoll[M221] - RMcoll[M212] - RMcoll[M122])+ foutRM[FPPP];

        // Unrolled streaming step
        int XX = periodic ? (iX - 1 + dim.nx) % dim.nx: iX - 1;
        int YY = iY;
        int ZZ = iZ;
        size_t nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[ 0]) = foutRM[0] + f(nb, 0);
        }
        else {
            fout(nb, 0) = foutRM[0];
        }
        XX = iX;
        YY = periodic ? (iY - 1 + dim.ny) % dim.ny: iY - 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[ 1]) = foutRM[1] + f(nb, 1);
        }
        else {
            fout(nb, 1) = foutRM[1];
        }
        XX = iX;
        YY = iY;
        ZZ = periodic ? (iZ - 1 + dim.nz) % dim.nz: iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[ 2]) = foutRM[2] + f(nb, 2);
        }
        else {
            fout(nb, 2) = foutRM[2];
        }
        XX = periodic ? (iX - 1 + dim.nx) % dim.nx: iX - 1;
        YY = periodic ? (iY - 1 + dim.ny) % dim.ny: iY - 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[ 3]) = foutRM[3] + f(nb, 3);
        }
        else {
            fout(nb, 3) = foutRM[3];
        }
        XX = periodic ? (iX - 1 + dim.nx) % dim.nx: iX - 1;
        YY = periodic ? (iY + 1 + dim.ny) % dim.ny: iY + 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[ 4]) = foutRM[4] + f(nb, 4);
        }
        else {
            fout(nb, 4) = foutRM[4];
        }
        XX = periodic ? (iX - 1 + dim.nx) % dim.nx: iX - 1;
        YY = iY;
        ZZ = periodic ? (iZ - 1 + dim.nz) % dim.nz: iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[ 5]) = foutRM[5] + f(nb, 5);
        }
        else {
            fout(nb, 5) = foutRM[5];
        }
        XX = periodic ? (iX - 1 + dim.nx) % dim.nx: iX - 1;
        YY = iY;
        ZZ = periodic ? (iZ + 1 + dim.nz) % dim.nz: iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[ 6]) = foutRM[6] + f(nb, 6);
        }
        else {
            fout(nb, 6) = foutRM[6];
        }
        XX = iX;
        YY = periodic ? (iY - 1 + dim.ny) % dim.ny: iY - 1;
        ZZ = periodic ? (iZ - 1 + dim.nz) % dim.nz: iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[ 7]) = foutRM[7] + f(nb, 7);
        }
        else {
            fout(nb, 7) = foutRM[7];
        }
        XX = iX;
        YY = periodic ? (iY - 1 + dim.ny) % dim.ny: iY - 1;
        ZZ = periodic ? (iZ + 1 + dim.nz) % dim.nz: iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[ 8]) = foutRM[8] + f(nb, 8);
        }
        else {
            fout(nb, 8) = foutRM[8];
        }
        XX = periodic ? (iX - 1 + dim.nx) % dim.nx: iX - 1;
        YY = periodic ? (iY - 1 + dim.ny) % dim.ny: iY - 1;
        ZZ = periodic ? (iZ - 1 + dim.nz) % dim.nz: iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[ 9]) = foutRM[9] + f(nb, 9);
        }
        else {
            fout(nb, 9) = foutRM[9];
        }
        XX = periodic ? (iX - 1 + dim.nx) % dim.nx: iX - 1;
        YY = periodic ? (iY - 1 + dim.ny) % dim.ny: iY - 1;
        ZZ = periodic ? (iZ + 1 + dim.nz) % dim.nz: iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[10]) = foutRM[10] + f(nb,10);
        }
        else {
            fout(nb,10) = foutRM[10];
        }
        XX = periodic ? (iX - 1 + dim.nx) % dim.nx: iX - 1;
        YY = periodic ? (iY + 1 + dim.ny) % dim.ny: iY + 1;
        ZZ = periodic ? (iZ - 1 + dim.nz) % dim.nz: iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[11]) = foutRM[11] + f(nb,11);
        }
        else {
            fout(nb,11) = foutRM[11];
        }
        XX = periodic ? (iX - 1 + dim.nx) % dim.nx: iX - 1;
        YY = periodic ? (iY + 1 + dim.ny) % dim.ny: iY + 1;
        ZZ = periodic ? (iZ + 1 + dim.nz) % dim.nz: iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[12]) = foutRM[12] + f(nb,12);
        }
        else {
            fout(nb,12) = foutRM[12];
        }


        fout(i,13) = foutRM[13];


        XX = periodic ? (iX + 1 + dim.nx) % dim.nx: iX + 1;
        YY = iY;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[14]) = foutRM[14] + f(nb,14);
        }
        else {
            fout(nb,14) = foutRM[14];
        }
        XX = iX;
        YY = periodic ? (iY + 1 + dim.ny) % dim.ny: iY + 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[15]) = foutRM[15] + f(nb,15);
        }
        else {
            fout(nb,15) = foutRM[15];
        }
        XX = iX;
        YY = iY;
        ZZ = periodic ? (iZ + 1 + dim.nz) % dim.nz: iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[16]) = foutRM[16] + f(nb,16);
        }
        else {
            fout(nb,16) = foutRM[16];
        }
        XX = periodic ? (iX + 1 + dim.nx) % dim.nx: iX + 1;
        YY = periodic ? (iY + 1 + dim.ny) % dim.ny: iY + 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[17]) = foutRM[17] + f(nb,17);
        }
        else {
            fout(nb,17) = foutRM[17];
        }
        XX = periodic ? (iX + 1 + dim.nx) % dim.nx: iX + 1;
        YY = periodic ? (iY - 1 + dim.ny) % dim.ny: iY - 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[18]) = foutRM[18] + f(nb,18);
        }
        else {
            fout(nb,18) = foutRM[18];
        }
        XX = periodic ? (iX + 1 + dim.nx) % dim.nx: iX + 1;
        YY = iY;
        ZZ = periodic ? (iZ + 1 + dim.nz) % dim.nz: iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[19]) = foutRM[19] + f(nb,19);
        }
        else {
            fout(nb,19) = foutRM[19];
        }
        XX = periodic ? (iX + 1 + dim.nx) % dim.nx: iX + 1;
        YY = iY;
        ZZ = periodic ? (iZ - 1 + dim.nz) % dim.nz: iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[20]) = foutRM[20] + f(nb,20);
        }
        else {
            fout(nb,20) = foutRM[20];
        }
        XX = iX;
        YY = periodic ? (iY + 1 + dim.ny) % dim.ny: iY + 1;
        ZZ = periodic ? (iZ + 1 + dim.nz) % dim.nz: iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[21]) = foutRM[21] + f(nb,21);
        }
        else {
            fout(nb,21) = foutRM[21];
        }
        XX = iX;
        YY = periodic ? (iY + 1 + dim.ny) % dim.ny: iY + 1;
        ZZ = periodic ? (iZ - 1 + dim.nz) % dim.nz: iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[22]) = foutRM[22] + f(nb,22);
        }
        else {
            fout(nb,22) = foutRM[22];
        }

        XX = periodic ? (iX + 1 + dim.nx) % dim.nx: iX + 1;
        YY = periodic ? (iY + 1 + dim.ny) % dim.ny: iY + 1;
        ZZ = periodic ? (iZ + 1 + dim.nz) % dim.nz: iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[23]) = foutRM[23] + f(nb,23);
        }
        else {
            fout(nb,23) = foutRM[23];
        }
        XX = periodic ? (iX + 1 + dim.nx) % dim.nx: iX + 1;
        YY = periodic ? (iY + 1 + dim.ny) % dim.ny: iY + 1;
        ZZ = periodic ? (iZ - 1 + dim.nz) % dim.nz: iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[24]) = foutRM[24] + f(nb,24);
        }
        else {
            fout(nb,24) = foutRM[24];
        }
        XX = periodic ? (iX + 1 + dim.nx) % dim.nx: iX + 1;
        YY = periodic ? (iY - 1 + dim.ny) % dim.ny: iY - 1;
        ZZ = periodic ? (iZ + 1 + dim.nz) % dim.nz: iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[25]) = foutRM[25] + f(nb,25);
        }
        else {
            fout(nb,25) = foutRM[25];
        }
        XX = periodic ? (iX + 1 + dim.nx) % dim.nx: iX + 1;
        YY = periodic ? (iY - 1 + dim.ny) % dim.ny: iY - 1;
        ZZ = periodic ? (iZ - 1 + dim.nz) % dim.nz: iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[26]) = foutRM[26] + f(nb,26);
        }
        else {
            fout(nb,26) = foutRM[26];
        }
    }

    void iterateCHM(double& f0) {
        int i = &f0 - lattice;
        if (flag[i] == CellType::bulk) {
            auto[iX, iY, iZ] = i_to_xyz(i);
            // auto CHM = computeCHM(f0);
            // auto CHM = computeCHMopt(f0);
            auto CHM = computeCHMopt2(f0);
            double rho = CHM[M000];
            std::array<double, 3> u = {CHM[M100], CHM[M010], CHM[M001]};  
            auto CHMeq = computeCHMeq();
            collideAndStreamCHM(i, iX, iY, iZ, rho, u, CHM, CHMeq);
        }
    }

    void operator() (double& f0) {
        iterateCHM(f0);
    }
};

} // namespace twopop_soa_chm


