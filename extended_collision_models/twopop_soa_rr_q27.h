// *****************************************************************************
// STLBM SOFTWARE LIBRARY

// Copyright © 2020 University of Geneva
// Authors: Jonas Latt, Christophe Coreixas, Joël Beny
// Contact: Jonas.Latt@unige.ch

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// *****************************************************************************

#pragma once

#include "lbm_q27.h"
#include <vector>
#include <array>
#include <tuple>
#include <iostream>

namespace twopop_soa_rr {

struct LBM {
    using CellData = double;
    static size_t sizeOfLattice(size_t nelem) { return 2 * 27 * nelem; }

    CellData* lattice;
    CellType* flag;
    int* parity;
    std::array<int, 3>* c;
    int* opp;
    double* t;
    double omega1;
    Dim dim;
    LBModel model;
    bool periodic = false;
    double omegaBulk = 0.;
    double omega2 = omega1;
    double omega3 = omega1;
    double omega4 = omega1;
    double omega5 = omega1;
    double omega6 = omega1;
    double omega7 = omega1;
    double omega8 = omega1;

    auto i_to_xyz (int i) const {
        int iX = i / (dim.ny * dim.nz);
        int remainder = i % (dim.ny * dim.nz);
        int iY = remainder / dim.nz;
        int iZ = remainder % dim.nz;
        return std::make_tuple(iX, iY, iZ);
    };

    size_t xyz_to_i (int x, int y, int z) const {
        return z + dim.nz * (y + dim.ny * x);
    };

    double& f (int i, int k) {
        return lattice[k * dim.nelem + i];
    }

    double& fin (int i, int k) {
        return lattice[*parity * dim.npop + k * dim.nelem + i];
    }
    
    double& fout (int i, int k) {
        return lattice[(1 - *parity) * dim.npop + k * dim.nelem + i];
    }

    auto iniLattice (double& f0) {
        auto i = &f0 - lattice;
        for (int k = 0; k < 27; ++k) {
            fin(i, k) = t[k];
        }
    };

    auto iniLattice (double& f0, double rho, std::array<double, 3> const& u) {
        auto i = &f0 - lattice;
        double usqr = 1.5 * (u[0] * u[0] + u[1] * u[1] + u[2] * u[2]);
        for (int k = 0; k < 27; ++k) {
            double ck_u = c[k][0] * u[0] + c[k][1] * u[1] + c[k][2] * u[2];
            f(i, k) = rho * t[k] * (1. + 3. * ck_u + 4.5 * ck_u * ck_u - usqr);
        }
    };

    // Required for the nonregression based on computeEnergy() 
    auto macro (double const& f0) {
        auto i = &f0 - lattice;
        double X_M1 = fin(i, 0) + fin(i, 3) + fin(i, 4) + fin(i, 5) + fin(i, 6) + fin(i, 9) + fin(i,10) + fin(i,11) + fin(i,12);
        double X_P1 = fin(i,14) + fin(i,17) + fin(i,18) + fin(i,19) + fin(i,20) + fin(i,23) + fin(i,24) + fin(i,25) + fin(i,26);
        double X_0  = fin(i, 1) + fin(i, 2) + fin(i, 7) + fin(i, 8) + fin(i,13) + fin(i,15) + fin(i,16) + fin(i,21) + fin(i,22);

        double Y_M1 = fin(i, 1) + fin(i, 3) + fin(i, 7) + fin(i, 8) + fin(i, 9) + fin(i,10) + fin(i,18) + fin(i,25) + fin(i,26);
        double Y_P1 = fin(i,15) + fin(i,17) + fin(i,21) + fin(i,22) + fin(i,23) + fin(i,24) + fin(i, 4) + fin(i,11) + fin(i,12);

        double Z_M1 = fin(i, 2) + fin(i, 5) + fin(i, 7) + fin(i, 9) + fin(i,11) + fin(i,20) + fin(i,22) + fin(i,24) + fin(i,26);
        double Z_P1 = fin(i,16) + fin(i,19) + fin(i,21) + fin(i,23) + fin(i,25) + fin(i, 6) + fin(i, 8) + fin(i,10) + fin(i,12);

        double rho = X_M1 + X_P1 + X_0;
        std::array<double, 3> u{ (X_P1 - X_M1) / rho, (Y_P1 - Y_M1) / rho, (Z_P1 - Z_M1) / rho };
        return std::make_pair(rho, u);
    }
 
    // Naive way to compute RR and macros (we only need 2nd-order RRs)
    auto computeRR(double const& f0) {
        auto i = &f0 - lattice;
        std::array<double, 27> RR;
        std::fill(RR.begin(), RR.end(), 0.);
        double Hxx, Hyy, Hzz;
        double cs2 = 1./3.;
        for (int k = 0; k<27; ++k) {
            Hxx = c[k][0] * c[k][0] - cs2;
            Hyy = c[k][1] * c[k][1] - cs2;
            Hzz = c[k][2] * c[k][2] - cs2;
            // Order 0
            RR[M000] += fin(i,k);
            // Order 1
            RR[M100] += c[k][0] * fin(i,k);
            RR[M010] += c[k][1] * fin(i,k);
            RR[M001] += c[k][2] * fin(i,k);
            // Order 2
            RR[M200] += Hxx * fin(i,k);
            RR[M020] += Hyy * fin(i,k);
            RR[M002] += Hzz * fin(i,k);
            RR[M110] += c[k][0] * c[k][1] * fin(i,k);
            RR[M101] += c[k][0] * c[k][2] * fin(i,k);
            RR[M011] += c[k][1] * c[k][2] * fin(i,k);
        }
        double invRho = 1. / RR[M000];
        for (int k = 1; k<10; ++k) {
            RR[k] *= invRho;
        }
        return RR;
    }

    // First optimization (loop unrolling)
    auto computeRRopt(double const& f0) {
        auto i = &f0 - lattice;
        std::array<double, 27> RR;
        std::fill(RR.begin(), RR.end(), 0.);
        // Order 0
        RR[M000] =  fin(i, 0) + fin(i, 1) + fin(i, 2) + fin(i, 3) + fin(i, 4) + fin(i, 5) + fin(i, 6) + fin(i, 7) + fin(i, 8) + fin(i, 9) + fin(i,10) + fin(i,11) + fin(i,12) + fin(i,13) + fin(i,14) + fin(i,15) + fin(i,16) + fin(i,17) + fin(i,18) + fin(i,19) + fin(i,20) + fin(i,21) + fin(i,22) + fin(i,23) + fin(i,24) + fin(i,25) + fin(i,26);
        double invRho = 1./RR[M000];
        // Order 1
        RR[M100] = invRho * (- fin(i, 0) - fin(i, 3) - fin(i, 4) - fin(i, 5) - fin(i, 6) - fin(i, 9) - fin(i,10) - fin(i,11) - fin(i,12) + fin(i,14) + fin(i,17) + fin(i,18) + fin(i,19) + fin(i,20) + fin(i,23) + fin(i,24) + fin(i,25) + fin(i,26));
        RR[M010] = invRho * (- fin(i, 1) - fin(i, 3) + fin(i, 4) - fin(i, 7) - fin(i, 8) - fin(i, 9) - fin(i,10) + fin(i,11) + fin(i,12) + fin(i,15) + fin(i,17) - fin(i,18) + fin(i,21) + fin(i,22) + fin(i,23) + fin(i,24) - fin(i,25) - fin(i,26));
        RR[M001] = invRho * (- fin(i, 2) - fin(i, 5) + fin(i, 6) - fin(i, 7) + fin(i, 8) - fin(i, 9) + fin(i,10) - fin(i,11) + fin(i,12) + fin(i,16) + fin(i,19) - fin(i,20) + fin(i,21) - fin(i,22) + fin(i,23) - fin(i,24) + fin(i,25) - fin(i,26));
        // Order 2
        RR[M200] = invRho * (  fin(i, 0) + fin(i, 3) + fin(i, 4) + fin(i, 5) + fin(i, 6) + fin(i, 9) + fin(i,10) + fin(i,11) + fin(i,12) + fin(i,14) + fin(i,17) + fin(i,18) + fin(i,19) + fin(i,20) + fin(i,23) + fin(i,24) + fin(i,25) + fin(i,26));
        RR[M020] = invRho * (  fin(i, 1) + fin(i, 3) + fin(i, 4) + fin(i, 7) + fin(i, 8) + fin(i, 9) + fin(i,10) + fin(i,11) + fin(i,12) + fin(i,15) + fin(i,17) + fin(i,18) + fin(i,21) + fin(i,22) + fin(i,23) + fin(i,24) + fin(i,25) + fin(i,26));
        RR[M002] = invRho * (  fin(i, 2) + fin(i, 5) + fin(i, 6) + fin(i, 7) + fin(i, 8) + fin(i, 9) + fin(i,10) + fin(i,11) + fin(i,12) + fin(i,16) + fin(i,19) + fin(i,20) + fin(i,21) + fin(i,22) + fin(i,23) + fin(i,24) + fin(i,25) + fin(i,26));
        RR[M110] = invRho * (  fin(i, 3) - fin(i, 4) + fin(i, 9) + fin(i,10) - fin(i,11) - fin(i,12) + fin(i,17) - fin(i,18) + fin(i,23) + fin(i,24) - fin(i,25) - fin(i,26));
        RR[M101] = invRho * (  fin(i, 5) - fin(i, 6) + fin(i, 9) - fin(i,10) + fin(i,11) - fin(i,12) + fin(i,19) - fin(i,20) + fin(i,23) - fin(i,24) + fin(i,25) - fin(i,26));
        RR[M011] = invRho * (  fin(i, 7) - fin(i, 8) + fin(i, 9) - fin(i,10) - fin(i,11) + fin(i,12) + fin(i,21) - fin(i,22) + fin(i,23) - fin(i,24) - fin(i,25) + fin(i,26));

        return RR;
    }

    // Most optimized version (loop unrolling + redundant terms)
    auto computeRRopt2(double const& f0) {
        auto i = &f0 - lattice;
        std::array<double, 27> RR;
        std::fill(RR.begin(), RR.end(), 0.);

        double X_M1 = fin(i, 0) + fin(i, 3) + fin(i, 4) + fin(i, 5) + fin(i, 6) + fin(i, 9) + fin(i,10) + fin(i,11) + fin(i,12);
        double X_P1 = fin(i,14) + fin(i,17) + fin(i,18) + fin(i,19) + fin(i,20) + fin(i,23) + fin(i,24) + fin(i,25) + fin(i,26);
        double X_0  = fin(i, 1) + fin(i, 2) + fin(i, 7) + fin(i, 8) + fin(i,13) + fin(i,15) + fin(i,16) + fin(i,21) + fin(i,22);

        double Y_M1 = fin(i, 1) + fin(i, 3) + fin(i, 7) + fin(i, 8) + fin(i, 9) + fin(i,10) + fin(i,18) + fin(i,25) + fin(i,26);
        double Y_P1 = fin(i,15) + fin(i,17) + fin(i,21) + fin(i,22) + fin(i,23) + fin(i,24) + fin(i, 4) + fin(i,11) + fin(i,12);

        double Z_M1 = fin(i, 2) + fin(i, 5) + fin(i, 7) + fin(i, 9) + fin(i,11) + fin(i,20) + fin(i,22) + fin(i,24) + fin(i,26);
        double Z_P1 = fin(i,16) + fin(i,19) + fin(i,21) + fin(i,23) + fin(i,25) + fin(i, 6) + fin(i, 8) + fin(i,10) + fin(i,12);

        // Order 0
        RR[M000] = X_M1 + X_P1 + X_0;
        double invRho = 1. / RR[M000];
        // Order 1
        RR[M100] = invRho * (X_P1 - X_M1);
        RR[M010] = invRho * (Y_P1 - Y_M1); 
        RR[M001] = invRho * (Z_P1 - Z_M1);
        // Order 2
        RR[M200] = invRho * (X_P1 + X_M1);
        RR[M020] = invRho * (Y_P1 + Y_M1); 
        RR[M002] = invRho * (Z_P1 + Z_M1);
        RR[M110] = invRho * (  fin(i, 3) - fin(i, 4) + fin(i, 9) + fin(i,10) - fin(i,11) - fin(i,12) + fin(i,17) - fin(i,18) + fin(i,23) + fin(i,24) - fin(i,25) - fin(i,26));
        RR[M101] = invRho * (  fin(i, 5) - fin(i, 6) + fin(i, 9) - fin(i,10) + fin(i,11) - fin(i,12) + fin(i,19) - fin(i,20) + fin(i,23) - fin(i,24) + fin(i,25) - fin(i,26));
        RR[M011] = invRho * (  fin(i, 7) - fin(i, 8) + fin(i, 9) - fin(i,10) - fin(i,11) + fin(i,12) + fin(i,21) - fin(i,22) + fin(i,23) - fin(i,24) - fin(i,25) + fin(i,26));

        // We come back to Hermite moments
        double cs2 = 1./3.;
        RR[M200] -= cs2;
        RR[M020] -= cs2;
        RR[M002] -= cs2;

        return RR;
    }

    auto computeRReq(std::array<double, 3> const& u) {

        std::array<double, 27> RReq;
        std::fill(RReq.begin(), RReq.end(), 0.);

        // Order 2
        RReq[M200] = u[0] * u[0];
        RReq[M020] = u[1] * u[1];
        RReq[M002] = u[2] * u[2];
        RReq[M110] = u[0] * u[1];
        RReq[M101] = u[0] * u[2];
        RReq[M011] = u[1] * u[2];
        // Order 3
        RReq[M210] = RReq[M200] * u[1];
        RReq[M201] = RReq[M200] * u[2];
        RReq[M021] = RReq[M020] * u[2];
        RReq[M120] = RReq[M020] * u[0];
        RReq[M102] = RReq[M002] * u[0];
        RReq[M012] = RReq[M002] * u[1];
        RReq[M111] = RReq[M110] * u[2];
        // Order 4
        RReq[M220] = RReq[M200] * RReq[M020];
        RReq[M202] = RReq[M200] * RReq[M002];
        RReq[M022] = RReq[M020] * RReq[M002];
        RReq[M211] = RReq[M200] * RReq[M011];
        RReq[M121] = RReq[M020] * RReq[M101];
        RReq[M112] = RReq[M002] * RReq[M110];
        // Order 5
        RReq[M221] = RReq[M220] * u[2];
        RReq[M212] = RReq[M202] * u[1];
        RReq[M122] = RReq[M022] * u[0];
        // Order 6
        RReq[M222] = RReq[M220] * RReq[M002];

        return RReq;
    }

    auto collideAndStreamRR(int i, int iX, int iY, int iZ, double rho, std::array<double, 3> const& u, std::array<double, 27> const& RR, std::array<double, 27> const& RReq)
    {
        // Post-collision and Nonequilibrium moments.
        std::array<double, 27> RRneq;
        std::array<double, 27> RRcoll;
        std::array<double, 27> RMcoll;

        // Recursive computation of nonequilibrium Hermite moments
        // Order 2 (standard way to compute them)
        RRneq[M200] = RR[M200] - RReq[M200];
        RRneq[M020] = RR[M020] - RReq[M020];
        RRneq[M002] = RR[M002] - RReq[M002];
        RRneq[M110] = RR[M110] - RReq[M110];
        RRneq[M101] = RR[M101] - RReq[M101];
        RRneq[M011] = RR[M011] - RReq[M011];

        // Order 3 (reconstruction using Chapman-Enskog formulas)
        RRneq[M210] = u[1]*RRneq[M200] + 2.*u[0]*RRneq[M110];
        RRneq[M201] = u[2]*RRneq[M200] + 2.*u[0]*RRneq[M101];
        RRneq[M021] = u[2]*RRneq[M020] + 2.*u[1]*RRneq[M011];
        RRneq[M120] = u[0]*RRneq[M020] + 2.*u[1]*RRneq[M110];
        RRneq[M102] = u[0]*RRneq[M002] + 2.*u[2]*RRneq[M101];
        RRneq[M012] = u[1]*RRneq[M002] + 2.*u[2]*RRneq[M011];
        RRneq[M111] = u[2]*RRneq[M110] + u[1]*RRneq[M101] + u[0]*RRneq[M011];

        // Order 4 (reconstruction using Chapman-Enskog formulas)
        RRneq[M220] = RReq[M020]*RRneq[M200] + RReq[M200]*RRneq[M020] + 4.*RReq[M110]*RRneq[M110];
        RRneq[M202] = RReq[M002]*RRneq[M200] + RReq[M200]*RRneq[M002] + 4.*RReq[M101]*RRneq[M101];
        RRneq[M022] = RReq[M002]*RRneq[M020] + RReq[M020]*RRneq[M002] + 4.*RReq[M011]*RRneq[M011];
        RRneq[M211] = u[1]*u[2]*RRneq[M200] + 2.*u[0]*u[2]*RRneq[M110] + 2.*u[0]*u[1]*RRneq[M101] + u[0]*u[0]*RRneq[M011];
        RRneq[M121] = u[0]*u[2]*RRneq[M020] + 2.*u[1]*u[2]*RRneq[M110] + u[1]*u[1]*RRneq[M101] + 2.*u[0]*u[1]*RRneq[M011];
        RRneq[M112] = u[0]*u[1]*RRneq[M002] + u[2]*u[2]*RRneq[M110] + 2.*u[1]*u[2]*RRneq[M101] + 2.*u[0]*u[2]*RRneq[M011];

        // Order 5 (reconstruction using Chapman-Enskog formulas)
        RRneq[M221] = u[1]*u[1]*u[2]*RRneq[M200] + u[0]*u[0]*u[2]*RRneq[M020] + 4.*u[0]*u[1]*u[2]*RRneq[M110] + 2.*u[0]*u[1]*u[1]*RRneq[M101] + 2.*u[0]*u[0]*u[1]*RRneq[M011];
        RRneq[M212] = u[2]*u[2]*u[1]*RRneq[M200] + u[0]*u[0]*u[1]*RRneq[M002] + 2.*u[0]*u[2]*u[2]*RRneq[M110] + 4.*u[0]*u[1]*u[2]*RRneq[M101] + 2.*u[0]*u[0]*u[2]*RRneq[M011];
        RRneq[M122] = u[2]*u[2]*u[0]*RRneq[M020] + u[1]*u[1]*u[0]*RRneq[M002] + 2.*u[1]*u[2]*u[2]*RRneq[M110] + 2.*u[1]*u[1]*u[2]*RRneq[M101] + 4.*u[0]*u[1]*u[2]*RRneq[M011];

        // Order 6 (reconstruction using Chapman-Enskog formulas)
        RRneq[M222] = u[1]*u[1]*u[2]*u[2]*RRneq[M200] + u[0]*u[0]*u[2]*u[2]*RRneq[M020] + u[0]*u[0]*u[1]*u[1]*RRneq[M002] + 4.*u[0]*u[1]*u[2]*u[2]*RRneq[M110] + 4.*u[0]*u[1]*u[1]*u[2]*RRneq[M101] + 4.*u[0]*u[0]*u[1]*u[2]*RRneq[M011];

        // Collision in the Hermite moment space
        // Order 2
        if (omegaBulk == 0.) { // Don't adjust bulk viscosity
            RRcoll[M200] = RR[M200] - omega1 * RRneq[M200];
            RRcoll[M020] = RR[M020] - omega1 * RRneq[M020];
            RRcoll[M002] = RR[M002] - omega1 * RRneq[M002];
        }
        else { // Adjust bulk viscosity
            const double omegaMinus = (omegaBulk - omega1)/3.; // Notation used by Fei
            const double omegaPlus  = omegaMinus + omega1;     // Notation used by Fei
            RRcoll[M200] = RR[M200] - omegaPlus  * RRneq[M200] - omegaMinus * RRneq[M020] - omegaMinus * RRneq[M002] ;
            RRcoll[M020] = RR[M020] - omegaMinus * RRneq[M200] - omegaPlus  * RRneq[M020] - omegaMinus * RRneq[M002] ;
            RRcoll[M002] = RR[M002] - omegaMinus * RRneq[M200] - omegaMinus * RRneq[M020] - omegaPlus  * RRneq[M002] ;
        }
        RRcoll[M110] = RR[M110] - omega2 * RRneq[M110];
        RRcoll[M101] = RR[M101] - omega2 * RRneq[M101];
        RRcoll[M011] = RR[M011] - omega2 * RRneq[M011];
        // Order 3 (Optimization: use RReq to avoid the computation of 3rd-order RR)
        RRcoll[M210] = RReq[M210] + (1. - omega3) * RRneq[M210];
        RRcoll[M201] = RReq[M201] + (1. - omega3) * RRneq[M201];
        RRcoll[M021] = RReq[M021] + (1. - omega3) * RRneq[M021];
        RRcoll[M120] = RReq[M120] + (1. - omega3) * RRneq[M120];
        RRcoll[M102] = RReq[M102] + (1. - omega3) * RRneq[M102];
        RRcoll[M012] = RReq[M012] + (1. - omega3) * RRneq[M012];
        RRcoll[M111] = RReq[M111] + (1. - omega4) * RRneq[M111];
        // Order 4 (Optimization: use RReq to avoid the computation of 4th-order RR)
        RRcoll[M220] = RReq[M220] + (1. - omega5) * RRneq[M220];
        RRcoll[M202] = RReq[M202] + (1. - omega5) * RRneq[M202];
        RRcoll[M022] = RReq[M022] + (1. - omega5) * RRneq[M022];
        RRcoll[M211] = RReq[M211] + (1. - omega6) * RRneq[M211];
        RRcoll[M121] = RReq[M121] + (1. - omega6) * RRneq[M121];
        RRcoll[M112] = RReq[M112] + (1. - omega6) * RRneq[M112];
        // Order 5 (Optimization: use RReq to avoid the computation of 5th-order RR)
        RRcoll[M221] = RReq[M221] + (1. - omega7) * RRneq[M221];
        RRcoll[M212] = RReq[M212] + (1. - omega7) * RRneq[M212];
        RRcoll[M122] = RReq[M122] + (1. - omega7) * RRneq[M122];
        // Order 6 (Optimization: use RReq to avoid the computation of 6th-order RR)
        RRcoll[M222] = RReq[M222] + (1. - omega8) * RRneq[M222];

        // Come back to RMcoll using relationships between RRs and RMs
        double cs2 = 1./3.;
        double cs4 = cs2*cs2;

        RMcoll[M200] = RRcoll[M200] + cs2;
        RMcoll[M020] = RRcoll[M020] + cs2;
        RMcoll[M002] = RRcoll[M002] + cs2;
        
        RMcoll[M110] = RRcoll[M110];
        RMcoll[M101] = RRcoll[M101];
        RMcoll[M011] = RRcoll[M011];

        RMcoll[M210] = RRcoll[M210] + cs2*u[1];
        RMcoll[M201] = RRcoll[M201] + cs2*u[2];
        RMcoll[M021] = RRcoll[M021] + cs2*u[2];
        RMcoll[M120] = RRcoll[M120] + cs2*u[0];
        RMcoll[M102] = RRcoll[M102] + cs2*u[0];
        RMcoll[M012] = RRcoll[M012] + cs2*u[1];
        
        RMcoll[M111] = RRcoll[M111];

        RMcoll[M220] = RRcoll[M220] + cs2*(RRcoll[M200] + RRcoll[M020]) + cs4;
        RMcoll[M202] = RRcoll[M202] + cs2*(RRcoll[M200] + RRcoll[M002]) + cs4;
        RMcoll[M022] = RRcoll[M022] + cs2*(RRcoll[M020] + RRcoll[M002]) + cs4;

        RMcoll[M211] = RRcoll[M211] + cs2*RRcoll[M011];
        RMcoll[M121] = RRcoll[M121] + cs2*RRcoll[M101];
        RMcoll[M112] = RRcoll[M112] + cs2*RRcoll[M110];

        RMcoll[M221] = RRcoll[M221] + cs2*(RRcoll[M201] + RRcoll[M021]) + cs4*u[2];
        RMcoll[M212] = RRcoll[M212] + cs2*(RRcoll[M210] + RRcoll[M012]) + cs4*u[1];
        RMcoll[M122] = RRcoll[M122] + cs2*(RRcoll[M120] + RRcoll[M102]) + cs4*u[0];

        RMcoll[M222] = RRcoll[M222] + cs2*(RRcoll[M220] + RRcoll[M202] + RRcoll[M022]) + cs4*(RRcoll[M200] + RRcoll[M020] + RRcoll[M002]) + cs2*cs4;

        // Optimization based on symmetries between populations and their opposite counterpart
        std::array<double, 27> foutRM;

        foutRM[F000] = rho *(1. - RMcoll[M200] - RMcoll[M020] - RMcoll[M002] + RMcoll[M220] + RMcoll[M202] + RMcoll[M022] - RMcoll[M222]);
        
        foutRM[FP00] = 0.5*rho * ( u[0] + RMcoll[M200] - RMcoll[M120] - RMcoll[M102] - RMcoll[M220] - RMcoll[M202] + RMcoll[M122] + RMcoll[M222]);
        foutRM[FM00] =     rho * (-u[0]                + RMcoll[M120] + RMcoll[M102]                               - RMcoll[M122])+ foutRM[FP00];

        foutRM[F0P0] = 0.5*rho * ( u[1] + RMcoll[M020] - RMcoll[M210] - RMcoll[M012] - RMcoll[M220] - RMcoll[M022] + RMcoll[M212] + RMcoll[M222]);
        foutRM[F0M0] =     rho * (-u[1]                + RMcoll[M210] + RMcoll[M012]                               - RMcoll[M212])+ foutRM[F0P0];

        foutRM[F00P] = 0.5*rho * ( u[2] + RMcoll[M002] - RMcoll[M201] - RMcoll[M021] - RMcoll[M202] - RMcoll[M022] + RMcoll[M221] + RMcoll[M222]);
        foutRM[F00M] =     rho * (-u[2]                + RMcoll[M201] + RMcoll[M021]                               - RMcoll[M221])+ foutRM[F00P];

        foutRM[FPP0] = 0.25*rho * ( RMcoll[M110] + RMcoll[M210] + RMcoll[M120] - RMcoll[M112] + RMcoll[M220] - RMcoll[M212] - RMcoll[M122] - RMcoll[M222]);
        foutRM[FMP0] =  0.5*rho * (-RMcoll[M110]                - RMcoll[M120] + RMcoll[M112]                               + RMcoll[M122])+ foutRM[FPP0];
        foutRM[FPM0] =  0.5*rho * (-RMcoll[M110] - RMcoll[M210]                + RMcoll[M112]                + RMcoll[M212]               )+ foutRM[FPP0];
        foutRM[FMM0] =  0.5*rho * (              - RMcoll[M210] - RMcoll[M120]                               + RMcoll[M212] + RMcoll[M122])+ foutRM[FPP0];

        foutRM[FP0P] = 0.25*rho * ( RMcoll[M101] + RMcoll[M201] + RMcoll[M102] - RMcoll[M121] + RMcoll[M202] - RMcoll[M221] - RMcoll[M122] - RMcoll[M222]);
        foutRM[FM0P] =  0.5*rho * (-RMcoll[M101]                - RMcoll[M102] + RMcoll[M121]                               + RMcoll[M122])+ foutRM[FP0P];
        foutRM[FP0M] =  0.5*rho * (-RMcoll[M101] - RMcoll[M201]                + RMcoll[M121]                + RMcoll[M221]               )+ foutRM[FP0P];
        foutRM[FM0M] =  0.5*rho * (              - RMcoll[M201] - RMcoll[M102]                               + RMcoll[M221] + RMcoll[M122])+ foutRM[FP0P];

        foutRM[F0PP] = 0.25*rho * ( RMcoll[M011] + RMcoll[M021] + RMcoll[M012] - RMcoll[M211] + RMcoll[M022] - RMcoll[M221] - RMcoll[M212] - RMcoll[M222]);
        foutRM[F0MP] =  0.5*rho * (-RMcoll[M011]                - RMcoll[M012] + RMcoll[M211]                               + RMcoll[M212])+ foutRM[F0PP];
        foutRM[F0PM] =  0.5*rho * (-RMcoll[M011] - RMcoll[M021]                + RMcoll[M211]                + RMcoll[M221]               )+ foutRM[F0PP];
        foutRM[F0MM] =  0.5*rho * (              - RMcoll[M021] - RMcoll[M012]                               + RMcoll[M221] + RMcoll[M212])+ foutRM[F0PP];

        foutRM[FPPP] = 0.125*rho * ( RMcoll[M111] + RMcoll[M211] + RMcoll[M121] + RMcoll[M112] + RMcoll[M221] + RMcoll[M212] + RMcoll[M122] + RMcoll[M222]);
        foutRM[FMPP] =  0.25*rho * (-RMcoll[M111]                - RMcoll[M121] - RMcoll[M112]                               - RMcoll[M122])+ foutRM[FPPP];
        foutRM[FPMP] =  0.25*rho * (-RMcoll[M111] - RMcoll[M211]                - RMcoll[M112]                - RMcoll[M212]               )+ foutRM[FPPP];
        foutRM[FPPM] =  0.25*rho * (-RMcoll[M111] - RMcoll[M211] - RMcoll[M121]                - RMcoll[M221]                              )+ foutRM[FPPP];
        foutRM[FMMP] =  0.25*rho * (              - RMcoll[M211] - RMcoll[M121]                               - RMcoll[M212] - RMcoll[M122])+ foutRM[FPPP];
        foutRM[FMPM] =  0.25*rho * (              - RMcoll[M211]                - RMcoll[M112] - RMcoll[M221]                - RMcoll[M122])+ foutRM[FPPP];
        foutRM[FPMM] =  0.25*rho * (                             - RMcoll[M121] - RMcoll[M112] - RMcoll[M221] - RMcoll[M212]               )+ foutRM[FPPP];
        foutRM[FMMM] =  0.25*rho * (-RMcoll[M111]                                              - RMcoll[M221] - RMcoll[M212] - RMcoll[M122])+ foutRM[FPPP];

        // Unrolled streaming step
        int XX = periodic ? (iX - 1 + dim.nx) % dim.nx: iX - 1;
        int YY = iY;
        int ZZ = iZ;
        size_t nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[ 0]) = foutRM[0] + f(nb, 0);
        }
        else {
            fout(nb, 0) = foutRM[0];
        }
        XX = iX;
        YY = periodic ? (iY - 1 + dim.ny) % dim.ny: iY - 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[ 1]) = foutRM[1] + f(nb, 1);
        }
        else {
            fout(nb, 1) = foutRM[1];
        }
        XX = iX;
        YY = iY;
        ZZ = periodic ? (iZ - 1 + dim.nz) % dim.nz: iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[ 2]) = foutRM[2] + f(nb, 2);
        }
        else {
            fout(nb, 2) = foutRM[2];
        }
        XX = periodic ? (iX - 1 + dim.nx) % dim.nx: iX - 1;
        YY = periodic ? (iY - 1 + dim.ny) % dim.ny: iY - 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[ 3]) = foutRM[3] + f(nb, 3);
        }
        else {
            fout(nb, 3) = foutRM[3];
        }
        XX = periodic ? (iX - 1 + dim.nx) % dim.nx: iX - 1;
        YY = periodic ? (iY + 1 + dim.ny) % dim.ny: iY + 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[ 4]) = foutRM[4] + f(nb, 4);
        }
        else {
            fout(nb, 4) = foutRM[4];
        }
        XX = periodic ? (iX - 1 + dim.nx) % dim.nx: iX - 1;
        YY = iY;
        ZZ = periodic ? (iZ - 1 + dim.nz) % dim.nz: iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[ 5]) = foutRM[5] + f(nb, 5);
        }
        else {
            fout(nb, 5) = foutRM[5];
        }
        XX = periodic ? (iX - 1 + dim.nx) % dim.nx: iX - 1;
        YY = iY;
        ZZ = periodic ? (iZ + 1 + dim.nz) % dim.nz: iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[ 6]) = foutRM[6] + f(nb, 6);
        }
        else {
            fout(nb, 6) = foutRM[6];
        }
        XX = iX;
        YY = periodic ? (iY - 1 + dim.ny) % dim.ny: iY - 1;
        ZZ = periodic ? (iZ - 1 + dim.nz) % dim.nz: iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[ 7]) = foutRM[7] + f(nb, 7);
        }
        else {
            fout(nb, 7) = foutRM[7];
        }
        XX = iX;
        YY = periodic ? (iY - 1 + dim.ny) % dim.ny: iY - 1;
        ZZ = periodic ? (iZ + 1 + dim.nz) % dim.nz: iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[ 8]) = foutRM[8] + f(nb, 8);
        }
        else {
            fout(nb, 8) = foutRM[8];
        }
        XX = periodic ? (iX - 1 + dim.nx) % dim.nx: iX - 1;
        YY = periodic ? (iY - 1 + dim.ny) % dim.ny: iY - 1;
        ZZ = periodic ? (iZ - 1 + dim.nz) % dim.nz: iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[ 9]) = foutRM[9] + f(nb, 9);
        }
        else {
            fout(nb, 9) = foutRM[9];
        }
        XX = periodic ? (iX - 1 + dim.nx) % dim.nx: iX - 1;
        YY = periodic ? (iY - 1 + dim.ny) % dim.ny: iY - 1;
        ZZ = periodic ? (iZ + 1 + dim.nz) % dim.nz: iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[10]) = foutRM[10] + f(nb,10);
        }
        else {
            fout(nb,10) = foutRM[10];
        }
        XX = periodic ? (iX - 1 + dim.nx) % dim.nx: iX - 1;
        YY = periodic ? (iY + 1 + dim.ny) % dim.ny: iY + 1;
        ZZ = periodic ? (iZ - 1 + dim.nz) % dim.nz: iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[11]) = foutRM[11] + f(nb,11);
        }
        else {
            fout(nb,11) = foutRM[11];
        }
        XX = periodic ? (iX - 1 + dim.nx) % dim.nx: iX - 1;
        YY = periodic ? (iY + 1 + dim.ny) % dim.ny: iY + 1;
        ZZ = periodic ? (iZ + 1 + dim.nz) % dim.nz: iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[12]) = foutRM[12] + f(nb,12);
        }
        else {
            fout(nb,12) = foutRM[12];
        }


        fout(i,13) = foutRM[13];


        XX = periodic ? (iX + 1 + dim.nx) % dim.nx: iX + 1;
        YY = iY;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[14]) = foutRM[14] + f(nb,14);
        }
        else {
            fout(nb,14) = foutRM[14];
        }
        XX = iX;
        YY = periodic ? (iY + 1 + dim.ny) % dim.ny: iY + 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[15]) = foutRM[15] + f(nb,15);
        }
        else {
            fout(nb,15) = foutRM[15];
        }
        XX = iX;
        YY = iY;
        ZZ = periodic ? (iZ + 1 + dim.nz) % dim.nz: iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[16]) = foutRM[16] + f(nb,16);
        }
        else {
            fout(nb,16) = foutRM[16];
        }
        XX = periodic ? (iX + 1 + dim.nx) % dim.nx: iX + 1;
        YY = periodic ? (iY + 1 + dim.ny) % dim.ny: iY + 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[17]) = foutRM[17] + f(nb,17);
        }
        else {
            fout(nb,17) = foutRM[17];
        }
        XX = periodic ? (iX + 1 + dim.nx) % dim.nx: iX + 1;
        YY = periodic ? (iY - 1 + dim.ny) % dim.ny: iY - 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[18]) = foutRM[18] + f(nb,18);
        }
        else {
            fout(nb,18) = foutRM[18];
        }
        XX = periodic ? (iX + 1 + dim.nx) % dim.nx: iX + 1;
        YY = iY;
        ZZ = periodic ? (iZ + 1 + dim.nz) % dim.nz: iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[19]) = foutRM[19] + f(nb,19);
        }
        else {
            fout(nb,19) = foutRM[19];
        }
        XX = periodic ? (iX + 1 + dim.nx) % dim.nx: iX + 1;
        YY = iY;
        ZZ = periodic ? (iZ - 1 + dim.nz) % dim.nz: iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[20]) = foutRM[20] + f(nb,20);
        }
        else {
            fout(nb,20) = foutRM[20];
        }
        XX = iX;
        YY = periodic ? (iY + 1 + dim.ny) % dim.ny: iY + 1;
        ZZ = periodic ? (iZ + 1 + dim.nz) % dim.nz: iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[21]) = foutRM[21] + f(nb,21);
        }
        else {
            fout(nb,21) = foutRM[21];
        }
        XX = iX;
        YY = periodic ? (iY + 1 + dim.ny) % dim.ny: iY + 1;
        ZZ = periodic ? (iZ - 1 + dim.nz) % dim.nz: iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[22]) = foutRM[22] + f(nb,22);
        }
        else {
            fout(nb,22) = foutRM[22];
        }

        XX = periodic ? (iX + 1 + dim.nx) % dim.nx: iX + 1;
        YY = periodic ? (iY + 1 + dim.ny) % dim.ny: iY + 1;
        ZZ = periodic ? (iZ + 1 + dim.nz) % dim.nz: iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[23]) = foutRM[23] + f(nb,23);
        }
        else {
            fout(nb,23) = foutRM[23];
        }
        XX = periodic ? (iX + 1 + dim.nx) % dim.nx: iX + 1;
        YY = periodic ? (iY + 1 + dim.ny) % dim.ny: iY + 1;
        ZZ = periodic ? (iZ - 1 + dim.nz) % dim.nz: iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[24]) = foutRM[24] + f(nb,24);
        }
        else {
            fout(nb,24) = foutRM[24];
        }
        XX = periodic ? (iX + 1 + dim.nx) % dim.nx: iX + 1;
        YY = periodic ? (iY - 1 + dim.ny) % dim.ny: iY - 1;
        ZZ = periodic ? (iZ + 1 + dim.nz) % dim.nz: iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[25]) = foutRM[25] + f(nb,25);
        }
        else {
            fout(nb,25) = foutRM[25];
        }
        XX = periodic ? (iX + 1 + dim.nx) % dim.nx: iX + 1;
        YY = periodic ? (iY - 1 + dim.ny) % dim.ny: iY - 1;
        ZZ = periodic ? (iZ - 1 + dim.nz) % dim.nz: iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[26]) = foutRM[26] + f(nb,26);
        }
        else {
            fout(nb,26) = foutRM[26];
        }
    }

    void iterateRR(double& f0) {
        int i = &f0 - lattice;
        if (flag[i] == CellType::bulk) {
            auto[iX, iY, iZ] = i_to_xyz(i);
            // auto RR = computeRR(f0);
            // auto RR = computeRRopt(f0);
            auto RR = computeRRopt2(f0);
            double rho = RR[M000];
            std::array<double, 3> u = {RR[M100], RR[M010], RR[M001]};  
            auto RReq = computeRReq(u);
            collideAndStreamRR(i, iX, iY, iZ, rho, u, RR, RReq);
        }
    }

    void operator() (double& f0) {
        iterateRR(f0);
    }
};

} // namespace twopop_soa_rr

